package com.phonethics.customclass;

import java.util.ArrayList;

import android.util.Log;

public class NissanGroupsAndLinks {

	String 							caption		=	"";
	String							link		=	"";
	String							group		= 	"";
	String							carName		= 	"";
	String							vLink		=	"";
	String							thumbnail	=	"";
	String							order		= 	"";
	String							description = 	"";
	//String							vlink		=	"";
	
	ArrayList<String> 				captionArr 	= 	new ArrayList<String>();
	ArrayList<String> 				linkArr 	= 	new ArrayList<String>();
	ArrayList<String> 				groupArr 	= 	new ArrayList<String>();
	ArrayList<String> 				carNameArr 	= 	new ArrayList<String>();
	ArrayList<String> 				vLinkArr 	= 	new ArrayList<String>();
	ArrayList<String> 				thumbnailArr = 	new ArrayList<String>();
	ArrayList<String> 				orderArr 	 = 	new ArrayList<String>();
	ArrayList<String> 				descriptionArr 	 = 	new ArrayList<String>();
	ArrayList<String> 				vlinkArr 	 = 	new ArrayList<String>();
	
	
	ArrayList<ArrayList<String>> 	allLinks 	= 	new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> 	allCaption 	= 	new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> 	allNames 	= 	new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> 	allThumbnails 	= 	new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> 	alldescription 	= 	new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> 	allvlink 	= 	new ArrayList<ArrayList<String>>();
	

	boolean							isNewArr 	= 	false;
	boolean							isLastLink	= 	false;
	boolean							convertStrToInt	= true;
	boolean							asPerDivision	= true;
	int	number = 0;

	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
		if(isNewArr){
			alldescription.add(descriptionArr);
			descriptionArr = new ArrayList<String>();
			descriptionArr.add(this.description);
		}else{
			descriptionArr.add(this.description);
			if(isLastLink){
				alldescription.add(descriptionArr);
			}

		}
		
	}
	public ArrayList<ArrayList<String>> getAlldescription() {
		return alldescription;
	}
	public void setAlldescription(ArrayList<ArrayList<String>> alldescription) {
		this.alldescription = alldescription;
	}
	public ArrayList<ArrayList<String>> getAllvlink() {
		return allvlink;
	}
	public void setAllvlink(ArrayList<ArrayList<String>> allvlink) {
		this.allvlink = allvlink;
	}
	
	
	public ArrayList<ArrayList<String>> getAllThumbnails() {
		return allThumbnails;
	}
	public void setAllThumbnails(ArrayList<ArrayList<String>> allThumbnails) {
		this.allThumbnails = allThumbnails;
	}

	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
		
		if(this.order.equalsIgnoreCase("")){
			this.order = "New Group";
		}
		if(!this.orderArr.contains(this.order) || orderArr.size()==0){
			orderArr.add(this.order);
			isNewArr = true;
			
		}else{
			Log.d("neon", "====== order " + this.order);
			//orderArr.add(this.order);
			isNewArr = false;
		}
		
	}
	public ArrayList<String> getOrderArr() {
		return orderArr;
	}
	public void setOrderArr(ArrayList<String> orderArr) {
		this.orderArr = orderArr;
	}
	public boolean isLastLink() {
		return isLastLink;
	}
	public void setLastLink(boolean isLastLink) {
		this.isLastLink = isLastLink;
	}
	public ArrayList<String> getLinkArr() {
		return linkArr;
	}
	public ArrayList<ArrayList<String>> getAllLinks() {
		return allLinks;
	}
	public void setAllLinks(ArrayList<ArrayList<String>> allLinks) {
		this.allLinks = allLinks;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
		/*if(caption.equalsIgnoreCase("")){
			this.caption = "New Group";
		}*/
		/*if(!captionArr.contains(this.caption) || captionArr.size()==0){
			captionArr.add(this.caption);
			isNewArr = true;
		}else{
			isNewArr = false;
		}*/
		
	
		if(isNewArr){
			allCaption.add(captionArr);
			captionArr = new ArrayList<String>();
			captionArr.add(this.caption);
			if(isLastLink){
				allCaption.add(captionArr);
			}
		}else{
			captionArr.add(this.caption);
			if(isLastLink){
				allCaption.add(captionArr);
			}

		}
		
		
	}
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
		if(isNewArr){
			allLinks.add(linkArr);
			linkArr = new ArrayList<String>();
			linkArr.add(this.link);
			if(isLastLink){
				allLinks.add(linkArr);
			}
		}else{
			linkArr.add(this.link);
			if(isLastLink){
				allLinks.add(linkArr);
			}

		}

	}
	public ArrayList<String> getCaptionArr() {
		return captionArr;
	}
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;

		if(!convertStrToInt){
			if(group.equalsIgnoreCase("")){
				this.group = "New Group";
			}
			if(!groupArr.contains(this.group) || groupArr.size()==0){
				groupArr.add(this.group);
				isNewArr = true;
			}else{
				isNewArr = false;
			}
		}else if(!asPerDivision){
			
			int groupNo = Integer.parseInt(group);
			if(groupNo<10){
				number = 0;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
				
			}else if((groupNo>=10) && (groupNo<100)){
				number = 1;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
				
			}else if((groupNo>=100) && (groupNo<200)){
				number = 2;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
				
			}else if((groupNo>=200) && (groupNo<300)){
				number = 3;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}else if((groupNo>=300) && (groupNo<400)){
				number = 4;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}else if((groupNo>=400) && (groupNo<500)){
				number = 5;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}else if((groupNo>=500) && (groupNo<600)){
				number = 6;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}else if((groupNo>=600) && (groupNo<700)){
				number = 7;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}else if((groupNo>=700) && (groupNo<800)){
				number = 8;
				if(!groupArr.contains(""+number) || groupArr.size()==0){
					groupArr.add(""+number);
					isNewArr = true;
					
				}else{
					isNewArr = false;
				}
			}
			
			
		}else{
			int groupNo = Integer.parseInt(group);
			number		= (int) Math.floor(groupNo/100);
			number		= number +1;
			if(!groupArr.contains(""+number) || groupArr.size()==0){
				groupArr.add(""+number);
				isNewArr = true;
				
			}else{
				isNewArr = false;
			}
		}
	}
	public String getThumbnail() {
		return thumbnail;
		
		
		
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
		if(isNewArr){
			allThumbnails.add(thumbnailArr);
			thumbnailArr = new ArrayList<String>();
			thumbnailArr.add(this.thumbnail);
			if(isLastLink){
				allThumbnails.add(thumbnailArr);
			}
		}else{
			thumbnailArr.add(this.thumbnail);
			if(isLastLink){
				allThumbnails.add(thumbnailArr);
			}

		}
		
	}
	public ArrayList<String> getThumbnailArr() {
		return thumbnailArr;
	}
	public void setThumbnailArr(ArrayList<String> thumbnailArr) {
		this.thumbnailArr = thumbnailArr;
	}
	public ArrayList<String> getGroupArr() {
		return groupArr;
	}
	public void setGroupArr(ArrayList<String> groupArr) {
		this.groupArr = groupArr;
	}



	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
		//Log.d("8888888888", "Set Name "+this.carName);
		if(isNewArr){
			allNames.add(carNameArr);
			carNameArr = new ArrayList<String>();
			carNameArr.add(this.carName);
			if(isLastLink){
				allNames.add(carNameArr);
			}
		}else{
			carNameArr.add(this.carName);
			if(isLastLink){
				allNames.add(carNameArr);
			}

		}
	}
	public String getvLink() {
		return vLink;
	}
	public void setvLink(String vLink) {
		this.vLink = vLink;
		if(isNewArr){
			allvlink.add(vlinkArr);
			vlinkArr = new ArrayList<String>();
			vlinkArr.add(this.vLink);
			if(isLastLink){
				allvlink.add(vlinkArr);
			}
		}else{
			vlinkArr.add(this.vLink);
			if(isLastLink){
				allvlink.add(vlinkArr);
			}

		}
	}
	public ArrayList<String> getCarNameArr() {
		return carNameArr;
	}
	public ArrayList<ArrayList<String>> getAllNames() {
		return allNames;
	}
	public void setAllNames(ArrayList<ArrayList<String>> allNames) {
		this.allNames = allNames;
	}
	public void setCarNameArr(ArrayList<String> carNameArr) {
		this.carNameArr = carNameArr;
	}
	public ArrayList<String> getvLinkArr() {
		return vLinkArr;
	}
	public void setvLinkArr(ArrayList<String> vLinkArr) {
		this.vLinkArr = vLinkArr;
	}
	public void setCaptionArr(ArrayList<String> captionArr) {
		this.captionArr = captionArr;
	}

	
	public ArrayList<ArrayList<String>> getAllCaption() {
		return allCaption;
	}
	public void setAllCaption(ArrayList<ArrayList<String>> allCaption) {
		this.allCaption = allCaption;
	}

}
