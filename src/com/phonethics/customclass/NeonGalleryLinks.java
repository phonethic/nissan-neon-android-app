package com.phonethics.customclass;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;

public class NeonGalleryLinks implements Serializable{

	/*
	 * To store Links captions and description of each photo.
	 */
	private String LINK="";
	private String CAPTION="";
	private String DESCRIPTION="";
	private String VLINK="";
	private String CARRNAME="";
	private String GROUP="";
	private String thumbnail = "";
	private String order = "";
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getGROUP() {
		return GROUP;
	}
	public void setGROUP(String gROUP) {
		GROUP = gROUP;
	}
	public String getCARRNAME() {
		return CARRNAME;
	}
	public void setCARRNAME(String cARRNAME) {
		CARRNAME = cARRNAME;
	}
	public String getVLINK() {
		return VLINK;
	}
	public void setVLINK(String vLINK) {
		VLINK = vLINK;
	}
	public String getLINK() {
		return LINK;
	}
	public void setLINK(String lINK) {
		LINK = lINK;
	//	Log.i("Inside POJO Links : ", " "+LINK);
		
	}
	public String getCAPTION() {
		return CAPTION;
	}
	public void setCAPTION(String cAPTION) {
		CAPTION = cAPTION;
		//Log.i("Inside POJO Caption : ", " "+CAPTION);
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
		//Log.i("Inside POJO DESCRIPTION : ", " "+dESCRIPTION);
	}
	
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
			
	
	
}
