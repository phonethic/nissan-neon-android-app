package com.phonethics.customclass;

import java.io.Serializable;
import java.util.ArrayList;

public class CarBrands implements Serializable {
	
	private String brandName 			= null;
	private String modelName 			= null;
	private ArrayList<String> carBrands = new ArrayList<String>();
	private ArrayList<String> carName 	= new ArrayList<String>();
	
	
	
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
		carBrands.add(this.brandName);
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
		carName.add(this.modelName);
	}
	
	public ArrayList<String> allCarBrands(){
		return carBrands;
	}
	
	public ArrayList<String> allCarNames(){
		return carName;
	}
	
	
	

}
