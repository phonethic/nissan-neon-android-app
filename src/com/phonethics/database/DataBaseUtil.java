package com.phonethics.database;
import java.util.ArrayList;

import android.R.string;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DataBaseUtil 
{

	/*
	 * FIELD INITIALIZATION FOR EXPENSES Table
	 */
	public static final String KEY_ROWID="_id";
	public static final String KEY_DATE="_DATE";
	public static final String KEY_COST="COST";
	public static final String KEY_TYPE="TYPE";
	public static final String KEY_NOTES="NOTES"; 
	public static final String KEY_READING="READING"; 
	public static final String SELECTED ="SELECTED"; 
	public static final String CARID ="CARID";

	/*
	 * FIELD INITIALIZATION FOR EXPTYPE DATABASE
	 */

	public static final String KEY_ROWID_EXTYPE="_id";
	public static final String KEY_TYPE_EXTYPE="TYPE";
	public static final String CARID_EXTYPE ="CARID";

	/*
	 * 	FIELD INITIALIZATION FOR MILEAGE BREAKPOINTS
	 */
	public static final String KEY_ROWID_MILEAGE="_id";
	public static final String KEY_MILEAGEID="MILEAGEID";
	public static final String KEY_ODOMETER_READING="ODOMETER_READING";
	public static final String KEY_ISPARTIAL="ISPARTIAL";
	public static final String KEY_MILEAGE_DATE="_DATE";
	public static final String KEY_MILEAGE_FUEL="FUEL";
	public static final String KEY_MILEAGE_COST="COST";
	public static final String KEY_MILEAGE_CARID="CARID";
	public static final String KEY_MARKER="MARKER";
	/*
	 * 	FIELD INITIALIZATION FOR MILEAGE 
	 */
	public static final String KEY_ROWID_MILEAGECALC="_id";
	public static final String KEY_MILEAGECALC_STARTDATE="_DATE";
	public static final String KEY_MILEAGECALC_ENDDATE="_DATEEND";
	public static final String KEY_MILEAGECALC_STARTKM="STARTKM";
	public static final String KEY_MILEAGECALC_ENDKM="ENDKM";
	public static final String KEY_MILEAGECALC_FUEL="FUEL";
	public static final String KEY_MILEAGECALC_SELECTED="SELECTED";
	public static final String KEY_MILEAGECALC_COST="COST";
	public static final String KEY_MILEAGECALC_MILEAGE="MILEAGE";
	public static final String KEY_MILEAGECALC_CARID="CARID";
	public static final String KEY_ISOPEN="ISOPEN"; 


	/*
	 * MY_CAR 	Table
	 */
	private final int firstcar = 0;
	/*	private final static String databaseName	= "MyCar_Database";*/
	private final static int 	databaseVersion	= 1;
	private final static String tableName 		= "MY_CAR";
	private final static String carId 			= "CAR_ID";
	private final static String imagepath 		= "IMAGE_PATH";
	private final static String model 			= "MODEL";
	private final static String carNumber 		= "CAR_NUMBER";
	private final static String mfgDate 		= "MFG_DATE";
	private final static String RcNumber 		= "RC_NUMBER";
	private final static String insurer 		= "INSURER";
	private final static String policy 			= "POLICY";
	private final static String policyDate 		= "POLICY_DATE";
	private final static String chassis 		= "CHASSIS";
	private final static String buyDate 		= "BUY_DATE";
	private final static String selected 		= "SELECTED";
	private final static String brandText 		= "BRAND";

	private final static String maxCarId 		= "SELECT MAX(" + carId + ") FROM "+ tableName;
	private final static String minCarId 		= "SELECT MIN(" + carId + ") FROM "+ tableName;

	/**
	 * 
	 * The following variables are for REMEINDERS table........
	 * 
	 * 
	 */
	private final static String table_reminder	= "REMEINDERS";
	private final static String	remId			= "REM_ID";
	private final static String	onoff			= "REMINDERONOFF";
	private final static String	messege_rem		= "MESSEGE";
	private final static String	date_rem		= "DATE";
	private final static String time_rem		= "TIME";
	private final static String	completed		= "COMPLETED";
	private final static String preValue		= "PRE_VALUE";
	private final static String preUnit			= "PRE_UNIT";



	/*
	 * Database Name
	 */
	private static final String DATABASE_NAME="NissanDataBase.db";

	/*
	 * Table Names
	 */
	private static final String DATABASE_TABLE_NAME="EXPENSES";
	private static final String DATABASE_TABLE_NAME_EXTYPE="EXPTYPE";
	private static final String DATABASE_TABLE_NAME_MILEAGE="MILEAGEBREAK";

	private static final String DATABASE_TABLE_NAME_MILEAGECAL="MILEAGE";

	private static final int  DATABASE_VERSOIN=1;

	private DbHelper dbHelper;
	private final Context ourContext;

	private SQLiteDatabase sqlDataBase;

	private boolean logOn=true;

	public DataBaseUtil(Context c)
	{
		ourContext=c;
	}

	private static class  DbHelper extends SQLiteOpenHelper
	{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			/*
			 * EXPENSES Table
			 */
			db.execSQL("CREATE TABLE "+
					DATABASE_TABLE_NAME +" ("+
					KEY_ROWID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_DATE +" TEXT, "+
					KEY_COST+" TEXT, " +
					KEY_TYPE +" TEXT, "+
					KEY_NOTES +" TEXT, "+
					KEY_READING + " TEXT,"+
					SELECTED + " TEXT,"+
					CARID + " INTEGER);"
					);
			/*
			 * EXPTYPE Table
			 */
			db.execSQL("CREATE TABLE  "+
					DATABASE_TABLE_NAME_EXTYPE +" ("+
					KEY_ROWID_EXTYPE +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_TYPE_EXTYPE +" TEXT, "+
					CARID_EXTYPE + " INTEGER);"
					);

			ContentValues cv=new ContentValues();
			ArrayList<String> expListArray=new ArrayList<String>();
			expListArray.add("Repair");
			expListArray.add("Wash");
			expListArray.add("Oil Changed");
			for(int i=0;i<expListArray.size();i++)
			{
				cv.put(KEY_TYPE_EXTYPE, expListArray.get(i));
				db.insert(DATABASE_TABLE_NAME_EXTYPE, null, cv);
			}

			/*
			 * MILEAGEBREAK Table
			 */

			db.execSQL("CREATE TABLE  "+
					DATABASE_TABLE_NAME_MILEAGE +" ("+
					KEY_ROWID_MILEAGE+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_MILEAGEID +" INTEGER, "+
					KEY_ODOMETER_READING + " REAL, "+
					KEY_MILEAGE_DATE +" INTEGER, "+
					KEY_MILEAGE_FUEL+" REAL, " +
					KEY_ISPARTIAL+" INTEGER, " + 
					KEY_MILEAGE_COST+" REAL, "+
					KEY_MILEAGE_CARID +" INTEGER, "+
					KEY_MARKER+" INTEGER);");


			/*
			 * MILEAGE
			 */
			db.execSQL("CREATE TABLE  "+
					DATABASE_TABLE_NAME_MILEAGECAL +" ("+
					KEY_ROWID_MILEAGECALC +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_MILEAGECALC_STARTDATE +" INTEGER, "+
					KEY_MILEAGECALC_ENDDATE +" INTEGER, "+
					KEY_MILEAGECALC_STARTKM +" REAL, "+
					KEY_MILEAGECALC_ENDKM +" REAL, "+
					KEY_MILEAGECALC_FUEL+" REAL, " +
					KEY_MILEAGECALC_SELECTED+" TEXT, " +
					KEY_MILEAGECALC_COST+" REAL, " +
					KEY_MILEAGECALC_MILEAGE+" REAL, " +
					KEY_MILEAGECALC_CARID + " INTEGER, "+
					KEY_ISOPEN + " INTEGER );"
					);

			/*
			 * MY_CAR TABLE
			 * 
			 */

			String create_table_mycar = "CREATE TABLE IF NOT EXISTS " + tableName + 
					"(" + carId + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
					imagepath   + " TEXT, " + 


					model		+ " TEXT, " + 
					carNumber	+ " TEXT, " + 

					mfgDate		+ " TEXT, " + 
					RcNumber	+ " TEXT, " + 

					insurer		+ " TEXT, " + 
					policy		+ " TEXT, " + 

					policyDate	+ " TEXT, " + 
					chassis		+ " TEXT, " +

					buyDate		+ " TEXT, " + 
					selected	+ " TEXT, " + 
					brandText	+ " TEXT" + ");";

			db.execSQL(create_table_mycar);

			/*
			 * REMINDERS TABLE
			 */
			String create_table_remeinders = "CREATE TABLE IF NOT EXISTS " + table_reminder + 
					"(" + remId + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
					onoff		    + " TEXT, " + 
					messege_rem		+ " TEXT, " + 
					date_rem		+ " TEXT, " +
					time_rem		+ " TEXT, " +
					completed	    + " TEXT, " +
					preValue	    + " TEXT, " +
					preUnit	    	+ " TEXT" + ");";

			db.execSQL(create_table_remeinders);


		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_NAME_EXTYPE);
			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_NAME_MILEAGE);
			db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE_NAME_MILEAGECAL);
			db.execSQL("DROP TABLE IF EXISTS " + tableName);
			db.execSQL("DROP TABLE IF EXISTS " +table_reminder);
			onCreate(db);

		}

	}//end of inner class

	public  DataBaseUtil open() throws SQLException
	{
		dbHelper=new DbHelper(ourContext);
		sqlDataBase=dbHelper.getWritableDatabase();

		return this;

	}

	public void close()
	{
		dbHelper.close();
	}


	//Creating entries 
	public long createEntery(String extype) {
		// TODO Auto-generated method stub
		ContentValues cv=new ContentValues();
		cv.put(KEY_TYPE_EXTYPE, extype);
		return sqlDataBase.insert(DATABASE_TABLE_NAME_EXTYPE, null, cv);
	}
	//getting Data from table
	public ArrayList<String> getTypeData() {
		// TODO Auto-generated method stub
		String [] columns=new String[]{KEY_ROWID_EXTYPE,KEY_TYPE_EXTYPE};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME_EXTYPE, columns, null, null, null, null, null);

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID_EXTYPE);
		int iType=c.getColumnIndex(KEY_TYPE_EXTYPE);

		//String data="";

		/*
		 * fetching data row by row
		 */
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			//data+=c.getString(iRowid)+""+c.getString(iType);
			//result.add(c.getString(iRowid)+""+c.getString(iType));
			result.add(c.getString(iRowid)+"~"+c.getString(iType)+"$");
			if(logOn)
				Log.i("Data : ", " "+c.getString(iType));
		}

		c.close();
		return result;
	}

	/*
	 * Update EXPTYPE
	 */

	public void updateExpTypeById(int id,String extype)
	{
		try
		{
			ContentValues cv=new ContentValues();
			cv.put(KEY_TYPE_EXTYPE, extype);
			sqlDataBase.update(DATABASE_TABLE_NAME_EXTYPE, cv, KEY_ROWID_EXTYPE + "=" + id, null);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/*
	 * Delete Entry From EXPTYPE
	 */
	public void deleteExpTypeById(int id)
	{
		try
		{
			sqlDataBase.delete(DATABASE_TABLE_NAME_EXTYPE, KEY_ROWID_EXTYPE+"="+id, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * Create entery for EXPENSES Table
	 */
	public long createEntery(String date, String cost,String type,String notes,String reading,int carId) {
		// TODO Auto-generated method stub
		ContentValues cv=new ContentValues();
		cv.put(KEY_DATE,date);
		cv.put(KEY_COST, cost);
		cv.put(KEY_TYPE, type);
		cv.put(KEY_NOTES, notes);
		cv.put(KEY_READING, reading);
		cv.put(SELECTED, "0");
		cv.put(CARID, carId);
		return sqlDataBase.insert(DATABASE_TABLE_NAME, null, cv);
	}

	/*
	 * Getting Data from EXPENSES Table
	 */
	public ArrayList<String> getData(int carID) {
		// TODO Auto-generated method stub
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns, CARID+ "=" +carID, null, null, null, null);

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
		int iReading=c.getColumnIndex(KEY_READING);
		String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");
			if(logOn)
			{
				Log.i("DATABASE UTIL Result: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;
	}

	/*
	 * Getting Data for specific from EXPENSES Table
	 */
	public ArrayList<String> getAllData(int id) {
		// TODO Auto-generated method stub
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns, KEY_ROWID+ "=" + id, null, null, null, null);

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		int iNotes=c.getColumnIndex(KEY_NOTES);
		int iReading=c.getColumnIndex(KEY_READING);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
		int iReading=c.getColumnIndex(KEY_READING);
		String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*"+c.getString(iNotes)+"|"+c.getString(iReading)+"$");
			if(logOn)
			{
				Log.i("Result: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;
	}


	/*
	 * Getting Data for specific from EXPENSES Table
	 */
	public ArrayList<String> getAllRows(int carID) {
		// TODO Auto-generated method stub
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns,CARID+ "=" +carID, null, null, null, null);

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		int iNotes=c.getColumnIndex(KEY_NOTES);
		int iReading=c.getColumnIndex(KEY_READING);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
		int iReading=c.getColumnIndex(KEY_READING);
		String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*"+c.getString(iNotes)+"|"+c.getString(iReading)+"$");
			if(logOn)
			{
				Log.i("Result: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;
	}


	/*
	 * Updating data of EXPENSES Table for perticular Id
	 *
	 */
	public void updateById(int id,String date,String cost,String type,String notes,String reading)
	{

		ContentValues values = new ContentValues();
		values.put(KEY_DATE,date);
		values.put(KEY_COST,cost);
		values.put(KEY_NOTES,notes);
		values.put(KEY_READING,reading);
		values.put(KEY_TYPE, type);
		try
		{
			sqlDataBase.update(DATABASE_TABLE_NAME, values, KEY_ROWID + "=" + id, null);
		}
		catch(SQLException sqex)
		{
			sqex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}



	}

	/*
	 * Setting visiblity of selected field
	 */
	public void setVisibility(String Id){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/
		String isvisible = "1";
		String notVisible = "0";
		if(logOn)
			Log.i("ID is : "," "+Id);
		visible.put(SELECTED, isvisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{

			sqlDataBase.update(DATABASE_TABLE_NAME, visible, KEY_ROWID + "=" + Id, null);
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
	}


	public void setInVisibility(String Id){
		/*		ContentValues visible = new ContentValues();*/
		ContentValues invisible = new ContentValues();
		/*String isvisible = "1";*/
		String notVisible = "0";
		/*visible.put(SELECTED, isvisible);*/
		invisible.put(SELECTED, notVisible);
		try{

			/*sqlDataBase.update(DATABASE_NAME, visible, KEY_ROWID + "=" + Id, null);*/
			sqlDataBase.update(DATABASE_TABLE_NAME, invisible, KEY_ROWID + "=" + Id, null);

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
	}


	public void setAllVisibile(){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/
		String isvisible = "1";
		String notVisible = "0";

		visible.put(SELECTED, isvisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{
			open();
			sqlDataBase.update(DATABASE_TABLE_NAME, visible,null, null);
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
		finally
		{
			close();
		}
	}

	public void setAllInVisibile(){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/

		String notVisible = "0";

		visible.put(SELECTED, notVisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{
			open();
			sqlDataBase.update(DATABASE_TABLE_NAME, visible,null, null);
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}finally
		{
			close();
		}
	}

	/*
	 * Get the selction mode of perticular id whether it is 1 or 0.
	 */
	public String selectMode(String Id)
	{
		/*ContentValues selectMode = new ContentValues();
		String [] columns=new String[]{SELECTED};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns, null, null, null, null, null);*/
		/*Cursor c1=sqlDataBase.query(DATABASE_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy)*/

		String selectionMode="";

		String query="SELECT "+SELECTED+" FROM "+DATABASE_TABLE_NAME+" WHERE "+KEY_ROWID+"='" + Id+"'";

		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		if (cursor != null) {
			cursor.moveToFirst();
		}
		selectionMode=cursor.getString(cursor.getColumnIndex(SELECTED));
		cursor.close();
		return selectionMode;

	}

	/*
	 * Checking whether all rows are selected or not
	 * Returns true if  all rows selected else false;
	 */
	public boolean isAllRowsSelected()
	{

		String selectionMode="";
		boolean isSelected=false;
		String query="SELECT "+SELECTED+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			selectionMode=cursor.getString(cursor.getColumnIndex(SELECTED));
			if(selectionMode.equalsIgnoreCase("1"))
			{
				isSelected=true;
				Log.i("Selection Mode"," "+selectionMode);
			}
			else
			{
				isSelected=false;
				Log.i("Selection Mode"," "+selectionMode);
				break;

			}
		}

		cursor.close();
		close();
		Log.i("Selection Status"," "+selectionMode);
		return isSelected;
	}

	public boolean isAnyRowsSelected()
	{

		String selectionMode="";
		boolean isSelected=false;
		String query="SELECT "+SELECTED+" FROM "+DATABASE_TABLE_NAME;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			selectionMode=cursor.getString(cursor.getColumnIndex(SELECTED));
			if(selectionMode.equalsIgnoreCase("1"))
			{
				isSelected=true;
				break;
			}
			else
			{
				isSelected=false;
			}
		}

		cursor.close();
		close();
		return isSelected;
	}

	public long getRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME);
		close();
		return rowCount ;
	}

	public long getRowCount(int carID)
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
	/*	open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME);
		close();
		return rowCount ;*/
		
		
		long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME +" WHERE "+CARID+ " = "+carID;
		
		try
		{
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();
		close();
		}
		catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return rowcount;
		
	}

	/*
	 * Sum of Cost From EXPENSES
	 */

	public String getTotalOfSelected()
	{
		double sum=0.0;
		try
		{

			open();

			String total;

			String query="SELECT "+KEY_COST+" FROM "+DATABASE_TABLE_NAME+" where SELECTED=1";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			int index=0;
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				//data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";
				total=cursor.getString(cursor.getColumnIndex(KEY_COST));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
				}
				index++;
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return sum+"";

	}


	public String Total(int carID)
	{
		double sum=0.0;
		try
		{

			open();

			String total;

			String query="SELECT "+KEY_COST+" FROM "+DATABASE_TABLE_NAME+" where "+CARID+" = "+carID;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			int index=0;
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				//data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";
				total=cursor.getString(cursor.getColumnIndex(KEY_COST));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
				}
				index++;
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return sum+"";

	}


	/*
	 * Getting Total Cost of Selected Items only 
	 *  
	 */


	/*
	 * Getting Cost of specific id for EXPENSES
	 */
	public String getCost(String Id)
	{
		double sum=0.0;
		try
		{

			open();

			String total;

			String query="SELECT "+KEY_COST+" FROM "+DATABASE_TABLE_NAME+" WHERE "+KEY_ROWID+"='" + Id+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			int index=0;
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total=cursor.getString(cursor.getColumnIndex(KEY_COST));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
				}
				index++;
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return sum+"";

	}
	/*
	 * Sorting Data by Date.
	 */
	public ArrayList<String> sortByDate(int carID)
	{
		/*Cursor c=ourDataBase.query(DATABASE_TABLE_NAME, columns, null, null, null, null, KEY_ROWID+" desc");*/
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns, CARID+ "="+carID, null, null, null, KEY_DATE+" desc");

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
	int iReading=c.getColumnIndex(KEY_READING);
	String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");
			if(logOn)
			{
				Log.i("Sort By Date: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;


	}
	/*
	 * Sort By cost
	 */
	public ArrayList<String> sortByCost(int carId)
	{
		/*Cursor c=ourDataBase.query(DATABASE_TABLE_NAME, columns, null, null, null, null, KEY_ROWID+" desc");*/
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns,CARID+ "="+carId, null, null, null, KEY_COST+" desc");

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
	int iReading=c.getColumnIndex(KEY_READING);
	String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");
			if(logOn)
			{
				Log.i("Sort By Cost: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;


	}
	/*
	 * Sort by Type
	 */

	public ArrayList<String> sortByType(int carId)
	{
		/*Cursor c=ourDataBase.query(DATABASE_TABLE_NAME, columns, null, null, null, null, KEY_ROWID+" desc");*/
		String [] columns=new String[]{KEY_ROWID,KEY_DATE,KEY_COST,KEY_TYPE,KEY_NOTES,KEY_READING};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns,CARID+ "="+carId, null, null, null, KEY_TYPE+" desc");

		ArrayList<String> result=new ArrayList<String>();

		int iRowid=c.getColumnIndex(KEY_ROWID);
		int iDate=c.getColumnIndex(KEY_DATE);
		int iCost=c.getColumnIndex(KEY_COST);
		int iType=c.getColumnIndex(KEY_TYPE);
		/*	int iNotes=c.getColumnIndex(KEY_NOTES);
	int iReading=c.getColumnIndex(KEY_READING);
	String data="";*/
		int index=0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
			result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");
			if(logOn)
			{
				Log.i("Sort By Type: "," "+result.get(index));
			}
			index++;
		}

		c.close();
		return result;


	}



	/*
	 * Deleting Data by Specific id From EXPENSES table
	 */
	public void delete_byID(int id){
		sqlDataBase.delete(DATABASE_TABLE_NAME, KEY_ROWID+"="+id, null);
	}

	public void delete_byCarID(int carId){
		sqlDataBase.delete(DATABASE_TABLE_NAME, CARID+"="+carId, null);
	}

	/*
	 * Crating Entry for Mileage Break points
	 *  
	 */

	public long createMileageBreakEntery(int mileageID,double odometer_reading,long date, double fuel,int isPartial,double cost,int marker,int car_id) {
		// TODO Auto-generated method stub

		/*db.execSQL("CREATE TABLE  "+
				DATABASE_TABLE_NAME_MILEAGE +" ("+
				KEY_ROWID_MILEAGE+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
				KEY_MILEAGEID +" INTEGER, "+
				KEY_ODOMETER_READING + " REAL, "+
				KEY_MILEAGE_DATE +" TEXT, "+
				KEY_MILEAGE_FUEL+" REAL, " +
				KEY_ISPARTIAL+" INTEGER, " + 
				KEY_MILEAGE_COST+" REAL, "+
				KEY_MILEAGE_CARID +" INTEGER, "+
				KEY_MARKER+" INTEGER);");*/

		ContentValues cv=new ContentValues();
		cv.put(KEY_MILEAGEID, mileageID);
		cv.put(KEY_ODOMETER_READING,odometer_reading);
		cv.put(KEY_MILEAGE_DATE,date);
		cv.put(KEY_MILEAGE_FUEL, fuel);
		cv.put(KEY_ISPARTIAL, isPartial);
		cv.put(KEY_MILEAGE_COST, cost);
		cv.put(KEY_MILEAGE_CARID, car_id);
		cv.put(KEY_MARKER, marker);
		return sqlDataBase.insert(DATABASE_TABLE_NAME_MILEAGE, null, cv);
	}

	/*
	 *	Update MILEAGEID In MILEAGEBREAK
	 */
	public void updateMileageIDBreakEntery(int id,int mid)
	{
		try
		{
			ContentValues cv=new ContentValues();
			cv.put(KEY_MILEAGEID, mid);
			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGE, cv, KEY_MILEAGEID + "=" + id, null);
		}
		catch(SQLException sqex)
		{
			sqex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	/*
	 * Getting Data from Mileage Break point
	 */


	/*public ArrayList<ArrayList<Object>> getFuelDetails()
	{
		ArrayList<ArrayList<Object>> fuelDetails = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGE,KEY_MILEAGEID,KEY_MILEAGE_DATE,
				KEY_MILEAGE_FUEL,KEY_MILEAGE_COST,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGE,columns,null,null,null,null,null

					);

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGE);
			int iMilId=cursor.getColumnIndex(KEY_MILEAGEID);
			int iDate=cursor.getColumnIndex(KEY_MILEAGE_DATE);

			int iFuel=cursor.getColumnIndex(KEY_MILEAGE_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGE_COST);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ArrayList<Object> fuel = new ArrayList<Object>();

				fuel.add(cursor.getString(iRowId));
				fuel.add(cursor.getString(iMilId));
				fuel.add(cursor.getString(iDate));

				fuel.add(cursor.getString(iFuel));
				fuel.add(cursor.getString(iMCost));
				fuel.add(cursor.getString(iMCarId));
				fuelDetails.add(fuel);

			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return fuelDetails;
	}*/


	public ArrayList<String> getFuelDetails()
	{
		ArrayList<String> fuelDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGE,KEY_MILEAGEID,KEY_ODOMETER_READING,KEY_MILEAGE_DATE,
				KEY_MILEAGE_FUEL,KEY_ISPARTIAL,KEY_MILEAGE_COST,
				KEY_MILEAGE_CARID,KEY_MARKER};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGE,columns,null,null,null,null,null

					);

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGE);
			int iMilId=cursor.getColumnIndex(KEY_MILEAGEID);
			int iOdometerReading=cursor.getColumnIndex(KEY_ODOMETER_READING);
			int iDate=cursor.getColumnIndex(KEY_MILEAGE_DATE);

			int iFuel=cursor.getColumnIndex(KEY_MILEAGE_FUEL);
			int iIsPartial=cursor.getColumnIndex(KEY_ISPARTIAL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGE_COST);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);
			int iMarker=cursor.getColumnIndex(KEY_MARKER);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{

				/*result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");*/
				fuelDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iMilId)+"~"+cursor.getString(iDate)+"#"+cursor.getString(iFuel)+"*"+cursor.getString(iMCost)+"|"+cursor.getString(iOdometerReading)+"&"+cursor.getString(iIsPartial)+"~"+cursor.getString(iMCarId)+"#"+cursor.getString(iMarker));

			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return fuelDetails;
	}

	/*
	 *  
	 */

	public ArrayList<String> fuelSessionState()
	{
		ArrayList<String>  state =new ArrayList<String>();

		String query="SELECT "+KEY_MARKER+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			state.add(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));

		}

		cursor.close();
		close();
		return state;
	}
	public ArrayList<String> fuelSessionState(int id,int carID)
	{
		ArrayList<String>  state =new ArrayList<String>();

		String query="SELECT "+KEY_MARKER+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			state.add(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));

		}

		cursor.close();
		close();
		return state;
	}

	public ArrayList<String> isPartial()
	{
		ArrayList<String>  ispartial =new ArrayList<String>();

		String query="SELECT "+KEY_ISPARTIAL+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			ispartial.add(cursor.getString(cursor.getColumnIndex(KEY_ISPARTIAL)));

		}

		cursor.close();
		close();
		return ispartial;
	}
	public ArrayList<String> isPartial(int id,int carID)
	{
		ArrayList<String>  ispartial =new ArrayList<String>();

		String query="SELECT "+KEY_ISPARTIAL+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			ispartial.add(cursor.getString(cursor.getColumnIndex(KEY_ISPARTIAL)));

		}

		cursor.close();
		close();
		return ispartial;
	}
	public ArrayList<String> fuelMarker()
	{
		ArrayList<String>  fuelMarker =new ArrayList<String>();

		String query="SELECT "+KEY_MARKER+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuelMarker.add(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));

		}

		cursor.close();
		close();
		return fuelMarker;
	}
	public ArrayList<String> fuelMarker(int id,int carID)
	{
		ArrayList<String>  fuelMarker =new ArrayList<String>();

		String query="SELECT "+KEY_MARKER+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuelMarker.add(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));

		}

		cursor.close();
		close();
		return fuelMarker;
	}

	public ArrayList<String> fuelDate()
	{
		ArrayList<String>  FuelDate =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_DATE+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			FuelDate.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGE_DATE)));

		}

		cursor.close();
		close();
		return FuelDate;
	}

	public ArrayList<String> fuelDate(int id,int carID)
	{
		ArrayList<String>  FuelDate =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_DATE+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			FuelDate.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGE_DATE)));

		}

		cursor.close();
		close();
		return FuelDate;
	}

	public String fuelDate(int id,int marker,int carID)
	{
		String  FuelDate ="";

		String query="SELECT "+KEY_MILEAGE_DATE+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MARKER+" = "+marker+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			//FuelDate.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGE_DATE)));
			FuelDate=cursor.getString(cursor.getColumnIndex(KEY_MILEAGE_DATE));

		}

		cursor.close();
		close();
		return FuelDate;
	}

	public ArrayList<String> fuelOdometer()
	{
		ArrayList<String>  FuelOdometer =new ArrayList<String>();

		String query="SELECT "+KEY_ODOMETER_READING+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			FuelOdometer.add(cursor.getDouble(cursor.getColumnIndex(KEY_ODOMETER_READING))+"");

		}

		cursor.close();
		close();
		return FuelOdometer;

	}
	public ArrayList<String> fuelOdometer(int id,int carID)
	{
		ArrayList<String>  FuelOdometer =new ArrayList<String>();

		String query="SELECT "+KEY_ODOMETER_READING+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			FuelOdometer.add(cursor.getDouble(cursor.getColumnIndex(KEY_ODOMETER_READING))+"");

		}

		cursor.close();
		close();
		return FuelOdometer;

	}

	public double fuelOdometer(int id,int marker,int carID)
	{
		double  FuelOdometer =0.0;

		String query="SELECT "+KEY_ODOMETER_READING+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MARKER+" = "+marker+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			//FuelOdometer.add(cursor.getDouble(cursor.getColumnIndex(KEY_ODOMETER_READING))+"");
			FuelOdometer=Double.parseDouble(cursor.getDouble(cursor.getColumnIndex(KEY_ODOMETER_READING))+"");

		}

		cursor.close();
		close();
		return FuelOdometer;

	}

	public ArrayList<String> fuel()
	{
		ArrayList<String>  fuel =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_FUEL+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuel.add(cursor.getDouble(cursor.getColumnIndex(KEY_MILEAGE_FUEL))+"");

		}

		cursor.close();
		close();
		return fuel;


	}





	public ArrayList<String> fuel(int id,int carID)
	{
		ArrayList<String>  fuel =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_FUEL+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuel.add(cursor.getDouble(cursor.getColumnIndex(KEY_MILEAGE_FUEL))+"");

		}

		cursor.close();
		close();
		return fuel;


	}

	public  ArrayList<String> fuelCost()
	{
		ArrayList<String>  fuelCost =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_COST+" FROM "+DATABASE_TABLE_NAME_MILEAGE;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuelCost.add(cursor.getDouble(cursor.getColumnIndex(KEY_MILEAGE_COST))+"");

		}

		cursor.close();
		close();
		return fuelCost;

	}
	public  ArrayList<String> fuelCost(int id,int carID)
	{
		ArrayList<String>  fuelCost =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGE_COST+" FROM "+DATABASE_TABLE_NAME_MILEAGE+" WHERE "+KEY_MILEAGEID+"="+id+" and "+KEY_MILEAGE_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			fuelCost.add(cursor.getDouble(cursor.getColumnIndex(KEY_MILEAGE_COST))+"");

		}

		cursor.close();
		close();
		return fuelCost;

	}

	public long getMileageBreakRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME_MILEAGE);
		close();
		return rowCount ;
	}

	
	public long getMileageBreakRowCount(int carId)
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		
		
		long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME_MILEAGE +" WHERE "+KEY_MILEAGE_CARID+ " = "+carId;
		
		try
		{
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();
		close();
		}
		catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		/*open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME_MILEAGE);
		close();*/
		return rowcount ;
	}


	/*
	 * 
	 */

	public double totalFuel(int mId,int carID)
	{
		open();
		double sum=0;
		Cursor cursor = sqlDataBase.rawQuery(
				"SELECT SUM(FUEL) FROM "+DATABASE_TABLE_NAME_MILEAGE+" where "+ KEY_MILEAGEID + "= "+mId+" and "+KEY_MILEAGE_CARID+" = "+carID, null);
		if(cursor.moveToFirst()) {
			sum= cursor.getDouble(0);
		}
		cursor.close();
		close();
		return sum;

	}

	public double totalFuelCost(int mId,int carID)
	{
		open();
		double sum=0;
		Cursor cursor = sqlDataBase.rawQuery(
				"SELECT SUM(COST) FROM "+DATABASE_TABLE_NAME_MILEAGE+" where "+ KEY_MILEAGEID + "= "+mId+" and "+KEY_MILEAGE_CARID+" = "+carID, null);
		if(cursor.moveToFirst()) {
			sum= cursor.getDouble(0);
		}
		cursor.close();
		close();
		return sum;

	}


	/*
	 * Updating data of MILEAGE Table for particular Id
	 *
	 */
	public void updateFuelById(int id,String date, String fuel,String cost)
	{

		ContentValues cv=new ContentValues();
		cv.put(KEY_MILEAGEID, -10);
		cv.put(KEY_MILEAGE_DATE,date);
		cv.put(KEY_MILEAGE_FUEL, fuel);
		cv.put(KEY_MILEAGE_COST, cost);
		/*cv.put(KEY_MILEAGE_CARID, 1);*/


		try
		{
			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGE, cv, KEY_ROWID_MILEAGE + "=" + id, null);
		}
		catch(SQLException sqex)
		{
			sqex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}



	}

	/*
	 * Getting Data By ID
	 */

	public ArrayList<String> getFuelDetailsById(int id)
	{
		ArrayList<String> fuelDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGE,KEY_MILEAGEID,KEY_MILEAGE_DATE,
				KEY_MILEAGE_FUEL,KEY_MILEAGE_COST,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGE,columns,KEY_ROWID_MILEAGE+ "=" + id,null,null,null,null

					);

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGE);
			int iMilId=cursor.getColumnIndex(KEY_MILEAGEID);
			int iDate=cursor.getColumnIndex(KEY_MILEAGE_DATE);

			int iFuel=cursor.getColumnIndex(KEY_MILEAGE_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGE_COST);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{

				/*result.add(c.getString(iRowid)+"&"+c.getString(iDate)+"~"+c.getString(iCost)+"#"+c.getString(iType)+"*");*/
				fuelDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iMilId)+"~"+cursor.getString(iDate)+"#"+cursor.getString(iFuel)+"*"+cursor.getString(iMCost)+"|");

			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return fuelDetails;
	}

	public void deleteFuelById(int id)
	{

		open();
		sqlDataBase.delete(DATABASE_TABLE_NAME_MILEAGE, KEY_ROWID_MILEAGE+"="+id, null);
		close();
	}
	public void deleteFuelByMileageId(int id)
	{
		open();
		sqlDataBase.delete(DATABASE_TABLE_NAME_MILEAGE, KEY_MILEAGEID+"="+id, null);
		close();
	}


	/*
	 * MILEAGECALC
	 */

	public long createMileageCalcEntery(long startDate,long endDate,double startKm, double endKm,double fuel,double cost,double mileage,int carId,int isopen) {
		// TODO Auto-generated method stub
		/*db.execSQL("CREATE TABLE  "+
		DATABASE_TABLE_NAME_MILEAGECAL +" ("+
		KEY_ROWID_MILEAGECALC +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
		KEY_MILEAGECALC_STARTDATE +" TEXT, "+
		KEY_MILEAGECALC_ENDDATE +" TEXT, "+
		KEY_MILEAGECALC_STARTKM +" TEXT, "+
		KEY_MILEAGECALC_ENDKM +" TEXT, "+
		KEY_MILEAGECALC_FUEL+" TEXT, " +
		KEY_MILEAGECALC_SELECTED+" TEXT, " +
		KEY_MILEAGECALC_COST+" TEXT, " +
		KEY_MILEAGECALC_AVG+" TEXT, " +
		KEY_MILEAGECALC_CARID + " INTEGER);"
		);*/



		ContentValues cv=new ContentValues();
		cv.put(KEY_MILEAGECALC_STARTDATE,startDate);
		cv.put(KEY_MILEAGECALC_ENDDATE,endDate);
		cv.put(KEY_MILEAGECALC_STARTKM,startKm);
		cv.put(KEY_MILEAGECALC_ENDKM,endKm);
		cv.put(KEY_MILEAGECALC_FUEL, fuel);
		cv.put(KEY_MILEAGECALC_SELECTED, "0");
		cv.put(KEY_MILEAGECALC_COST, cost);
		cv.put(KEY_MILEAGECALC_MILEAGE, mileage);
		cv.put(KEY_MILEAGECALC_CARID, carId);
		cv.put(KEY_ISOPEN, isopen);
		return sqlDataBase.insert(DATABASE_TABLE_NAME_MILEAGECAL, null, cv);
	}




	public ArrayList<String> getMileageDetails(int carId)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_MILEAGECALC_CARID+ "="+carId,null,null,null,null);

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMileage=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMileage)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}

	/*
	 * Getting Data From MILEAGE by id
	 */
	public ArrayList<String> getMileageDetailsById(int id)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_ROWID_MILEAGECALC+ "=" + id,null,null,null,null);

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMileage=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMileage)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}


	/*
	 * Check if any session open or not
	 */
	public boolean isMileageSessionOpen()
	{


		int status;
		boolean isOpen=false;
		String query="SELECT "+KEY_ISOPEN+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			status=cursor.getInt(cursor.getColumnIndex(KEY_ISOPEN));
			if(status==1)
			{
				isOpen=true;
				break;
			}
			else
			{
				isOpen=false;
			}
		}

		cursor.close();
		close();
		return isOpen;

	}
	
	
	public boolean isMileageSessionOpen(int carId)
	{


		int status;
		boolean isOpen=false;
		String query="SELECT "+KEY_ISOPEN+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carId;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			status=cursor.getInt(cursor.getColumnIndex(KEY_ISOPEN));
			if(status==1)
			{
				isOpen=true;
				break;
			}
			else
			{
				isOpen=false;
			}
		}

		cursor.close();
		close();
		return isOpen;

	}
	

	public int mileageRowId()
	{
		int rowid = 0;

		String query="SELECT "+KEY_ROWID_MILEAGECALC+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_ISOPEN+"=1";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			rowid=cursor.getInt(cursor.getColumnIndex(KEY_ROWID_MILEAGECALC));

		}

		cursor.close();
		close();
		return rowid;

	}

	/*
	 * Updating data of MILEAGE Table for particular Id
	 *
	 */
	public void updateMilegeCalcById(int id,long startDate,long endDate,double startKm, double endKm,double fuel,double cost,double mileage,int isopen,int carID)
	{


		/*	
		db.execSQL("CREATE TABLE  "+
				DATABASE_TABLE_NAME_MILEAGECAL +" ("+
				KEY_ROWID_MILEAGECALC +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
				KEY_MILEAGECALC_STARTDATE +" TEXT, "+
				KEY_MILEAGECALC_ENDDATE +" TEXT, "+
				KEY_MILEAGECALC_STARTKM +" REAL, "+
				KEY_MILEAGECALC_ENDKM +" REAL, "+
				KEY_MILEAGECALC_FUEL+" REAL, " +
				KEY_MILEAGECALC_SELECTED+" TEXT, " +
				KEY_MILEAGECALC_COST+" REAL, " +
				KEY_MILEAGECALC_MILEAGE+" REAL, " +
				KEY_MILEAGECALC_CARID + " INTEGER, "+
				KEY_ISOPEN + " INTEGER );"
				);*/


		ContentValues cv=new ContentValues();
		cv.put(KEY_MILEAGECALC_STARTDATE,startDate);
		cv.put(KEY_MILEAGECALC_ENDDATE,endDate);
		cv.put(KEY_MILEAGECALC_STARTKM,startKm);
		cv.put(KEY_MILEAGECALC_ENDKM,endKm);
		cv.put(KEY_MILEAGECALC_FUEL, fuel);
		/*		cv.put(KEY_MILEAGECALC_SELECTED, "0");
		 */		cv.put(KEY_MILEAGECALC_COST, cost);
		 cv.put(KEY_MILEAGECALC_MILEAGE, mileage);
		 cv.put(KEY_ISOPEN, isopen);
		 cv.put(KEY_MILEAGECALC_CARID, carID);

		 try
		 {
			 sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGECAL, cv, KEY_ROWID_MILEAGECALC + "=" + id, null);
		 }
		 catch(SQLException sqex)
		 {
			 sqex.printStackTrace();
		 }catch(Exception ex)
		 {
			 ex.printStackTrace();
		 }



	}

	/*
	 * Gettind Data For Mileage Calculator
	 */

	public ArrayList<String> mileageID(int carID)
	{
		ArrayList<String>  id =new ArrayList<String>();

		String query="SELECT "+KEY_ROWID_MILEAGECALC +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			id.add(cursor.getString(cursor.getColumnIndex(KEY_ROWID_MILEAGECALC )));

		}

		cursor.close();
		close();
		return id;
	}

	public ArrayList<String> sessionStatus(int carID)
	{
		ArrayList<String>  isOpen =new ArrayList<String>();

		String query="SELECT "+KEY_ISOPEN +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			isOpen.add(cursor.getString(cursor.getColumnIndex(KEY_ISOPEN )));

		}

		cursor.close();
		close();
		return isOpen;
	}
	public int sessionStatus(int id,int carID)
	{
		int  isOpen =-1;

		String query="SELECT "+KEY_ISOPEN +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_ROWID_MILEAGECALC+" = "+id+" and "+KEY_MILEAGECALC_CARID+" = "+carID;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			isOpen=cursor.getInt(cursor.getColumnIndex(KEY_ISOPEN ));

		}

		cursor.close();
		close();
		return isOpen;
	}

	public ArrayList<Long> mileageStartDate(int carID)
	{
		ArrayList<Long>  startDate =new ArrayList<Long>();

		String query="SELECT "+KEY_MILEAGECALC_STARTDATE +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			startDate.add(cursor.getLong(cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE )));

		}

		cursor.close();
		close();
		return startDate;
	}

	public ArrayList<Long> mileageEndDate(int carID)
	{
		ArrayList<Long>  endDate =new ArrayList<Long>();

		String query="SELECT "+KEY_MILEAGECALC_ENDDATE +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			endDate.add(cursor.getLong(cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE )));

		}

		cursor.close();
		close();
		return endDate;
	}

	public ArrayList<String> mileageStartkm(int carID)
	{
		ArrayList<String>  startKm =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGECALC_STARTKM +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			startKm.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM )));

		}

		cursor.close();
		close();
		return startKm;
	}



	public ArrayList<String> mileageEndkm(int carID)
	{
		ArrayList<String>  endKm =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGECALC_ENDKM +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			endKm.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM )));

		}

		cursor.close();
		close();
		return endKm;
	}



	public ArrayList<String> mileage(int carID)
	{
		ArrayList<String>  mileage =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGECALC_MILEAGE +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			mileage.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE )));

		}

		cursor.close();
		close();
		return mileage;
	}

	public ArrayList<String> mileageCost(int carID)
	{
		ArrayList<String>  mileagecost =new ArrayList<String>();

		String query="SELECT "+KEY_MILEAGECALC_COST +" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_CARID+" = "+carID+" ORDER BY "+KEY_ROWID_MILEAGECALC+" DESC ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			mileagecost.add(cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_COST )));

		}

		cursor.close();
		close();
		return mileagecost;
	}


	/*
	 * 
	 */


	public void deleteMileageById(int id)
	{
		try
		{
			open();
			sqlDataBase.delete(DATABASE_TABLE_NAME_MILEAGECAL, KEY_ROWID_MILEAGECALC+"="+id, null);
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void delete_MileagebyCarID(int carId){
		try
		{
			sqlDataBase.delete(DATABASE_TABLE_NAME, KEY_MILEAGECALC_CARID+"="+carId, null);
		}
		catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	/*
	 * Setting visiblity of selected field
	 */
	public void setMileageVisibility(String Id){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/
		String isvisible = "1";
		String notVisible = "0";
		if(logOn)
			Log.i("ID is : "," "+Id);
		visible.put(KEY_MILEAGECALC_SELECTED, isvisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{

			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGECAL, visible, KEY_ROWID_MILEAGECALC + "=" + Id, null);
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
	}


	public void setMileageInVisibility(String Id){
		/*		ContentValues visible = new ContentValues();*/
		ContentValues invisible = new ContentValues();
		/*String isvisible = "1";*/
		String notVisible = "0";
		/*visible.put(SELECTED, isvisible);*/
		invisible.put(KEY_MILEAGECALC_SELECTED, notVisible);
		try{

			/*sqlDataBase.update(DATABASE_NAME, visible, KEY_ROWID + "=" + Id, null);*/
			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGECAL, invisible, KEY_ROWID_MILEAGECALC + "=" + Id, null);

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
	}


	public void setMileageAllVisibile(int carId){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/
		String isvisible = "1";
		String notVisible = "0";

		visible.put(KEY_MILEAGECALC_SELECTED, isvisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{
			open();
			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGECAL, visible,KEY_MILEAGECALC_CARID+" = "+carId, null);
			/*KEY_ROWID_MILEAGECALC + "=" + Id*/
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}
		finally
		{
			close();
		}
	}

	public void setMileageAllInVisibile(int carId){
		ContentValues visible = new ContentValues();
		/*		ContentValues invisible = new ContentValues();*/

		String notVisible = "0";

		visible.put(KEY_MILEAGECALC_SELECTED, notVisible);
		/*invisible.put(SELECTED, notVisible);*/
		try{
			open();
			sqlDataBase.update(DATABASE_TABLE_NAME_MILEAGECAL, visible,KEY_MILEAGECALC_CARID+" = "+carId, null);
			/*sqlDataBase.update(DATABASE_NAME, invisible, CARID + "!=" + Id, null);*/

		}catch(Exception e)
		{
			if(logOn)
			{
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}

		}finally
		{
			close();
		}
	}


	public String selectMileageMode(String Id)
	{
		/*ContentValues selectMode = new ContentValues();
		String [] columns=new String[]{SELECTED};
		Cursor c=sqlDataBase.query(DATABASE_TABLE_NAME, columns, null, null, null, null, null);*/
		/*Cursor c1=sqlDataBase.query(DATABASE_TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy)*/

		String selectionMode="";

		String query="SELECT "+KEY_MILEAGECALC_SELECTED+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_ROWID_MILEAGECALC+"='" + Id+"'";

		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		if (cursor != null) {
			cursor.moveToFirst();
		}
		selectionMode=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED));
		cursor.close();
		return selectionMode;

	}

	/*
	 * Getting Avg of specific id for MILEAGE
	 */
	public String getAvgOfMileage(int Id)
	{
		double sum=0.0;
		int index=0;
		try
		{

			open();

			String total;

			String query="SELECT "+KEY_MILEAGECALC_MILEAGE+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_ROWID_MILEAGECALC+"=" + Id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
					index++;
				}

			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		//String result = String.format("%.2f", (sum/index));
		return sum+"";

	}




	/*
	 * Checking whether all rows are selected or not
	 * Returns true if  all rows selected else false;
	 */
	public boolean isAllMileageRowsSelected()
	{

		String selectionMode="";
		boolean isSelected=false;
		String query="SELECT "+KEY_MILEAGECALC_SELECTED+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			selectionMode=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED));
			if(selectionMode.equalsIgnoreCase("1"))
			{
				isSelected=true;
				Log.i("Selection Mode"," "+selectionMode);
			}
			else
			{
				isSelected=false;
				Log.i("Selection Mode"," "+selectionMode);
				break;

			}
		}

		cursor.close();
		close();
		Log.i("Selection Status"," "+selectionMode);
		return isSelected;
	}

	public boolean isAnyMileageRowsSelected()
	{

		String selectionMode="";
		boolean isSelected=false;
		String query="SELECT "+KEY_MILEAGECALC_SELECTED+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			selectionMode=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED));
			if(selectionMode.equalsIgnoreCase("1"))
			{
				isSelected=true;
				break;
			}
			else
			{
				isSelected=false;
			}
		}

		cursor.close();
		close();
		return isSelected;
	}

	public int selectCount()
	{

		String selectionMode="";
		int isSelected=0;
		String query="SELECT "+KEY_MILEAGECALC_SELECTED+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" WHERE "+KEY_MILEAGECALC_SELECTED+" = 1 ";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			selectionMode=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED));
			if(selectionMode.equalsIgnoreCase("1"))
			{
				isSelected++;

			}

		}

		cursor.close();
		close();
		return isSelected;
	}


	public long getMileageRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME_MILEAGECAL);
		close();
		return rowCount ;
	}

	public long getMileageRowCount(int carID)
	{
		long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME_MILEAGECAL +" WHERE "+KEY_MILEAGECALC_CARID+ " = "+carID;
		
		try
		{
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();
		close();
		}
		catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		/*	open();
		long rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, DATABASE_TABLE_NAME_MILEAGECAL);
		close();*/
		return rowcount ;
	}
	/*final String SQL_STATEMENT = "SELECT COUNT(*) FROM users WHERE uname=? AND pwd=?";*/


	/*
	 * Sum of AVG From MILEAGE
	 */

	public String getTotalAvgOfSelected()
	{
		double sum=0.0;
		try
		{

			open();

			String total;

			String query="SELECT "+KEY_MILEAGECALC_MILEAGE+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" where SELECTED=1";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			int index=0;
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				//data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";
				total=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
				}
				index++;
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return sum+"";

	}



	/*
	 * Sum of AVG From MILEAGE
	 */

	public String TotalAVG(int carID)
	{
		double sum=0.0;
		int count=0;
		try
		{

			open();

			String total;


			String query="SELECT "+KEY_MILEAGECALC_MILEAGE+" FROM "+DATABASE_TABLE_NAME_MILEAGECAL+" where "+KEY_MILEAGECALC_CARID+" = "+carID;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			int index=0;
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				//data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";
				total=cursor.getString(cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE));
				if(!(total.equalsIgnoreCase("")))
				{
					sum+=Double.parseDouble(total);
					count++;
				}
				index++;
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		/*String result = String.format("%.2f", (sum/count));*/
		String result = String.format("%.2f", (sum));
		/*String result = String.format("%.2f", sum);*/
		return result+"";

	}


	public ArrayList<String> sortByStartDate(int carID)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_MILEAGECALC_CARID+"="+carID,null,null,null,KEY_MILEAGECALC_STARTDATE+" desc");

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMAvg=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMAvg)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}

	public ArrayList<String> sortByEndDate(int carID)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_MILEAGECALC_CARID+"="+carID,null,null,null,KEY_MILEAGECALC_ENDDATE+" desc");

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMAvg=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMAvg)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}


	public ArrayList<String> sortByAvg(int carID)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_MILEAGECALC_CARID+"="+carID,null,null,null,KEY_MILEAGECALC_MILEAGE+" desc");

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMAvg=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMAvg)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}

	public ArrayList<String> sortByMileageCost(int carID)
	{
		ArrayList<String> MileageDetails = new ArrayList<String>();
		Cursor cursor;
		String [] columns= new String[]{KEY_ROWID_MILEAGECALC,KEY_MILEAGECALC_STARTKM,KEY_MILEAGECALC_ENDKM,KEY_MILEAGECALC_STARTDATE,
				KEY_MILEAGECALC_ENDDATE,KEY_MILEAGECALC_FUEL,KEY_MILEAGECALC_COST,KEY_MILEAGECALC_MILEAGE,KEY_MILEAGECALC_SELECTED,
				KEY_MILEAGE_CARID};
		try{
			cursor = sqlDataBase.query(

					DATABASE_TABLE_NAME_MILEAGECAL,columns,KEY_MILEAGECALC_CARID+"="+carID,null,null,null,KEY_MILEAGECALC_COST+" desc");

			int iRowId=cursor.getColumnIndex(KEY_ROWID_MILEAGECALC);
			int iStartKM=cursor.getColumnIndex(KEY_MILEAGECALC_STARTKM);
			int iEndKM=cursor.getColumnIndex(KEY_MILEAGECALC_ENDKM);
			int iStartDate=cursor.getColumnIndex(KEY_MILEAGECALC_STARTDATE);
			int iENDDate=cursor.getColumnIndex(KEY_MILEAGECALC_ENDDATE);
			int iFuel=cursor.getColumnIndex(KEY_MILEAGECALC_FUEL);
			int iMCost=cursor.getColumnIndex(KEY_MILEAGECALC_COST);
			int iMAvg=cursor.getColumnIndex(KEY_MILEAGECALC_MILEAGE);
			int iSelected=cursor.getColumnIndex(KEY_MILEAGECALC_SELECTED);
			int iMCarId=cursor.getColumnIndex(KEY_MILEAGE_CARID);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{


				MileageDetails.add(cursor.getString(iRowId)+"&"+cursor.getString(iStartKM)+"~"+cursor.getString(iEndKM)+"#"+cursor.getString(iStartDate)+"*"+cursor.getString(iENDDate)+"|"
						+cursor.getString(iFuel)+"&"+cursor.getString(iMCost)+"~"+cursor.getString(iMAvg)+"#"+cursor.getString(iSelected)+"*"+cursor.getString(iMCarId)+"|");
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return MileageDetails;
	}

	/*
	 * MY_CAR AND REMINDERS TABLE
	 */

	/*
	 * Use To add New Car In Database
	 * 
	 */


	public void addCar(
			String imagepathText,


			String modelText,
			String carNumberText,
			String mfgDateText,

			String RcNumberText,
			String insurerText,
			String policyText,

			String policyDateText,
			String chassisText,
			String buyDateText,
			String brand_Text

			)

	{
		ContentValues values = new ContentValues();

		values.put(imagepath,   imagepathText);

		values.put(model, 		modelText);
		values.put(carNumber, 	carNumberText);
		values.put(mfgDate, 	mfgDateText);
		values.put(RcNumber, 	RcNumberText);
		values.put(insurer, 	insurerText);
		values.put(policy, 		policyText);
		values.put(policyDate, 	policyDateText);
		values.put(chassis, 	chassisText);
		values.put(buyDate, 	buyDateText);
		values.put(selected, "1");
		values.put(brandText,   brand_Text);

		try{
			sqlDataBase.insert(tableName, null, values);
			Log.d("Data", "----" + carId + "----- "+ imagepathText + " " + modelText +" " + carNumberText + " " + mfgDateText + " " + RcNumberText + " " + insurerText + " " + policyText + " "  + policyDateText + " " + chassisText + " " + buyDateText);
			Log.d("Data", "Data Added Succefully");
			/*	Toast.makeText(context, "DATA ADDED SUCCEFULLY", Toast.LENGTH_SHORT).show();*/

		}catch(Exception e){
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

	}

	public int totalCars(){
		Cursor cursor;
		String selectQuery=("SELECT COUNT(" + carId+ ")" + "FROM " + tableName );
		cursor  = sqlDataBase.rawQuery(selectQuery, null);
		return cursor.getInt(0);
	}

	public void deleteCar(String carVal){
		try{
			sqlDataBase.delete(tableName, carId + "=" + carVal, null);
			Log.d("caVal","carVal " + carVal);
			Log.d("Delete", "DATA DELETED SUCCEFULLY");
		}catch(Exception e){
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * Use To Update the Car In Database the first parameter i.e car in following method is car id stored in databse
	 * 
	 */
	public void updateCar(
			String car,
			String imagepathText,


			String modelText,
			String carNumberText,
			String mfgDateText,

			String RcNumberText,
			String insurerText,
			String policyText,

			String policyDateText,
			String chassisText,
			String buyDateText,
			String brand_Text
			)


	{
		ContentValues values = new ContentValues();

		values.put(imagepath,   imagepathText);

		values.put(model, 		modelText);	
		values.put(carNumber, 	carNumberText);
		values.put(mfgDate, 	mfgDateText);
		values.put(RcNumber, 	RcNumberText);
		values.put(insurer, 	insurerText);
		values.put(policy, 		policyText);
		values.put(policyDate, 	policyDateText);
		values.put(chassis, 	chassisText);
		values.put(buyDate, 	buyDateText);
		values.put(brandText, brand_Text);

		try {
			sqlDataBase.update(tableName, values, carId + "=" + car, null);
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

	}

	public void setCarVisibility(String Id){
		ContentValues values = new ContentValues();
		ContentValues values1 = new ContentValues();
		String visible = "1";
		String noVisible = "0";
		values.put(selected, visible);
		values1.put(selected, noVisible);
		try{

			sqlDataBase.update(tableName, values, carId + "=" + Id, null);
			sqlDataBase.update(tableName, values1, carId + "!=" + Id, null);

		}catch(Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();

		}
	}

	public int getCarVisiblity()
	{
		String query="SELECT "+carId+" FROM "+tableName+" WHERE "+selected+"='" + 1+"'";

		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		if(cursor.moveToFirst()) {
			return cursor.getInt(0);
		}	
		return 0;

	}

	public ArrayList<Object> getCarAsArray(String id){

		ArrayList<Object> carArray = new ArrayList<Object>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					tableName, new String[]{imagepath,carId,model,carNumber,mfgDate,RcNumber,insurer,policy,policyDate,chassis,buyDate,brandText},carId + "=" + id,null,null,null,null,null
					);
			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{
					carArray.add(cursor.getString(0));
					carArray.add(cursor.getString(1));
					carArray.add(cursor.getString(2));

					carArray.add(cursor.getString(3));
					carArray.add(cursor.getString(4));
					carArray.add(cursor.getString(5));

					carArray.add(cursor.getString(6));
					carArray.add(cursor.getString(7));
					carArray.add(cursor.getString(8));
					carArray.add(cursor.getString(9));
					carArray.add(cursor.getString(10));
					carArray.add(cursor.getString(11));


				}while(cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return carArray;
	}

	
	
	public ArrayList<Object> getCarBrand(){

		ArrayList<Object> carArray = new ArrayList<Object>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					tableName, new String[]{brandText},null,null,null,null,null,null
					);
			
			
			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{
					carArray.add(cursor.getString(0));

				}while(cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return carArray;
	}
	public ArrayList<ArrayList<Object>> getAllRowsAsArrays(){
		ArrayList<ArrayList<Object>> dataArrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{imagepath,carId,model,
				carNumber,mfgDate,
				RcNumber,insurer,
				policy,policyDate,
				chassis,buyDate,selected,brandText};
		try{
			cursor = sqlDataBase.query(

					tableName,columns,null,null,null,null,null

					);

			int iImagePath=cursor.getColumnIndex(imagepath);
			int iCarId=cursor.getColumnIndex(carId);
			int iModel=cursor.getColumnIndex(model);

			int iCarNumber=cursor.getColumnIndex(carNumber);
			int imfgDate=cursor.getColumnIndex(mfgDate);
			int iRcNumber=cursor.getColumnIndex(RcNumber);

			int iInsurer=cursor.getColumnIndex(insurer);
			int iPolicy=cursor.getColumnIndex(policy);
			int iPolicyDate=cursor.getColumnIndex(policyDate);

			int iChassis=cursor.getColumnIndex(chassis);
			int iBuyDate=cursor.getColumnIndex(buyDate);
			int iSelected=cursor.getColumnIndex(selected);
			int iBrand=cursor.getColumnIndex(brandText);
			/*cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{*/
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ArrayList<Object> carDetails = new ArrayList<Object>();

				carDetails.add(cursor.getString(iImagePath));
				carDetails.add(cursor.getString(iCarId));
				carDetails.add(cursor.getString(iModel));

				carDetails.add(cursor.getString(iCarNumber));
				carDetails.add(cursor.getString(imfgDate));
				carDetails.add(cursor.getString(iRcNumber));

				carDetails.add(cursor.getString(iInsurer));
				carDetails.add(cursor.getString(iPolicy));
				carDetails.add(cursor.getString(iPolicyDate));

				carDetails.add(cursor.getString(iChassis));
				carDetails.add(cursor.getString(iBuyDate));
				carDetails.add(cursor.getString(iSelected));
				carDetails.add(cursor.getString(iBrand));


				dataArrays.add(carDetails);

				/*				}while(cursor.moveToNext());
				 */			}
			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return dataArrays;
	}






	/**
	 * The Following methods are useful in Reminders Tab
	 */

	public void addRemeinder(String rMessage, String rDate, String rTime, String rPreValue, String rPreUnit){

		ContentValues values = new ContentValues();

		values.put(messege_rem, rMessage);
		values.put(date_rem, 	rDate);
		values.put(time_rem, 	rTime);
		values.put(onoff,   "1");
		values.put(completed, "0");
		values.put(preValue, rPreValue);
		values.put(preUnit, rPreUnit);

		try{
			sqlDataBase.insert(table_reminder, null, values);
			Log.d("********Data**********", "----" + "----- "+ rMessage + " " + rDate +" " + rTime + " " + rPreValue + " " + rPreUnit );
		}catch(Exception e){
			Log.e(" REMEINDER DB ERROR ", e.toString());
			e.printStackTrace();
		}

	}

	public void moveToComplete(String id){

		ContentValues values = new ContentValues();
		values.put(completed, "1");


		try{
			sqlDataBase.update(table_reminder, values, remId + "="  + id, null);

		}catch(Exception e){
			Log.e(" Move to  Complete Error", e.toString());
			e.printStackTrace();
		}


	}
	public void moveToPending(String id){

		ContentValues values = new ContentValues();
		values.put(completed, "0");
		try{
			sqlDataBase.update(table_reminder, values, remId + "=" + id, null);

		}catch(Exception e){
			Log.e(" Move to  Complete Error", e.toString());
			e.printStackTrace();
		}
	}

	///// will return all pending reminders
	public ArrayList<ArrayList<Object>> getAllPendingRemeinders(){

		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT * FROM " + table_reminder + " WHERE " + completed+"=0";
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);


			int iRemId			=	cursor.getColumnIndex(remId);
			int iOnOff			=	cursor.getColumnIndex(onoff);
			int iMessage		=	cursor.getColumnIndex(messege_rem);

			int iDate			=	cursor.getColumnIndex(date_rem);
			int iTime			=	cursor.getColumnIndex(time_rem);

			int iPreValue		=	cursor.getColumnIndex(preValue);
			int iPreUnit		=	cursor.getColumnIndex(preUnit);



			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ArrayList<Object> reminderDetails = new ArrayList<Object>();

				reminderDetails.add(cursor.getString(iRemId));
				reminderDetails.add(cursor.getString(iOnOff));
				reminderDetails.add(cursor.getString(iMessage));

				reminderDetails.add(cursor.getString(iDate));
				reminderDetails.add(cursor.getString(iTime));

				reminderDetails.add(cursor.getString(iPreValue));
				reminderDetails.add(cursor.getString(iPreUnit));

				reminder_Arrays.add(reminderDetails);

			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminder_Arrays;
	}



	///// will return all completed reminders

	public ArrayList<ArrayList<Object>> getAllCompletedRemeinders(){

		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT * FROM " + table_reminder + " WHERE " + completed+"=1";
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);


			int iRemId		=	cursor.getColumnIndex(remId);
			int iOnOff		=	cursor.getColumnIndex(onoff);
			int iMessage	=	cursor.getColumnIndex(messege_rem);

			int iDate		=	cursor.getColumnIndex(date_rem);
			int iTime		=	cursor.getColumnIndex(time_rem);

			int iPreValue		=	cursor.getColumnIndex(preValue);
			int iPreUnit		=	cursor.getColumnIndex(preUnit);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ArrayList<Object> reminderDetails = new ArrayList<Object>();

				reminderDetails.add(cursor.getString(iRemId));
				reminderDetails.add(cursor.getString(iOnOff));
				reminderDetails.add(cursor.getString(iMessage));
				reminderDetails.add(cursor.getString(iDate));
				reminderDetails.add(cursor.getString(iTime));
				reminderDetails.add(cursor.getString(iPreValue));
				reminderDetails.add(cursor.getString(iPreUnit));

				reminder_Arrays.add(reminderDetails);

			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminder_Arrays;
	}
	public ArrayList<Object> getUpdate(){

		/*		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();*/
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT "+ onoff + " FROM " + table_reminder + " WHERE " + completed+"=0";
		ArrayList<Object> reminderDetails = new ArrayList<Object>();
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);
			int iOnOff		=	cursor.getColumnIndex(onoff);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				reminderDetails.add(cursor.getString(iOnOff));	
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminderDetails;
	}

	public ArrayList<Object> getIdUpdate(){

		/*		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();*/
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT "+ remId + " FROM " + table_reminder + " WHERE " + completed+"=0";
		ArrayList<Object> reminderDetails = new ArrayList<Object>();
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor 			= sqlDataBase.rawQuery(query, null);
			int iRemId		=	cursor.getColumnIndex(remId);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				reminderDetails.add(cursor.getString(iRemId));	
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminderDetails;
	}

	public ArrayList<ArrayList<Object>> getNewUpdate(){

		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT * FROM " + table_reminder + " WHERE " + completed+"=1";
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);


			int iRemId		=	cursor.getColumnIndex(remId);
			int iOnOff		=	cursor.getColumnIndex(onoff);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ArrayList<Object> reminderDetails = new ArrayList<Object>();

				reminderDetails.add(cursor.getString(iRemId));
				reminderDetails.add(cursor.getString(iOnOff));

				reminder_Arrays.add(reminderDetails);
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminder_Arrays;
	}



	public void deleteReminder(String Id){
		try{
			sqlDataBase.delete(table_reminder, remId + "=" + Id, null);
		}catch(SQLException ex){
			Log.d("DELETE REMINDER SQL EXCEPTION", ex.toString());
		}catch(Exception ex){
			Log.d("DELETING REMINDER ERROR", ex.toString());
		}

	}

	public void setAlarm(String id, int on){

		ContentValues values = new ContentValues();
		if(on == 1){
			values.put(onoff, "1");
		}else if(on == 0){
			values.put(onoff, "0");
		}
		try{
			sqlDataBase.update(table_reminder, values, remId + "=" + id, null);	

		}catch(Exception e){
			Log.e(" Move to  Complete Error", e.toString());
			e.printStackTrace();
		}
	}


}
