package com.phonethics.receiver;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.PushService;
import com.phonethics.neon.Actionbar;
import com.phonethics.neon.ContactUs;
import com.phonethics.neon.MycarActivity;
import com.phonethics.neon.Nissan_Map;
import com.phonethics.neon.ParseApplication;
import com.phonethics.neon.Remeinders;
import com.phonethics.neon.Tips;
import com.phonethics.neon.TrafficSigns;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import android.widget.Toast;

public class MyCustomReceiver extends BroadcastReceiver{


	private static final String TAG = "MyCustomReceiver";
	ParseApplication		parseAppClick;

	public static final String SHARED_PREFERENCES_NAME = "NissanGallery";
	public static final String TYPE = "type";
	public static final String BADGE = "badge_count";

	SharedPreferences NissanTab;
	Editor editor;




	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		//Toast.makeText(context, "Received",Toast.LENGTH_SHORT).show();


		parseAppClick = new ParseApplication();

		try {

			String action = intent.getAction();
			String channel = intent.getExtras().getString("com.parse.Channel");
			JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));


			String type =  json.getString("type");



			Log.d("TYPE", "TYPE" + type);


			ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);

			String packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();

			String class_running = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();



			if(packageName.equals("com.phonethics.neon")){


				if(type.equals("gallery")){

					//Toast.makeText(context, " type gallery ", 0).show();
					if(class_running.equals("com.phonethics.neon.Actionbar")){

						Actionbar.isInOther=true;
					}
					else{
						
						//Tips.isInOther = true;
						Nissan_Map.isInOther = true;
						Remeinders.isInOther = true;
						TrafficSigns.isInOther = true;
						MycarActivity.isInOther = true;
						ContactUs.isInOther = true;
					}

				}
				else{

					//Toast.makeText(context, " type tips ", 0).show();
					if(class_running.equals("com.phonethics.neon.Tips")){

						Actionbar.isInOther=true;
					}
					else{
						
						
					}
					
				}

			}else{


				if(type.equals("gallery")){

					//					Intent in = new Intent(context,Actionbar.class);
					//					in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					//					context.startActivity(in);
				}
				else{

					//					Intent in = new Intent(context,Tips.class);
					//					in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					//					context.startActivity(in);

					Intent tab_intent1 = new Intent();
					tab_intent1.setAction("tipsTab");
					context.sendBroadcast(tab_intent1);
				}
			}


		} catch (JSONException e) {
			Log.d(TAG, "JSONException: " + e.getMessage());
		}


	}

}
