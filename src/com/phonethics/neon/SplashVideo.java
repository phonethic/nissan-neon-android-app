package com.phonethics.neon;



import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;

public class SplashVideo extends Activity {
	private Context context;
	private static long SLEEP_TIME = 1; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		setContentView(R.layout.activity_splash_video);
		
		

		
		IntentLauncher launcher = new IntentLauncher();
		launcher.start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_remeinders, menu);
		return false;
	}

	private class IntentLauncher extends Thread {
		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */
		public void run() {
			try {
				// Sleeping
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {

			}

			// Start main activity
			Intent mainactivity = new Intent(context,  SplashScreen.class);
			startActivity(mainactivity);
			finish();
			overridePendingTransition(0, 0);

		}
	}	
	
}
