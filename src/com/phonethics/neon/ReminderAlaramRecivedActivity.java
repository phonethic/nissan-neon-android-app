package com.phonethics.neon;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.database.DataBaseUtil;
import com.phonethics.neon.R;
import com.phonethics.neon.Remeinders;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

public class ReminderAlaramRecivedActivity extends Activity {

	DataBaseUtil db;
	ArrayList<ArrayList<Object>> tempArr;
	boolean		toClear;
	ActivityManager activityManager;
	List<RunningAppProcessInfo> procInfos;
	String packageName;
	String className;
	String baseActivity;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL, LayoutParams.FLAG_NOT_TOUCH_MODAL);
		getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
		setContentView(R.layout.activity_reminder_alaram_recived);

		final Context context 		= this;

		Button okButton 			= (Button) findViewById(R.id.button_ok);
		Button cancelButton 		= (Button) findViewById(R.id.button_cancel);

		TextView text 				= (TextView) findViewById(R.id.textview_message);

		Bundle extras				= getIntent().getExtras();
		String title 				= extras.getString("message");
		toClear			= extras.getBoolean("ClearFlags");
		text.setText(title);
		activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		/*procInfos = 		activityManager.getRunningAppProcesses();*/
		packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
		className = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
		baseActivity = activityManager.getRunningTasks(1).get(0).baseActivity.getClassName();
		Log.d("--------------", "Base "+baseActivity);
		Log.d("--------------", "Package "+packageName);
		Log.d("--------------", "Class "+className);



		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub




				if(packageName.equals("com.phonethics.neon") && baseActivity.equals("com.phonethics.neon.ReminderAlaramRecivedActivity")){
					Intent intent = new Intent(context, Remeinders.class);
					startActivity(intent);
					finish();
				}else if(packageName.equals("com.phonethics.neon") ){

					if(!className.equals("com.phonethics.neon.Remeinders")){

						Intent intent = new Intent(context, Remeinders.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);		
						finish();
					}
					else{

						finish();
					}

				}else{


					Intent intent = new Intent(context, Remeinders.class);
					startActivity(intent);
					finish();

				}




			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				/*int pid = android.os.Process.myPid();
				   android.os.Process.killProcess(pid);*/

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_reminder_alaram_recived, menu);
		return true;
	}




}
