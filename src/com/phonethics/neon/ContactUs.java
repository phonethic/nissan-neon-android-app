package com.phonethics.neon;





import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ContactUs extends SherlockActivity implements OnClickListener {

	/*
	 * Tab bar 
	 * 
	 */
	ImageView imgNissanTab;
	ImageView imgToolsTab;
	ImageView imgTipsTab;
	ImageView imgMycarTab;
	ImageView imgContactTab;

	ImageView imgNissanCarHelpine;
	ImageView imgNissanTestDrive;

	Context context;
	Activity actContext;

	ArrayList<String> contactListData;

	public static ContactUs objContact;

	ListView			contactList;

	ActionBar ac;
	boolean	selfTabVisible =true;



	public static boolean call = false, clarNotify = false;
	/*public static String className = "";
	public static String BADGE = "";*/

	private NotificationManager mNotificationManager;

	//	static RelativeLayout lay_badgetip;
	//	static TextView badge_tips_text;
	//public static boolean notifyClear = false;

	static TextView badge_tips_text;
	static TextView badge_gel_text;

	static String type;
	static String badges="";
	public static boolean clarNotifyElse=false;
	RelativeLayout contactParent;
	ImageView    contactBack;
	Bitmap contactBack_bmp;
	private int bitmapWidth;
	private int bitmapHeight;



	String SHARED_PREFERENCES_MAP="FIRSTCUT";
	String SHARED_PREFERENCES_NAME_EXPENSES="SECONDCUT";
	String SHARED_PREFERENCES_NAME_MILEAGE="THRIRDCUT";
	String SHARED_PREFERENCES_NAME_REMINDERS="FOURTHCUT";
	String SHARED_PREFERENCES_NAME_TRAFFIC="FIFTHCUT";

	Map<String, String> articleParams = new HashMap<String, String>();
	private String gallery_type;
	private String gallery_badges;
	ArrayList<Integer> imagesArr;

	public static boolean isInOther = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_contact_us);

		context=this;
		actContext = this;
		objContact=this;

		//Flurry Event Log.
		articleParams.put("App", "Neon"); // Capture author info

		ac = getSupportActionBar();
		ac.setTitle("Contact Us");


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		imagesArr = new ArrayList<Integer>();

		imagesArr.add(R.drawable.cont);
		imagesArr.add(R.drawable.cont);
		imagesArr.add(R.drawable.cont);
		imagesArr.add(R.drawable.mail);
		imagesArr.add(R.drawable.fb);
		imagesArr.add(R.drawable.location_new);
		imagesArr.add(R.drawable.location_new);
		/*
		 * Car helpline and customer care
		 */

		/*		imgNissanCarHelpine=(ImageView)findViewById(R.id.nissanCarHelpline);
		imgNissanTestDrive=(ImageView)findViewById(R.id.nissanCustomerCare);


		 * 

		imgNissanCarHelpine.setOnClickListener(this);
		imgNissanTestDrive.setOnClickListener(this);*/


		contactList = (ListView) findViewById(R.id.listView_contact);

		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		contactList.setLayoutAnimation(controller);


		contactListData = new ArrayList<String>();
		contactListData.add("Call to book a test drive");
		contactListData.add("Call nissan customer support");
		contactListData.add("Call breakdown helpline");
		contactListData.add("Email customer support");
		contactListData.add("Nissan india on facebook");
		contactListData.add("Email breakdown location");
		contactListData.add("Dealer locator");

		ConatcAdapter adap = new ConatcAdapter(this, R.drawable.ic_launcher,  R.drawable.ic_launcher, contactListData, imagesArr);

		contactList.setAdapter(adap);

		contactList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				if(pos==6){
					
					FlurryAgent.logEvent("Dealer_StoreLocator_List_Event");
					startActivity(new Intent(context, StoreList.class));
				}else if(pos==0){
					FlurryAgent.logEvent("TestDrive_Call_Event");
					AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Neon");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});

					AlertDialog alert=alertDialog.show();
				}else if(pos==1){
					FlurryAgent.logEvent("CustomerSupport_Call_Event");
					AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Neon");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});

					AlertDialog alert=alertDialog.show();
				}else if(pos == 3){
					try
					{
						FlurryAgent.logEvent("Customer_Email_Event");

						Intent email=new Intent(Intent.ACTION_SEND);

						email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
						/* email.putExtra("android.intent.extra.SUBJECT", "New Mail" + " " + number.getText().toString());*/
						email.putExtra("android.intent.extra.SUBJECT", "Inquiry for NISSAN" + " ");
						email.setType("text/plain");
						startActivity(Intent.createChooser(email, "Send mail..."));
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}else if(pos==2){
					FlurryAgent.logEvent("Breakdown_Call_Event");
					AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Neon");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});

					AlertDialog alert=alertDialog.show();
				}else if(pos==5){

					Intent intent=new Intent(context, MailLink.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else if(pos==4){
					FlurryAgent.logEvent("Nissan_Facebook_Page_Event");
					Intent intent=new Intent(context, WebViewActivity.class);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */


		SubMenu subMenu1 = menu.addSubMenu("Action Item");



		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Find My Car").setIcon(R.drawable.location);
		subMenu1.add("Reminders").setIcon(R.drawable.reminder);
		subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
		subMenu1.add("Tips").setIcon(R.drawable.tips);
		subMenu1.add("My Car").setIcon(R.drawable.mycar);
		if(selfTabVisible){
			subMenu1.add("Contact Us").setIcon(R.drawable.contact);
		}
		//subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Home"))
		{
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){
			
			FlurryAgent.logEvent("Tips_Tab_Event");
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}	
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}	

		return true;
	}

	public  int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}
		}

		/* if (height > reqHeight || width > reqWidth) {
			 inSampleSize = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
               (double) Math.max(height, width)) / Math.log(0.5)));
        }*/

		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}
	public  Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		/*// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inInputShareable=true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = true;
		return BitmapFactory.decodeResource(res, resId, options);	*/

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		/*
		 * Tab bar
		 */



		/*
		 * Car helpline and Testdrive customercare
		 * 
		 */

		/*if(v.getId()==imgNissanCarHelpine.getId())
		{

			AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setTitle("Neon");
			alertDialog.setMessage("Do you want to call on this Number?");
			alertDialog.setCancelable(true);
			alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Intent call = new Intent(android.content.Intent.ACTION_CALL);
					call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
					startActivity(call);
				}
			});	
			alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			AlertDialog alert=alertDialog.show();
		}
		if(v.getId()==imgNissanTestDrive.getId())
		{

			AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setTitle("Neon");
			alertDialog.setMessage("Do you want to call on this Number?");
			alertDialog.setCancelable(true);
			alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Intent call = new Intent(android.content.Intent.ACTION_CALL);
					call.setData(Uri.parse(getResources().getString(R.string.car_helpLine)));
					startActivity(call);
				}
			});	
			alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			AlertDialog alert=alertDialog.show();
		}*/


	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		/*
		 * Parse
		 */
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");
		
		if(isInOther){

			isInOther = false;
			finish();
		}

	}



	public static void setBadgeforTips(String badge) {
		// TODO Auto-generated method stub

		badge_tips_text.setText(badge);
		badge_tips_text.setVisibility(View.VISIBLE);


	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		FlurryAgent.onEndSession(this);

	}



	public void clearPrefernces()
	{
		SharedPreferences shrd1 = context.getSharedPreferences(SHARED_PREFERENCES_MAP, context.MODE_WORLD_WRITEABLE);
		shrd1.edit().clear().commit();

		SharedPreferences shrd2 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_EXPENSES, context.MODE_WORLD_WRITEABLE);
		shrd2.edit().clear().commit();

		SharedPreferences shrd3 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_MILEAGE, context.MODE_WORLD_WRITEABLE);
		shrd3.edit().clear().commit();

		SharedPreferences shrd4 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_REMINDERS, context.MODE_WORLD_WRITEABLE);
		shrd4.edit().clear().commit();

		SharedPreferences shrd5 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_TRAFFIC, context.MODE_WORLD_WRITEABLE);
		shrd5.edit().clear().commit();
	}





	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();

		/*try
		{
			objContact=null;
			System.gc();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}*/

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();

	}

	private void unbindDrawables(View view) {
		// TODO Auto-generated method stub

		Log.d("=============++++", "CALLED_HOME");
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof RelativeLayout) {
			for (int i = 0; i < ((RelativeLayout) view).getChildCount(); i++) {
				unbindDrawables(((RelativeLayout) view).getChildAt(i));
			}
			((RelativeLayout) view).removeAllViews();
		}

	}

	public static void setBadge(String badge) {
		// TODO Auto-generated method stub

		badge_gel_text.setVisibility(View.VISIBLE);
		badge_gel_text.setText(badge);

		clarNotify = true;
	}

	static class ConatcAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance;
		ArrayList<Integer> logo;
		Activity context;
		LayoutInflater inflate;

		public ConatcAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> store_name,ArrayList<Integer>logo) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.store_name=store_name;

			this.logo=logo;
			this.context=context;
			inflate=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.contactlayout,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtContact=(TextView)convertView.findViewById(R.id.txtSearchStoreName);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();

			hold.imgStoreLogo.setImageResource(logo.get(position));
			hold.txtContact.setText(store_name.get(position));




			return convertView;
		}
			static class ViewHolder
			{
				ImageView imgStoreLogo;
				TextView txtContact;
				TextView txtDistance;
			}


	}




}
