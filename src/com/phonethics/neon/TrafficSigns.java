package com.phonethics.neon;




import java.util.HashMap;
import java.util.Map;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.ImageAdapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TrafficSigns extends SherlockActivity {


	ActionBar ac;
	RelativeLayout parent;
	Activity context;
	TextView textview_Info;
	ImageView gridBack;
	Bitmap gridBack_bmp;
	TrafficSigns objtrf;
	Map<String, String> articleParams = new HashMap<String, String>();
	boolean selfTabVisible = true;
	
	public static boolean isInOther = false;

	public String[] str_ary = {
			"Straight No Entry", "No Parking",
			"No Stopping or Standing","Horns Prohibited",
			"School Ahead", "Narrow Road Ahead",
			"Narrow Bridge", "Right Hand Curve",
			"Left Hand Curve", "Compulsory Sound Horn",
			"One Way Signs" , "One Way Signs",
			"Vehicles Prohibited \n In Both Directions", "All Motor Vehicles Prohibited",
			"Tonga Prohibited", "Truck Prohibited",
			"Bullock Cart And \nHand Cart Prohibited", "Hand Cart Prohibited",
			"Bullock Cart Prohibited", "Cycle Prohibited",
			"Pedestrian Prohibited", "Right Turn Prohibited",
			"Left Turn Prohibited", "U-turn Prohibited",
			"Speed Limit", "Width Limit",
			"Height Limit", "Length Limit",
			"Load Limit", "Axe Load Limit",
			"Bus Stop", "Restriction End Sign",
			"Compulsory Keep Left", "Compulsory Turn Left",
			"Compulsory Turn Right Ahead", "Compulsory Ahead or Turn Left",
			"Compulsory Ahead or Turn Right", "Compulsory Ahead Only",
			"Compulsory Cycle Track" , "Move On",
			"Stop", "Give way",
			"Bump ahead", "Ferry",
			"Right HairPin Bend", "Left HairPin Bend",
			"Left Reverse Bend", "Right Reverse Bend",
			"Steep Ascent" , "Steep Descent",
			"Road Widens Ahead", "Slippery Ahead", "Cycle Crossing"
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_traffic_signs);
		GridView gridView = (GridView) findViewById(R.id.grid_view);

		parent = (RelativeLayout) findViewById(R.id.RootView);
		context=this;
		//Flurry Event Log.
		//articleParams.put("App", "Neon"); // Capture author info

		textview_Info=(TextView)findViewById(R.id.textview_Info);
		objtrf=this;

		ac = getSupportActionBar();
		ac.setTitle("Traffic Signs");

		ac.show();
		
		

		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		/*gridBack=(ImageView)findViewById(R.id.gridBack);
		gridBack_bmp=BitmapFactory.decodeResource(getResources(), R.drawable.published_page);
		gridBack.setImageBitmap(gridBack_bmp);
		 */
		// Instance of ImageAdapter Class
		gridView.setAdapter(new ImageAdapter(context));

		/**
		 * On Click event for Single Gridview Item
		 * */
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				
				FlurryAgent.logEvent("TrafficSigns_Event");
				
				textview_Info.setText(str_ary[position]+"");
				// Sending image id to FullScreenActivity
				/*Intent i = new Intent(getApplicationContext(), TrafficDetails.class);
				// passing array index
				i.putExtra("id", position);

				startActivity(i);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();*/
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(gridBack_bmp!=null)
		{
			gridBack_bmp.recycle();
			gridBack_bmp=null;
		}

		unbindDrawables(parent);
		System.gc();
	}



	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		/*try
		{
			objtrf=null;
			System.gc();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}*/

	}

	private void unbindDrawables(View view) {
		// TODO Auto-generated method stub

		Log.d("=============++++", "CALLED_TRAFFIC");
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof RelativeLayout) {
			for (int i = 0; i < ((RelativeLayout) view).getChildCount(); i++) {
				unbindDrawables(((RelativeLayout) view).getChildAt(i));
			}
			((RelativeLayout) view).removeAllViews();
		}

	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		/*		super.onBackPressed();*/
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");
		
		if(isInOther){
			
			isInOther = false;
			finish();
		}
	}


	public  int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}
		}

		/* if (height > reqHeight || width > reqWidth) {
			 inSampleSize = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
               (double) Math.max(height, width)) / Math.log(0.5)));
        }*/

		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}
	public  Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inInputShareable=true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = true;
		return BitmapFactory.decodeResource(res, resId, options);	
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		FlurryAgent.onEndSession(this);

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */


		SubMenu subMenu1 = menu.addSubMenu("Action Item");



		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Find My Car").setIcon(R.drawable.location);
		subMenu1.add("Reminders").setIcon(R.drawable.reminder);
		if (selfTabVisible) {
			subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
		}
		subMenu1.add("Tips").setIcon(R.drawable.tips);
		subMenu1.add("My Car").setIcon(R.drawable.mycar);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us"))
		{
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Find My Car"))
		{
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Home"))
		{
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips"))
		{
			FlurryAgent.logEvent("Tips_Tab_Event");
			
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}

		return true;
	}
}
