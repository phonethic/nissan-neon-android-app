package com.phonethics.neon;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class DealerViewPager extends FragmentActivity {
	
	ViewPager viewPager;
	ArrayList<String> 	imgUrlArr;
	Context	context;
	ImageView img_close;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dealer_view_pager);
		
		viewPager = (ViewPager) findViewById(R.id.viewPagerDealer);
		img_close = (ImageView) findViewById(R.id.img_close);
		Bundle extras	= getIntent().getExtras();
		if(extras!=null){
			imgUrlArr	=	extras.getStringArrayList("imgUrlArr");

		}
		
		for(int i =0; i<imgUrlArr.size();i++){
			Log.i("urls -- >", "Image Urls --->" + imgUrlArr.get(i));
		}
		
		DealerImageAdapter adapter = new DealerImageAdapter(getSupportFragmentManager(),context,imgUrlArr);
		viewPager.setAdapter(adapter);
		
		
		img_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_dealer_view_pager, menu);
		return true;
	}

	
	public  class DealerImageAdapter extends FragmentPagerAdapter
	{

		Context context;
		ArrayList<String> url=null;
		public DealerImageAdapter(FragmentManager fm,Context context,ArrayList<String>url) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.url=url;
			
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			
			return new DealerImageFragment(context, url.get(position));

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return url.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}



	}	
}
