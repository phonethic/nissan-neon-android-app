package com.phonethics.neon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class NearMe extends SherlockFragmentActivity implements LocationListener,OnClickListener {


	Context				context;
	Activity			actContext;
	GoogleMap 			googleMap;
	SupportMapFragment	mapFrag;
	String 				provider,DealersName;
	ArrayList<String> 	latArr,phone1,phone2,mobile1,mobile2,fax,email,website,extraDetails,photo1,photo2,imgUrlArr;
	ArrayList<String> 	longArr;
	ArrayList<String>	dealersName;
	ArrayList<String>	dealersAdd;
	double cur_latitude, cur_longitude;
	LatLng current_pos, latlngarry[];
	float distance[];
	double indLat;
	double indLong = 0;
	Marker markers[];
	int count ;

	ActionBar ac;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_near_me);

		ac = getSupportActionBar();
		ac.setTitle("Dealers Near Me");

		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();




		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		context		= 	this;
		actContext	= 	this;
		ConnectivityManager cm 			= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo 			= cm.getActiveNetworkInfo();
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		/*phone1				= new ArrayList<String>();
		phone2				= new ArrayList<String>();
		mobile1				= new ArrayList<String>();
		mobile2				= new ArrayList<String>();
		fax					= new ArrayList<String>();
		email				= new ArrayList<String>();
		website				= new ArrayList<String>();*/

		extraDetails				= new ArrayList<String>();
		imgUrlArr				= new ArrayList<String>();

		mapFrag		= 	(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		googleMap	= 	mapFrag.getMap();
		googleMap.setMyLocationEnabled(true);


		if (netInfo != null && netInfo.isConnected()) {

			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(provider);

			if(location!=null){
				onLocationChanged(location);
			}
			else {

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Location Check");
				alertDialogBuilder3
				.setMessage("No location found. Please check GPS Settings")
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						NearMe.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		}else{

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			alertDialogBuilder.setTitle("Check Connection");
			alertDialogBuilder
			.setMessage("No internet Connection Do U Want To Exit ?")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					NearMe.this.finish();
				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}

		Bundle extras	= getIntent().getExtras();
		if(extras!=null){
			latArr	=	extras.getStringArrayList("latArr");
			longArr	=	extras.getStringArrayList("longArr");
			dealersName = extras.getStringArrayList("dealersName");
			dealersAdd = extras.getStringArrayList("dealerAddress");

			photo1 = extras.getStringArrayList("photo1");
			photo2 = extras.getStringArrayList("photo2");
			phone1 = extras.getStringArrayList("phone1");
			phone2 = extras.getStringArrayList("phone2");
			mobile1 = extras.getStringArrayList("mobile1");
			mobile2 = extras.getStringArrayList("mobile2");
			fax = extras.getStringArrayList("fax");
			website = extras.getStringArrayList("website");
			email = extras.getStringArrayList("email");



			Log.d("===", "Near Me ==> Name Size " + dealersName.size());
			Log.d("===", "Near Me ==> Lat Size " + latArr.size());
			Log.d("===", "Near Me ==> Long Size " + longArr.size());

		}


		for(int i=0;i<dealersName.size();i++){

			distance = new float[latArr.size()];

			indLat = 0;
			indLong = 0;

			if(!latArr.get(i).equals("") || !longArr.get(i).equals("")){

				try {

					indLat = Double.parseDouble(latArr.get(i));

					indLong= Double.parseDouble(longArr.get(i));

				} catch (NumberFormatException e) {
					// TODO: handle exception

					e.printStackTrace();
				} catch (Exception ex) {
					// TODO: handle exception

					ex.printStackTrace();
				}



			}


			Location dest = new Location("Destinartion");
			dest.setLatitude(indLat);
			dest.setLongitude(indLong);

			Location curr = new Location("MyLoc");
			curr.setLatitude(cur_latitude);
			curr.setLongitude(cur_longitude);

			distance[i] = curr.distanceTo(dest);

			distance[i] = distance[i]/1000 ;

			//Log.d("Distance Array", " Array -------------------" + distance[i]);

			//Log.d("Distance Array", " Array -------------------" + latArr.get(i));


			if(distance[i]<=100){

				count++;

				DealersName 	= dealersName.get(i);
				final String address 		= dealersAdd.get(i);

				final String sPhoto1 = photo1.get(i);
				String sPhoto2 = photo2.get(i);
				String sPhone1 = phone1.get(i);
				String sPhone2 = phone2.get(i);
				String sMobile1 = mobile1.get(i);
				String sMobile2 = mobile2.get(i);
				String sFax = fax.get(i);
				String sEmail = email.get(i);
				String sWebsite = website.get(i);



				extraDetails.add(sPhone1);
				extraDetails.add(sPhone2);
				extraDetails.add(sMobile1);
				extraDetails.add(sMobile2);
				extraDetails.add(sFax);
				extraDetails.add(sEmail);
				extraDetails.add(sWebsite);


				imgUrlArr.add(sPhoto1);
				imgUrlArr.add(sPhoto2);





				/*				Geocoder geoCoder = new Geocoder(
						getBaseContext(), Locale.getDefault());
				try {
					List<Address> addresses = geoCoder.getFromLocation(
							indLat, indLong, 1);

					if (addresses.size() > 0) {
						for (int index = 0; 
								index < addresses.get(0).getMaxAddressLineIndex(); index++)
							address += addresses.get(0).getAddressLine(index) + " ";
					}
				}catch (IOException e) {        
					e.printStackTrace();
				}   
				 */

				Marker marker = googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(indLat, indLong))
				.title(DealersName)
				.snippet(address));

				marker.setVisible(true);

				/* googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(Marker arg0) {
						// TODO Auto-generated method stub

						Toast.makeText(context, "Clicked "+ arg0.getTitle(), Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(context, Dealer_location.class);
						intent.putExtra("dlrName", DealersName);
						intent.putExtra("dlrAdd", address);
						intent.putExtra("imgUrl", sPhoto1);
						intent.putExtra("latitude", ""+indLat);
						intent.putExtra("longitude", ""+indLong);
						intent.putStringArrayListExtra("extraDetails", extraDetails);
						intent.putStringArrayListExtra("imgUrlsArr", imgUrlArr);
						intent.putExtra("header", "");
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						if(arg0.getTitle().equalsIgnoreCase(DealersName)){

					Toast.makeText(context, "true for " + DealersName, Toast.LENGTH_SHORT).show();
				}


					}
				});*/

			}

		}


		googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub

				//Toast.makeText(context, "Clicked "+ arg0.getTitle(), Toast.LENGTH_SHORT).show();
				/*Intent intent = new Intent(context, Dealer_location.class);
				intent.putExtra("dlrName", DealersName);
				intent.putExtra("dlrAdd", address);
				intent.putExtra("imgUrl", sPhoto1);
				intent.putExtra("latitude", ""+indLat);`
				intent.putExtra("longitude", ""+indLong);
				intent.putStringArrayListExtra("extraDetails", extraDetails);
				intent.putStringArrayListExtra("imgUrlsArr", imgUrlArr);
				intent.putExtra("header", "");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


				if(arg0.getTitle().equalsIgnoreCase("Torrent NISSAN")){

					Toast.makeText(context, "true for ritu", Toast.LENGTH_SHORT).show();
				}*/

				for(int i=0;i<dealersName.size();i++){
					String tempName = dealersName.get(i);
					if(arg0.getTitle().equalsIgnoreCase(tempName)){

						/*Toast.makeText(context, tempName, 0).show();*/
						String address = dealersAdd.get(i);
						String sPhoto1 = photo1.get(i);
						String sPhoto2 = photo2.get(i);
						String sPhone1 = phone1.get(i);
						String sPhone2 = phone2.get(i);
						String sMobile1 = mobile1.get(i);
						String sMobile2 = mobile2.get(i);
						String sFax = fax.get(i);
						String sEmail = email.get(i);
						String sWebsite = website.get(i);
						String sLat		= latArr.get(i);
						String sLong		= longArr.get(i);


						extraDetails  = new ArrayList<String>();
						extraDetails.add(sPhone1);
						extraDetails.add(sPhone2);
						extraDetails.add(sMobile1);
						extraDetails.add(sMobile2);
						extraDetails.add(sFax);
						extraDetails.add(sEmail);
						extraDetails.add(sWebsite);

						imgUrlArr = new ArrayList<String>();
						if(!sPhoto1.equalsIgnoreCase("")){
							imgUrlArr.add(sPhoto1);

						}
						if(!sPhoto2.equalsIgnoreCase("")){

							imgUrlArr.add(sPhoto2);
						}
						/*imgUrlArr.add(sPhoto1);
						imgUrlArr.add(sPhoto2);*/


						Intent intent = new Intent(context, Dealer_location.class);
						intent.putExtra("dlrName", tempName);
						intent.putExtra("dlrAdd", address);
						intent.putExtra("imgUrl", sPhoto1);
						intent.putExtra("latitude", ""+sLat);
						intent.putExtra("longitude", ""+sLong);
						intent.putStringArrayListExtra("extraDetails", extraDetails);
						intent.putStringArrayListExtra("imgUrlsArr", imgUrlArr);
						intent.putExtra("header", "");
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}

				}

			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}




	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		cur_latitude = location.getLatitude();

		// Getting longitude of the current location
		cur_longitude = location.getLongitude();

		// Creating a LatLng object for the current location
		current_pos = new LatLng(cur_latitude, cur_longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(current_pos));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));


		/*Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(cur_latitude, cur_longitude)));

		marker.setVisible(true);*/

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);


	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub


		if(item.getTitle().toString().equalsIgnoreCase("Dealers Near Me")){

			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}



		return true;


	}




}
