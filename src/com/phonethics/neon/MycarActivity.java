package com.phonethics.neon;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.ArrayAdapterList;
import com.phonethics.customclass.CarBrands;
import com.phonethics.database.DataBaseUtil;
import com.phonethics.flip.DisplayNextView;
import com.phonethics.flip.Flip3dAnimation;
import com.phonethics.network.NetworkCheck;
import com.phonethics.parser.MyCarParser;
import com.phonethics.wheel.ArrayWheelAdapter;
import com.phonethics.wheel.NumericWheelAdapter;
import com.phonethics.wheel.OnWheelChangedListener;
import com.phonethics.wheel.OnWheelScrollListener;
import com.phonethics.wheel.WheelView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.graphics.Color;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;


/*import com.flurry.android.FlurryAgent;
import com.parse.PushService;
import com.phonethics.database.DataBaseUtil;*/

public class MycarActivity extends SherlockActivity implements OnClickListener {







	Activity 				context;
	ImageView 				addcar, tabImage, captureImage, savebutton, rightArrrow, leftArrow, deletebutton, updatebutton, header;
	RelativeLayout 			layout, mycarLayout;
	DataBaseUtil 	db;
	EditText 				brandText,modeltext, plateNumbertext, mfgdatetext, rcNumbertext;
	EditText				insurertext, policytext, policyDatetext, chassistext, purchaseDatetext;
	Bitmap 					thumbnail, galleryThumbnail;
	String 					filemanagerstring, selectedImagePath, Path;
	String 					modeldata, platedata, mfgdata, rcdata,insurerdata,policydata,chassisdata,purchasedata,policynumberdata;
	ViewSwitcher 			view;
	Bitmap 					bm;
	ArrayList<String> 		img_Array, car_details, values, tempList;
	ListView 				mycarList;
	TextView 				textview, imagetext, swiptext;
	LayoutInflater 			inflator;
	ScrollView 				scroll;
	RelativeLayout.LayoutParams params;
	RelativeLayout.LayoutParams listParams;

	Calendar				c,mainCalendar;
	int 					startPosition = 1, height,endPosition, totalCars, counter = 1, DATE_DIALOG_ID = 0;
	int						Year, month, day,mYear,mMonth,mDay, mfgdate =0, policy =0,purchase =0,save=0;
	GestureDetector 		gestureDetector, gestureDetectorOnArrow;
	String 					carDatabaseId, imagePath, path;
	InputMethodManager 		imm;
	Dialog 					customDialog;

	TextView 				txt_info,txtHeaderMyCar;
	int 					takePicHieght,takePicWidth, no_entries = 0, add_car = 2, update_car = 0, to_view = 0;
	boolean					MFG= false, POLICY=false, PURCHASE = false, running,mediaCard = false;
	boolean					selfTabVisible  = true;
	WheelView 				dateWheel, monthWheel, yearWheel;
	Spinner					spinnerBrand, spinnerModel;
	List<CarBrands> 		brands 			= null;

	ArrayList<String> 		cars 			= null;
	ArrayList<String> 		carBrands 		= null;

	OnWheelScrollListener 		dateListener;
	OnWheelChangedListener 		changeListener;

	int 						carid;

	/*ImageView 					imgNissanTab;
	ImageView 					imgToolsTab;
	ImageView 					imgTipsTab;
	ImageView 					imgMycarTab;
	ImageView 					imgContactTab;*/


	ImageView 					mycarBackground;
	Bitmap 						mycarBackground_bmp;

	String months[] 			= new String[] {"January", "February", "March", "April", "May","June", "July", "August", "September", "October", "November", "December"};
	String MYCAR_URL;
	NetworkCheck				netCheck;
	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;
	FileInputStream				filename;

	String						xml , MYCAR_XML	="/sdcard/neoncache/myCar.xml";
	FileFromURL					downloadFile;
	int										CACHE_VALIDITY;
	ProgressDialog							pDialog;
	public static final int 				progress_bar_type = 1; 
	String						SELECTED_BRAND = "NISSAN";
	String						tempCarBrand = SELECTED_BRAND;
	String 						tempCar;

	public static boolean call = false;
	/*public static String className = "";
	public static String BADGE = "";*/

	private NotificationManager mNotificationManager;

	public static TextView badge_tips_text;
	public static TextView badge_gel_text;

	static String type;
	static String badges="";
	public static boolean clarNotify=false;
	public static boolean clarNotifyElse=false;


	ArrayAdapter<String> array1;

	MycarActivity objm;
	private int bitmapHeight;
	private int bitmapWidth;


	String SHARED_PREFERENCES_MAP="FIRSTCUT";
	String SHARED_PREFERENCES_NAME_EXPENSES="SECONDCUT";
	String SHARED_PREFERENCES_NAME_MILEAGE="THRIRDCUT";
	String SHARED_PREFERENCES_NAME_REMINDERS="FOURTHCUT";
	String SHARED_PREFERENCES_NAME_TRAFFIC="FIFTHCUT";

	Map<String, String> articleParams = new HashMap<String, String>();
	private String gallery_type;
	private String gallery_badges;

	ActionBar ac;

	public static boolean isInOther = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mycar);

		ac = getSupportActionBar();
		ac.setTitle("My Car");

		ac.show();


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);
		/*
		 * Tab bar 
		 */
		/*imgNissanTab		=	(ImageView)findViewById(R.id.tabNissan);
		imgTipsTab			=	(ImageView)findViewById(R.id.tabTips);
		imgToolsTab			=	(ImageView)findViewById(R.id.tabTools);
		imgMycarTab			=	(ImageView)findViewById(R.id.tabMyCar);
		imgMycarTab.setImageResource(R.drawable.mycar);
		imgContactTab		=	(ImageView)findViewById(R.id.tabContact);*/

		spinnerBrand		= (Spinner) findViewById(R.id.spinner_brand);
		spinnerModel		= (Spinner) findViewById(R.id.spinner_model);


		/*BitmapDrawable bdm=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.color);
		bitmapHeight = bdm.getIntrinsicHeight();
		bitmapWidth=bdm.getIntrinsicWidth();


		mycarBackground		=(ImageView)findViewById(R.id.mycarBackground);
		mycarBackground_bmp=decodeSampledBitmapFromResource(getResources(), R.drawable.published_page, bitmapWidth, bitmapHeight);

		mycarBackground.setImageBitmap(mycarBackground_bmp);*/
		/*	spinnerBrand.setClickable(false);
		spinnerModel.setClickable(false);
		 */

		objm=this;


		badge_tips_text = (TextView) findViewById(R.id.badge_tips);
		badge_gel_text = (TextView) findViewById(R.id.badge_gal);

		/*
		 * Regisering click listeners
		 */
		/*imgNissanTab.setOnClickListener(this);
		imgTipsTab.setOnClickListener(this);
		imgToolsTab.setOnClickListener(this);
		imgMycarTab.setOnClickListener(this);
		imgContactTab.setOnClickListener(this);*/

		CACHE_VALIDITY 		= Integer.parseInt(getResources().getString(R.string.CacheFileValidity));
		MYCAR_URL			= getString(R.string.my_car_url);

		txt_info			=	(TextView)findViewById(R.id.textInfo);
		txtHeaderMyCar		=	(TextView)findViewById(R.id.textview_headertextAddcar);
		imagetext 			=	(TextView) findViewById(R.id.textview_insideImage);
		swiptext			=	(TextView) findViewById(R.id.textview_swipe);




		context 			= this;
		netCheck			 = new NetworkCheck(context);
		cars 				= new ArrayList<String>();
		carBrands			= new ArrayList<String>();
		//Flurry Event Log.
		//articleParams.put("App", "Neon"); // Capture author info

		setUpViews();


		BitmapDrawable bd	=	(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.headerbg);
		height				=	bd.getIntrinsicHeight();


		car_details 		= 	new ArrayList<String>();
		tempList	 		= 	new ArrayList<String>();

		BitmapDrawable takePicbd	=	(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.capture_frame);
		takePicHieght		=	takePicbd.getIntrinsicHeight();
		takePicWidth		=	takePicbd.getIntrinsicWidth();

		params 				= new RelativeLayout.LayoutParams(takePicWidth,takePicHieght);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL);

		mycarList  			= (ListView) findViewById(R.id.listView_myCar);  ///// The list on front page of My car

		captureImage 		= (ImageView) findViewById(R.id.imageview_capture_image);
		//captureImage.setLayoutParams(params);

		//captureImage.setScaleType(ScaleType.FIT_XY);

		customDialog = new Dialog(context);


		String state = android.os.Environment.getExternalStorageState();


		/*
		 * Checking for micro sd card
		 * 18/2/2013
		 */

		if (state.equals(android.os.Environment.MEDIA_MOUNTED)) {
			/*	Toast.makeText(context, "Sd card Mounted", Toast.LENGTH_SHORT).show();*/
			mediaCard = true;
		}else{
			/*Toast.makeText(context, "Sd card Not Mounted", Toast.LENGTH_SHORT).show();*/
			mediaCard = false;
		}



		values 				= new ArrayList<String>(); 
		//values.add("Mileage Cal");
		values.add("Reminders");
		//values.add("Expense Cal");

		values.add("Car Brand");
		values.add("Model");
		values.add("Registration No");
		values.add("Purchase Date");
		values.add("MFG Date");

		values.add("RC No");
		values.add("Insurer Name");
		values.add("Policy No");

		values.add("Policy Date");
		values.add("Chassis No");
		/*values.add("Purchase Date");*/

		/*img_Array = new ArrayList<String>();

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,textview.getId(),values);*/


		getupdate(counter);
		update(counter);

		if(!(car_details.isEmpty())){
			mycarList.setAdapter(new ArrayAdapterList(this,values,car_details));
		}else{



			for(int position = 0; position < values.size(); position++){
				if(position < 1){
					car_details.add(">");
				}else{
					car_details.add(" ");
				}
			}
			mycarList.setAdapter(new ArrayAdapterList(this,values,car_details));
		}
		mycarList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				if(pos==0)
				{
					Intent intent=new Intent(context,Remeinders.class);
					intent.putExtra("mycar",carDatabaseId);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else{

					tabImage.performClick();
				}
				/*if(pos==1)
				{
					Intent intent=new Intent(context,Remeinders.class);
					intent.putExtra("mycar",carDatabaseId);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				if(pos==2)
				{
					Intent intent=new Intent(context,ExpenseCalculator.class);
					intent.putExtra("mycar",carDatabaseId);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}*/
			}
		});
		onClick();

		gestureDetector = new GestureDetector(new MyGestureDetector());
		gestureDetectorOnArrow = new GestureDetector(new MyGestureDetector());

		/*mycarLayout.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (gestureDetector.onTouchEvent(event)) {
					return false;
				} else {
					return false;
				}
			}
		});*/

		tabImage.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (gestureDetector.onTouchEvent(event)) {
					return true;
				} else {
					return false;
				}

			}
		});

		rightArrrow.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (gestureDetectorOnArrow.onTouchEvent(event)) {
					return true;
				} else {
					return false;
				}
			}
		});

		leftArrow.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (gestureDetectorOnArrow.onTouchEvent(event)) {
					return true;
				} else {
					return false;
				}
			}
		});




	}

	private void setUpViews(){



		/*try {
			brands = MyCa    rParser.parse(getAssets().open("mycar.xml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=0; i<brands.size();i++){
			carBrands.add(brands.get(i).getBrandName());
		}
		 */




		addcar 			= (ImageView)findViewById(R.id.image_addbutton);
		tabImage 		= (ImageView) findViewById(R.id.imageview_toEditUpdate);
		rightArrrow 	= (ImageView) findViewById(R.id.imageview_rightarrow);
		leftArrow 		= (ImageView) findViewById(R.id.imageview_leftarrow);
		deletebutton	= (ImageView) findViewById(R.id.imageview_deletebutton);
		header			= (ImageView) findViewById(R.id.header);


		layout 			= (RelativeLayout) findViewById(R.id.editcar_layout);
		mycarLayout		= (RelativeLayout) findViewById(R.id.myCar_Xml);

		view 			= (ViewSwitcher) findViewById(R.id.viewSwitcher);

		db 				= new DataBaseUtil(getApplicationContext());

		savebutton 		= (ImageView) findViewById(R.id.image_savebutton);
		updatebutton	= (ImageView) findViewById(R.id.image_updatebutton);

		imm 			= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		modeltext 			= (EditText) findViewById(R.id.edittext_model);
		plateNumbertext 	= (EditText) findViewById(R.id.edittext_plateNo);
		mfgdatetext 		= (EditText) findViewById(R.id.edittext_mfgDate);
		rcNumbertext 		= (EditText) findViewById(R.id.edittext_RCnumber);
		insurertext 		= (EditText) findViewById(R.id.edittext_InsurerName);
		policytext 			= (EditText) findViewById(R.id.edittext_policyNo);
		policyDatetext 		= (EditText) findViewById(R.id.edittext_policyDate);
		chassistext 		= (EditText) findViewById(R.id.edittext_chasisNo);
		purchaseDatetext 	= (EditText) findViewById(R.id.edittext_purchaseDate);
		brandText			= (EditText) findViewById(R.id.edittext_brand);

		modeltext.setCursorVisible(false);
		brandText.setCursorVisible(false);
		purchaseDatetext.setCursorVisible(false);
		mfgdatetext.setCursorVisible(false);

		modeltext.setInputType(InputType.TYPE_NULL);
		brandText.setInputType(InputType.TYPE_NULL);
		purchaseDatetext.setInputType(InputType.TYPE_NULL);
		mfgdatetext.setInputType(InputType.TYPE_NULL);
		rcNumbertext.setNextFocusDownId(R.id.edittext_InsurerName);

		inflator = getLayoutInflater();
		View view=inflator.inflate(R.layout.text_mycar_row, null);
		textview = (TextView) view.findViewById(R.id.Textview_mycar);
		scroll = (ScrollView) findViewById(R.id.scrollView_edit);

		/*cars.addAll(brands.get(0).allCarNames());
		ArrayAdapter<String> array = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, carBrands);
		spinnerBrand.setAdapter(array);
		ArrayAdapter<String> array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
		spinnerModel.setAdapter(array1);*/


		if(netCheck.isNetworkAvailable()){
			/*
			 * creating directory to store xml 
			 */
			File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

			if(!myDirectory.exists()) {                                 
				myDirectory.mkdirs();
			}
			File xmlFile=new File(MYCAR_XML);

			/*			if(mediaCard){*/

			if(xmlFile.exists() && xmlFile.length() != 0){

				Date lastModDate = new Date(xmlFile.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);
				if(diffDays > CACHE_VALIDITY){
					downloadFile=new FileFromURL();
					downloadFile.execute(MYCAR_URL);	
				}else{
					try{
						filename = new FileInputStream(MYCAR_XML);
						parseDoc(filename);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}

			}else{
				if(xmlFile.exists()){
					xmlFile.delete();
				}
				downloadFile=new FileFromURL();
				downloadFile.execute(MYCAR_URL);
			}
			/*}else{
				try{
					Log.i("Asset" , "Took from asset");
					parseDoc(getAssets().open("myCar.xml"));
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}*/
		}else{
			/*
			 * creating directory to store xml 
			 */
			File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

			if(!myDirectory.exists()) {                                 
				myDirectory.mkdirs();
			}
			File xmlFile=new File(MYCAR_XML);
			/*if(mediaCard){*/
			if(xmlFile.exists() && xmlFile.length()!=0)
			{
				try {
					filename=new FileInputStream(MYCAR_XML);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				parseDoc(filename);
			}
			else
			{

				/*ArrayAdapter<String> arr = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item);
				spinnerBrand.setAdapter(arr);
				spinnerModel.setAdapter(arr);
				spinnerBrand.setEnabled(false);*/

				/*spinnerBrand.setAdapter(null);*/
				try{
					Log.i("Asset" , "Took from asset");
					parseDoc(getAssets().open("myCar.xml"));
				}catch(Exception ex){
					ex.printStackTrace();
				}
				/*	Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
				 */
			}

			/*}*/
			/*else{
				try{
					Log.i("Asset" , "Took from asset");
					parseDoc(getAssets().open("myCar.xml"));
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}*/
		}
	}


	private void onClick(){

		/*spinnerBrand.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(brands.size()==0){
					Toast.makeText(context, "Need Internet Connection", Toast.LENGTH_SHORT).show();
				}
			}
		});*/

		addcar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				path = ""; 
				add_car  = 1;

				view.showNext();
				applyRotation(0, 90);
				updatebutton.setVisibility(View.GONE);
				savebutton.setVisibility(View.GONE);
				deletebutton.setVisibility(View.INVISIBLE);
				emptyFormFields();
				setPresentDate();
				getSherlock().dispatchInvalidateOptionsMenu();
				spinnerBrand.setSelection(getPosition("Nissan"));
				cars.addAll(brands.get(getPosition("Nissan")).allCarNames());
				array1 = null;
				array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
				spinnerModel.setAdapter(array1);

			}
		});

		/*if(!car_details.isEmpty()){*/
		/*tabImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				if(totalCars != 0){
					update_car = 1;
					updatebutton.setVisibility(View.VISIBLE);
					txtHeaderMyCar.setText("EDIT MY CAR");
					savebutton.setVisibility(View.GONE);

					view.showNext();


					applyRotation(0, 90);

				}else{
					path = ""; 
					add_car  = 1;

					view.showNext();

					applyRotation(0, 90);
					updatebutton.setVisibility(View.GONE);
					savebutton.setVisibility(View.VISIBLE);
					emptyFormFields();

				}
			}
		});	*/


		tabImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub

				if(totalCars != 0){

					update_car = 1;
					updatebutton.setVisibility(View.GONE);
					txtHeaderMyCar.setText("EDIT MY CAR");
					savebutton.setVisibility(View.GONE);
					spinnerModel.setSelection(cars.indexOf(tempCar));
					array1.notifyDataSetChanged();
					modeltext.setText(tempCar);


					/*spinnerBrand.setSelection(getPosition(tempCarBrand));
			cars.clear();
			cars.addAll(brands.get(getPosition(tempCarBrand)).allCarNames());
			ArrayAdapter<String> array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
			spinnerModel = null;
			spinnerModel = (Spinner) findViewById(R.id.spinner_model);
			spinnerModel.setAdapter(array1);
			int index=array1.getPosition(tempCar);
			spinnerModel.setSelection(index);*/


					view.showNext();
					view.showPrevious();
					view.showNext();
					applyRotation(0, 90);
					modeltext.setText(tempCar);
					deletebutton.setVisibility(View.VISIBLE);
					array1 = null;
					array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
					spinnerModel.setAdapter(array1);
					spinnerModel.setSelection(cars.indexOf(tempCar));
					array1.notifyDataSetChanged();
					modeltext.setText(tempCar);
					modeltext.setVisibility(View.VISIBLE);
					spinnerModel.setVisibility(View.GONE);


				}else{
					path = "";
					add_car = 1;

					view.showNext();

					applyRotation(0, 90);
					updatebutton.setVisibility(View.GONE);
					savebutton.setVisibility(View.GONE);
					emptyFormFields();
					setPresentDate();
					deletebutton.setVisibility(View.INVISIBLE);
					spinnerBrand.setSelection(getPosition("Nissan"));
					cars.addAll(brands.get(getPosition("Nissan")).allCarNames());
					array1 = null;
					array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
					spinnerModel.setAdapter(array1);



				}
				getSherlock().dispatchInvalidateOptionsMenu();
			}
		});



		/*}*/




		captureImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Neon");
				alertDialog.setMessage("Choose Image From :");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);

						startActivityForResult(intent, 10);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
				alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

						startActivityForResult(takePictureIntent, 2);	
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();
			}
		});




		// Call the save method on click

		savebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*updatebutton.setVisibility(View.VISIBLE);
				savebutton.setVisibility(View.GONE);*/
				/*save = 1;*/



				getCarDetails();
				getupdate(counter);
				update(counter);
				view.showPrevious();
				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				add_car  = 0;

				try
				{
					FlurryAgent.logEvent("NewCar_Added_Event");

					if(!brandText.getText().toString().equals(""))
					{
						//PushService.subscribe(context, brandText.getText().toString(), NissanTabActivity.class);
					}

				}
				catch(NullPointerException npx)
				{
					npx.printStackTrace();
				}
				catch(IllegalArgumentException ilg)
				{
					ilg.printStackTrace();
				}catch(Exception ex)
				{
					ex.printStackTrace();	
				}
				getSherlock().dispatchInvalidateOptionsMenu();
			}


			/*AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Neon");
				alertDialog.setMessage("Do you want to save these details ?");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						getCarDetails();
						getupdate(counter);
						update(counter);
						view.showPrevious();
						mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						add_car  = 0;

						try
						{
							FlurryAgent.logEvent("NewCar_Added_Event");

							if(!brandText.getText().toString().equals(""))
							{
								PushService.subscribe(context, brandText.getText().toString(), NissanTabActivity.class);
							}

						}
						catch(NullPointerException npx)
						{
							npx.printStackTrace();
						}
						catch(IllegalArgumentException ilg)
						{
							ilg.printStackTrace();
						}catch(Exception ex)
						{
							ex.printStackTrace();	
						}

					}
				});
				alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						view.showPrevious();
						add_car  = 0;
					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();


			}*/
		});

		updatebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Neon");
				alertDialog.setMessage("Do you want to update these details ?");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						updateParse();
						saveUpdateDetails(carDatabaseId);
						update(counter);
						view.showPrevious();
						if(!(car_details.isEmpty())){
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
							update_car = 0;
						}else{
							for(int position = 0; position < values.size(); position++){
								if(position < 1){
									car_details.add(">");
								}else{
									car_details.add(" ");
								}
							}
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
							update_car = 0;
						}
						getSherlock().dispatchInvalidateOptionsMenu();

						/*mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));*/
					}
				});
				alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						view.showPrevious();
						update_car = 0;
						getSherlock().dispatchInvalidateOptionsMenu();
					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();
			}
		});

		/**
		 * The following commented code is suppose to be handled through Animation 
		 * that is to switch between the next and previous car. 
		 */

		rightArrrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stubt
				/*view.showPrevious();*/
				counter++;
				Log.d("COUNTER VALUE", "COUNTER " + counter);
				/*getupdate(counter);
				update(counter);
				if(!(car_details.isEmpty())){
					mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				}else{
					for(int position = 0; position < values.size(); position++){
						if(position < 1){
							car_details.add(">");
						}else{
							car_details.add(" ");
						}
					}
					mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				}*/


				mycarLayout.startAnimation(inFromRightAnimation());
			}
		});

		leftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*view.showPrevious();*/
				counter--;
				if(counter <= 0){
					counter = 1;
				}
				/*getupdate(counter);
				update(counter);
				if(!(car_details.isEmpty())){
					mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				}else{
					for(int position = 0; position < values.size(); position++){
						if(position < 1){
							car_details.add(">");
						}else{
							car_details.add(" ");
						}
					}
					mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				}
				Log.d("COUNTER VALUE", "COUNTER " + counter);


				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
				 */
				mycarLayout.startAnimation(inFromLeftAnimation());
			}
		});



		deletebutton.setOnClickListener(new OnClickListener() {



			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if(no_entries == 0){
						Toast.makeText(getApplicationContext(), "There Are No Entries In Database ", Toast.LENGTH_SHORT).show();
					}else{*/

				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Neon");
				alertDialog.setMessage("Are you sure you want to delete this data ?");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						delete(carDatabaseId);
						counter--;
						if(counter <= 1){
							counter = 1;
						}
						getupdate(counter);
						update(counter);
						add_car = 0;
						update_car = 0;
						if(!(car_details.isEmpty())){
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}else{
							for(int position = 0; position < values.size(); position++){
								if(position < 1){
									car_details.add(">");
								}else{
									car_details.add(" ");
								}
							}
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}
						view.showPrevious();
						getSherlock().dispatchInvalidateOptionsMenu();
					}
				});


				alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();
			}
		});

		brandText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerBrand.performClick();
				imm.hideSoftInputFromWindow(brandText.getWindowToken(), 0);
			}
		});

		brandText.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					imm.hideSoftInputFromWindow(brandText.getWindowToken(), 0);

				}

			}
		});

		/*	modeltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerModel.performClick();

				imm.hideSoftInputFromWindow(modeltext.getWindowToken(), 0);
			}
		});*/

		modeltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				spinnerModel.setVisibility(View.INVISIBLE);
				spinnerModel.performClick();
				spinnerModel.setSelection(cars.indexOf(tempCar));

				imm.hideSoftInputFromWindow(modeltext.getWindowToken(), 0);
			}
		});



		modeltext.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					imm.hideSoftInputFromWindow(modeltext.getWindowToken(), 0);

				}

			}
		});

		spinnerBrand.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				brandText.setText(carBrands.get(pos));
				cars.clear();
				cars.addAll(brands.get(pos).allCarNames());
				array1 = null;
				array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
				spinnerModel.setAdapter(array1);
				/*spinnerModel.performClick();*/

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});


		spinnerModel.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				modeltext.setText(cars.get(pos));
				/*plateNumbertext.requestFocus();*/


			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});



		/*modeltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				scroll.scrollTo(0, modeltext.getHeight() + plateNumbertext.getHeight()+ mfgdatetext.getHeight()+ rcNumbertext.getHeight());
				spinnerModel.performClick();
				imm.hideSoftInputFromWindow(modeltext.getWindowToken(), 0);

				Log.d("Height", "" + plateNumbertext.getHeight()+" "+ mfgdatetext.getHeight()+" "+ rcNumbertext.getHeight());

				scroll.smoothScrollTo(0,modeltext.getHeight() + plateNumbertext.getHeight()+ mfgdatetext.
Height()+ rcNumbertext.getHeight());
				scroll.smoothScrollTo(0, (height*3));
			}
		});
		 */
		mfgdatetext.setOnFocusChangeListener(new View.OnFocusChangeListener() {


			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				mfgdate = 0;
				if (hasFocus) {
					mfgdate = 1;
					/*setTodaysDate();*/
					/*	showDialog(0);*/
					MFG =true;
					POLICY = false;
					PURCHASE = false;
					openDialog();



				}
			}
		});

		mfgdatetext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*setTodaysDate();*/
				/*showDialog(0);*/

				/*customDialog = new Dialog(context);
				View myView = LayoutInflater.from(context).inflate(R.layout.mycar_wheel, null);
				customDialog.setContentView(myView);
				customDialog.setTitle("Add MFG Date : ");
				customDialog.setCancelable(true);

				initwheel(myView);

				customDialog.show();*/
				MFG =true;
				POLICY = false;
				PURCHASE = false;
				openDialog();




			}
		});

		policyDatetext.setOnFocusChangeListener(new View.OnFocusChangeListener() {


			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				policy = 0;
				if (hasFocus) {
					policy = 1;
					/*setTodaysDate();*/
					/*showDialog(0);*/
					MFG =false;
					POLICY = true;
					PURCHASE = false;
					openDialog();

				}
			}
		});
		policyDatetext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*setTodaysDate();*/
				/*showDialog(0);*/
				MFG =false;
				POLICY = true;
				PURCHASE = false;
				openDialog();



			}
		});

		purchaseDatetext.setOnFocusChangeListener(new View.OnFocusChangeListener() {


			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				purchase = 0;
				if (hasFocus) {
					purchase = 1;
					/*setTodaysDate();*/
					/*	showDialog(0);*/
					MFG =false;
					POLICY = false;
					PURCHASE = true;
					openDialog();
				}
			}
		});
		purchaseDatetext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*setTodaysDate();*/
				/*showDialog(0);*/
				MFG =false;
				POLICY = false;
				PURCHASE = true;
				openDialog();

			}
		});

		plateNumbertext.setOnFocusChangeListener(new View.OnFocusChangeListener() {


			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				policy = 0;
				if (hasFocus) {
					policy = 1;
					/*setTodaysDate();*/
					/*showDialog(0);*/
					/*MFG =false;
					POLICY = true;
					PURCHASE = false;
					openDialog();
					 */

					plateNumbertext.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							keyboard.showSoftInput(plateNumbertext, 0);

						}
					}, 200);
				}
			}
		});

		rcNumbertext.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					/*Toast.makeText(context, "It has Focus", Toast.LENGTH_SHORT).show();*/
					if(customDialog.isShowing()){
						customDialog.dismiss();
					}
					imm.showSoftInput(rcNumbertext, 0);
					rcNumbertext.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							keyboard.showSoftInput(rcNumbertext, 0);

						}
					}, 200);
				}
			}
		});

		/*rcNumbertext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openDialog();
				imm.showSoftInput(rcNumbertext, 0);
				rcNumbertext.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						keyboard.showSoftInput(rcNumbertext, 0);

					}
				}, 200);
			}
		});*/

		chassistext.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					/*Toast.makeText(context, "It has Focus", Toast.LENGTH_SHORT).show();*/
					chassistext.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							keyboard.showSoftInput(chassistext, 0);

						}
					}, 200);
				}
			}
		});

		/*insurertext.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					Toast.makeText(context, "It has Focus", Toast.LENGTH_SHORT).show();
					imm.showSoftInput(insurertext, 0);
				}
			}
		});*/


	}


	public void clearAllFocus(){
		/*modeltext.clearFocus();
		plateNumbertext.clearFocus();
		mfgdatetext.clearFocus();
		rcNumbertext.clearFocus();
		insurertext.clearFocus();
		policytext.clearFocus();
		policyDatetext.clearFocus();
		chassistext.clearFocus();
		purchaseDatetext.clearFocus();
		brandText.clearFocus();*/


	}

	public void setPresentDate(){
		Calendar c1 = Calendar.getInstance();
		Date date = c1.getTime();
		SimpleDateFormat format = new SimpleDateFormat("dd MMMMM yyyy");
		String dateformat = format.format(date);
		mfgdatetext.setText(dateformat);
		purchaseDatetext.setText(dateformat);
		policyDatetext.setText(dateformat);

	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");


		SharedPreferences shrds = context.getSharedPreferences("NissanGallery", context.MODE_WORLD_READABLE);
		type = shrds.getString("type", "null");
		badges = shrds.getString("badge_count", "	null");

		SharedPreferences shrdss = context.getSharedPreferences("NissanGalleryTab", context.MODE_WORLD_READABLE);
		gallery_type = shrdss.getString("type", "null");
		gallery_badges = shrdss.getString("badge_count", "null");

		if(type.equals("null") || badges.equals("null")){

			/*	Toast.makeText(context, " " + type + "\n" + badges , Toast.LENGTH_LONG).show();*/
			badge_tips_text.setVisibility(View.GONE);
		}
		else{

			if(type.equals("tips")){

				/*		Toast.makeText(context, " " + type + "\n" + badges , Toast.LENGTH_LONG).show();*/
				badge_tips_text.setVisibility(View.VISIBLE);
				badge_tips_text.setText(badges+" ");
			}
			else{
				badge_tips_text.setVisibility(View.GONE);
			}


			/*Toast.makeText(context, " " + type + "\n" + badges , Toast.LENGTH_LONG).show();
			badge_tips_text.setVisibility(View.VISIBLE);
			badge_tips_text.setText(badges+" ");*/
		}
		/*if(!type.equals("null")){

			if(type.equals("gallery")){

				badge_gel_text.setVisibility(View.VISIBLE);
				badge_gel_text.setText(badges+" ");
			}
			else{
				badge_tips_text.setVisibility(View.VISIBLE);
				badge_tips_text.setText(badges+" ");
			}
		}
		 */

		if(call)
		{
			/*Toast.makeText(context, "inside mycar " + badges , Toast.LENGTH_LONG).show();*/
			if(!badges.equals(""))
			{
				if(type.equals("gallery")){

					/*	SharedPreferences shrd = context.getSharedPreferences("NissanGallery", context.MODE_WORLD_READABLE);
					type = shrd.getString("type", "null");
					badges = shrd.getString("badge_count", "null");


					badge_gel_text.setVisibility(View.GONE);
					 */

					/*badge_gel_text.setVisibility(View.VISIBLE);
					badge_gel_text.setText(badges+" ");*/
					//Toast.makeText(context, "gal ka h" , Toast.LENGTH_LONG).show();
				}
				else{	


					if(type == "null" || badges == "null"){

						/*		Toast.makeText(context, " " + type + "\n" + badges , Toast.LENGTH_LONG).show();*/
						badge_tips_text.setVisibility(View.GONE);
					}
					else{

						badge_tips_text.setVisibility(View.VISIBLE);
						badge_tips_text.setText(badges+" ");
						/*	Toast.makeText(context, "tps ka h" , Toast.LENGTH_LONG).show();*/
					}
				}
				call = false;


			}
		}
		if(clarNotify){

			NotificationManager mNotificationManager;

			mNotificationManager = 	(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

			mNotificationManager.cancelAll();

			clarNotify = false;
		}

		if(clarNotifyElse){

			NotificationManager mNotificationManager;

			mNotificationManager = 	(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

			mNotificationManager.cancelAll();

			clarNotifyElse = false;
		}

		if(gallery_type.equals("null") || gallery_badges.equals("null"))
		{
			badge_gel_text.setVisibility(View.GONE);
		}
		else
		{
			if(gallery_type.equals("gallery")){
				badge_gel_text.setVisibility(View.VISIBLE);
				badge_gel_text.setText(gallery_badges);
			}

		}


		if(isInOther){

			isInOther = false;
			finish();
		}

	}


	public static void setBadgeforTips(String badge) {
		// TODO Auto-generated method stub

		badge_tips_text.setText(badge);
		badge_tips_text.setVisibility(View.VISIBLE);


	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);

	}

	public static void setBadge(String badge) {
		// TODO Auto-generated method stub

		badge_gel_text.setVisibility(View.VISIBLE);
		badge_gel_text.setText(badge);

		clarNotify = true;
	}
	/**
	 * Showing Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			/*pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);*/
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.setCancelable(true);
			pDialog.show();


			pDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Log.d("------------","**** pDIALOG OFF *****");
					Log.d("------------","**** running off *****");
					try{
						File externalFile = new File(Environment.getExternalStorageDirectory(),"neoncache/myCar.xml");
						externalFile.delete();
						running = false;
						externalFile = new File(Environment.getExternalStorageDirectory(),"neoncache/myCar.xml");
						if(!externalFile.exists()){
							Log.d("------------","**** No Such File *****");
						}
					}catch(Exception ex){

					}
				}
			});

			return pDialog;
		default:
			return null;
		}
	}


	class FileFromURL extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(MYCAR_URL);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(MYCAR_XML);


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1) && (running == true)  ) {


					// publishing the progress....
					// After this onProgressUpdate will be called
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

					// writing data to file
					total += count;
					/*	publishProgress(""+(int)((total*100)/lenghtOfFile));*/
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if(running){
				dismissDialog(progress_bar_type);
				try {

					filename = new FileInputStream(MYCAR_XML);
					parseDoc(filename);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);
			publishProgress(""+(int)(0));
			running = true;
		}



	}

	private void initwheel(View dialog) {
		WheelView dateWheel =  (WheelView) dialog.findViewById(R.id.dateWheel_mycar);
		WheelView monthWheel =  (WheelView) dialog.findViewById(R.id.monthWheel_mycar);
		WheelView yearWheel =  (WheelView) dialog.findViewById(R.id.yearWheel_mycar);
		Button button = (Button) dialog.findViewById(R.id.ok_button);

		Calendar c	=	Calendar.getInstance();
		mYear		=	c.get(Calendar.YEAR);
		mMonth		=	c.get(Calendar.MONTH);
		mDay		=	c.get(Calendar.DAY_OF_MONTH);

		monthWheel.setViewAdapter(new DateArrayAdapter(context, months, mMonth));
		monthWheel.setCurrentItem(mMonth);

		yearWheel.setViewAdapter(new DateNumericAdapter(context, mYear-10, mYear + 10,10));
		yearWheel.setCurrentItem(10);

		updateDays(yearWheel, monthWheel, dateWheel);
		dateWheel.setCurrentItem(mDay-1);

		/*	monthWheel.addScrollingListener(dateListener);
		yearWheel.addScrollingListener(dateListener);
		dateWheel.addScrollingListener(dateListener);*/

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				customDialog.dismiss();
			}
		});






	}



	private void openDialog(){
		customDialog = new Dialog(context);

		customDialog.getWindow().setGravity(Gravity.BOTTOM);

		customDialog.setContentView(R.layout.mycar_wheel);
		if(MFG){
			customDialog.setTitle("Set MFG Date : ");
		}else if(POLICY){
			customDialog.setTitle("Set POLICY Date : ");
		}else if(PURCHASE){
			customDialog.setTitle("Set PURCHASE Date : ");
		}
		customDialog.setCancelable(true);


		changeListener = new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				updateDays(yearWheel, monthWheel, dateWheel);

			}
		};

		dateListener = new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub

			}
		};


		dateWheel =  (WheelView) customDialog.findViewById(R.id.dateWheel_mycar);
		monthWheel =  (WheelView) customDialog.findViewById(R.id.monthWheel_mycar);
		yearWheel =  (WheelView) customDialog.findViewById(R.id.yearWheel_mycar);
		Button button = (Button) customDialog.findViewById(R.id.ok_button);
		/*final Button next = (Button) customDialog.findViewById(R.id.next_button);*/

		c	=	Calendar.getInstance();
		mainCalendar = Calendar.getInstance();

		mYear		=	c.get(Calendar.YEAR);
		mMonth		=	c.get(Calendar.MONTH);
		mDay		=	c.get(Calendar.DAY_OF_MONTH);

		monthWheel.setViewAdapter(new DateArrayAdapter(context, months, mMonth));
		monthWheel.setCurrentItem(mMonth);
		monthWheel.addChangingListener(changeListener);
		monthWheel.addScrollingListener(dateListener);


		yearWheel.setViewAdapter(new DateNumericAdapter(context, mYear-10, mYear + 10,10));
		yearWheel.setCurrentItem(10);
		yearWheel.addChangingListener(changeListener);
		yearWheel.addScrollingListener(dateListener);


		updateDays(yearWheel, monthWheel, dateWheel);


		dateWheel.setCurrentItem(mDay - 1);
		dateWheel.addScrollingListener(dateListener);


		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int day = dateWheel.getCurrentItem()+1;
				String mth = months[monthWheel.getCurrentItem()];
				int year = yearWheel.getCurrentItem();
				if(year<10){
					year = mYear - (10-year);
				}else if(year>10){
					year = mYear + (year-10);
				}else if(year == 10){
					year = mYear;
				}
				if(MFG){

					/*Toast.makeText(context, "Inside MFG", Toast.LENGTH_SHORT).show();*/
					mfgdatetext.setText(day+ " " + mth + " " +year);
					customDialog.dismiss();

					/*customDialog.cancel();
					customDialog.dismiss();*/
					/*	customDialog = null;
					customDialog.dismiss();
					rcNumbertext.requestFocus();
					rcNumbertext.performClick();*/
					rcNumbertext.performClick();

				}else if(POLICY){

					policyDatetext.setText(day+ " " + mth + " " +year);
					chassistext.requestFocus();
					imm.showSoftInput(chassistext, 0);

				}else if(PURCHASE){

					purchaseDatetext.setText(day+ " " + mth + " " +year);
					customDialog.dismiss();
					mfgdatetext.requestFocus();
					/*mfgdatetext.performClick();*/
					/*	next.setText("Done");*/
				}

				customDialog.dismiss();
			}
		});
		customDialog.show();


	}





	/*	@Override    
	protected Dialog onCreateDialog(int id) 
	{
		switch (id) {
		case 0: 
			Calendar c	=	Calendar.getInstance();
			return new DatePickerDialog(this, mDateSetListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		}
		return null;    
	}
	 */
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() 
	{	

		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {

			Year 	= year;
			month 	= monthOfYear;
			day 	= dayOfMonth;
			/*Toast.makeText(getBaseContext(), "You have selected : " + (month + 1) + "/" + day + "/" + Year,Toast.LENGTH_SHORT).show();*/
			/*mfgdatetext.setText(new StringBuilder().append(mDay).append(" / ").append(mMonth+1).append(" / ").append(mYear));*/
			if(mfgdate == 1){
				mfgdatetext.setText(new StringBuilder().append(day).append(" / ").append(month+1).append(" / ").append(Year));
			}else if(policy == 1){
				policyDatetext.setText(new StringBuilder().append(day).append(" / ").append(month+1).append(" / ").append(Year));
			}else if(purchase == 1){
				purchaseDatetext.setText(new StringBuilder().append(day).append(" / ").append(month+1).append(" / ").append(Year));
			}
		}
	};

	/**
	 * Adapter for string based wheel. Highlights the current value.
	 */
	private class DateArrayAdapter extends ArrayWheelAdapter<String> {
		// Index of current item
		int currentItem;
		// Index of item to be highlighted
		int currentValue;

		/**
		 * Constructor
		 */
		public DateArrayAdapter(Context context, String[] items, int current) {
			super(context, items);
			this.currentValue = current;
			setTextSize(16);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			if (currentItem == currentValue) {
				view.setTextColor(0xFF0000F0);
			}
			view.setTypeface(Typeface.SANS_SERIF);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}

	/**
	 * Updates day wheel. Sets max days according to selected month and year
	 */



	void updateDays(WheelView year, WheelView month, WheelView day) {
		mainCalendar = Calendar.getInstance();
		mainCalendar.set(Calendar.YEAR, mainCalendar.get(Calendar.YEAR) + year.getCurrentItem());
		mainCalendar.set(Calendar.MONTH, month.getCurrentItem());

		int maxDays = mainCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		day.setViewAdapter(new DateNumericAdapter(this, 1, maxDays, mainCalendar.get(Calendar.DAY_OF_MONTH) - 1));
		int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
		day.setCurrentItem(curDay - 1, true);
		int today = mainCalendar.get(Calendar.DAY_OF_MONTH);
		Log.d("=============", "===== Listner Called ===== " + today);
	}


	/**
	 * Adapter for numeric wheels. Highlights the current value.
	 */
	private class DateNumericAdapter extends NumericWheelAdapter {
		// Index of current item
		int currentItem;
		// Index of item to be highlighted
		int currentValue;

		/**
		 * Constructor
		 */
		public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
			super(context, minValue, maxValue);
			this.currentValue = current;
			setTextSize(16);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			if (currentItem == currentValue) {
				view.setTextColor(0xFF0000F0);
			}
			view.setTypeface(Typeface.SANS_SERIF);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}




	/** The following method is used to give update on total entries of cars in database and accordingly it will set
	 * 	the visibility of Right arrow and left arrow image.
	 */
	private void getupdate(int countValue){
		ArrayList<ArrayList<Object>> cardetails = new ArrayList<ArrayList<Object>>();
		try{
			db.open();
			cardetails = db.getAllRowsAsArrays();
			db.close();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			db.close();
		}
		totalCars = cardetails.size();

		/*		totalCars = db.totalCars();*/

		Log.d("--------------------", "TOTAL CARS "+totalCars);

		if(to_view == 0){
			path="";
			to_view = 1;
			if(totalCars !=0){
				ArrayList<Object> rowData = new ArrayList<Object>();
				rowData = cardetails.get(countValue-1);
				Log.d("--------------------", "ROW DATA  Position 11" + "---" + rowData.get(11).toString() );
				while(countValue != totalCars+1){
					if((rowData.get(11).toString()).equalsIgnoreCase("1")){
						break;
					}
					countValue++;
					Log.d("--------------------", "CountValue " + "---" + countValue );
					rowData = cardetails.get(countValue-1);

				}
			}
		}


		counter = countValue;

		Log.d(" Inside GetUpdate Method", " Total CAR VALUES  " + cardetails.size());

		if(cardetails.isEmpty()){
			mycarList.setVisibility(View.GONE);
			swiptext.setVisibility(View.GONE);
			no_entries =1;
		}

		if((countValue == totalCars) && (countValue == startPosition)){
			leftArrow.setVisibility(View.GONE);
			rightArrrow.setVisibility(View.GONE);
			swiptext.setVisibility(View.GONE);

		}else if(countValue < totalCars && countValue > startPosition){
			rightArrrow.setVisibility(View.VISIBLE);
			leftArrow.setVisibility(View.VISIBLE);
			swiptext.setVisibility(View.VISIBLE);

		}else if(countValue == totalCars){
			rightArrrow.setVisibility(View.GONE);
			leftArrow.setVisibility(View.VISIBLE);
			swiptext.setVisibility(View.VISIBLE);

		}else if(countValue == startPosition && countValue < totalCars){
			rightArrrow.setVisibility(View.VISIBLE);
			leftArrow.setVisibility(View.GONE);
			swiptext.setVisibility(View.VISIBLE);
		}
	}


	private void update(int count){

		ArrayList<ArrayList<Object>> details = new ArrayList<ArrayList<Object>>();
		try{


			db.open();
			details = db.getAllRowsAsArrays();
			db.close();
			Log.d("Inside Update Method ", "Count " + count);



			if(details.isEmpty()){
				//Toast.makeText(getApplicationContext(), "There Are No Cars in Database..Add Some", Toast.LENGTH_SHORT).show();
				mycarList.setVisibility(View.INVISIBLE);
				txt_info.setVisibility(View.VISIBLE);
				deletebutton.setVisibility(View.INVISIBLE);
				swiptext.setVisibility(View.INVISIBLE);
				path="";
				showImage(path);
				car_details.clear();
				tabImage.setVisibility(View.INVISIBLE);



			}else{
				mycarList.setVisibility(View.VISIBLE);
				txt_info.setVisibility(View.GONE);
				deletebutton.setVisibility(View.VISIBLE);
				ArrayList<Object> row = new ArrayList<Object>();
				row = details.get(count-1);


				for(int i=0;i<row.size();i++)
				{
					Log.i("Key ",":"+row.get(i));
				}


				path = row.get(0).toString();
				showImage(path);


				carDatabaseId = row.get(1).toString();
				db.open();
				db.setCarVisibility(carDatabaseId);
				db.close();

				tempCar = row.get(2).toString();
				/*		Toast.makeText(context, "Temp car " + tempCar, Toast.LENGTH_SHORT).show();*/
				modeltext.setText(row.get(2).toString());
				plateNumbertext.setText(row.get(3).toString());
				mfgdatetext.setText(row.get(4).toString());

				rcNumbertext.setText(row.get(5).toString());
				insurertext.setText(row.get(6).toString());
				policytext.setText(row.get(7).toString());

				policyDatetext.setText(row.get(8).toString());
				chassistext.setText(row.get(9).toString());
				purchaseDatetext.setText(row.get(10).toString());
				brandText.setText(row.get(12).toString());

				spinnerBrand.setSelection(getPosition(row.get(12).toString()));
				cars.clear();
				cars.addAll(brands.get(getPosition(row.get(12).toString())).allCarNames());



				car_details.clear();
				//car_details.add(">");
				//car_details.add(">");
				car_details.add(">");
				car_details.add(row.get(12).toString());
				car_details.add(row.get(2).toString());

				car_details.add(row.get(3).toString());
				car_details.add(row.get(10).toString());
				car_details.add(row.get(4).toString());
				car_details.add(row.get(5).toString());

				car_details.add(row.get(6).toString());
				car_details.add(row.get(7).toString());
				car_details.add(row.get(8).toString());

				car_details.add(row.get(9).toString());
				/*car_details.add(row.get(10).toString());*/
			}
		}catch (Exception e)
		{
			Log.e("Add Error", e.toString());
			e.printStackTrace();
		}finally{
			db.close();
		}
	}

	private void getCarDetails(){
		try{
			db.open();
			db.addCar(
					path,

					modeltext.getText().toString(),

					plateNumbertext.getText().toString(),
					mfgdatetext.getText().toString(),

					rcNumbertext.getText().toString(),
					insurertext.getText().toString(),
					policytext.getText().toString(),

					policyDatetext.getText().toString(),
					chassistext.getText().toString(),
					purchaseDatetext.getText().toString(),
					brandText.getText().toString()
					);
			db.close();
			//	Toast.makeText(context, "DATA ADDED SUCCEFULLY", Toast.LENGTH_SHORT).show();
			/*view.showPrevious();*/
			emptyFormFields();
		}catch (Exception e)
		{
			Log.e("Add Error", e.toString());
			e.printStackTrace();
		}finally{
			db.close();
		}
	}


	/*private void updateCar(int count){
		try{
			ArrayList<Object> car = new ArrayList<Object>();
			car = db.getCarAsArray(count);

			modeltext.setText(car.get(0).toString());
			plateNumbertext.setText(car.get(1).toString());
			mfgdatetext.setText(car.get(2).toString());

			 .setText(car.get(3).toString());
			insurertext.setText(car.get(4).toString());
			policytext.setText(car.get(5).toString());

			policyDatetext.setText(car.get(6).toString());
			chassistext.setText(car.get(7).toString());
			purchaseDatetext.setText(car.get(8).toString());

		}catch (Exception e)
		{
			Log.e("Update Error", e.toString());
			e.printStackTrace();
		}
	}*/



	private void saveUpdateDetails(String count){
		try{
			db.open();
			db.updateCar(
					count,
					path,
					modeltext.getText().toString(),
					plateNumbertext.getText().toString(),
					mfgdatetext.getText().toString(),

					rcNumbertext.getText().toString(),
					insurertext.getText().toString(),
					policytext.getText().toString(),

					policyDatetext.getText().toString(),
					chassistext.getText().toString(),
					purchaseDatetext.getText().toString(),
					brandText.getText().toString());
			db.close();
			emptyFormFields();
		}catch (Exception e)
		{
			Log.e("Update Error", e.toString());
			e.printStackTrace();
		}
	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try
		{
			if(mycarBackground_bmp!=null)
			{
				mycarBackground_bmp.recycle();
				mycarBackground_bmp=null;
			}

			/*			objm=null;*/
			unbindDrawables(view);

			System.gc();
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		try
		{
			objm=null;
			System.gc();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void unbindDrawables(View view) {
		// TODO Auto-generated method stub

		Log.d("=============++++", "CALLED_HOME");
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof RelativeLayout) {
			for (int i = 0; i < ((RelativeLayout) view).getChildCount(); i++) {
				unbindDrawables(((RelativeLayout) view).getChildAt(i));
			}
			((RelativeLayout) view).removeAllViews();
		}

	}


	private void delete(String countVal){
		ArrayList<Object> carBrands = new ArrayList<Object>();
		try{
			Log.d("Counter Value", "Counter Value "+ countVal);
			/*Toast.makeText(context, "Delete Method", Toast.LENGTH_SHORT).show();*/
			db.open();
			carBrands = db.getCarBrand();
			String brand = "";
			String currentBrand = car_details.get(3);
			int tempCount=0;
			for(int i=0;i<carBrands.size();i++){
				Log.d("==========>>>>>", "" + carBrands.get(i).toString());
			}
			for(int i=0;i<carBrands.size();i++){
				if(currentBrand.equals(carBrands.get(i).toString())){
					tempCount++;
				}
			}
			if(tempCount==1){
				/*Toast.makeText(context, "Its Done", Toast.LENGTH_SHORT).show();*/
				try
				{
					//PushService.unsubscribe(context, currentBrand);
				}catch(Exception  ex)
				{
					ex.printStackTrace();
				}

			}
			db.deleteCar(countVal);
			db.delete_MileagebyCarID(Integer.parseInt(carDatabaseId));
			db.delete_byCarID(Integer.parseInt(carDatabaseId));
			db.close();
			emptyFormFields();
		}catch (Exception e)
		{
			Log.e("Delete Error", e.toString());
			e.printStackTrace();
		}

	}

	private void updateParse(){
		ArrayList<Object> carBrands = new ArrayList<Object>();
		boolean newBrandInMem = false;
		int tempCount=0;
		try{

			String currentBrandInMemory = car_details.get(3);
			String newBrandToUpdate = brandText.getText().toString();

			db.open();
			carBrands = db.getCarBrand();
			db.close();

			if(!currentBrandInMemory.equalsIgnoreCase(newBrandToUpdate)){
				for(int i=0;i<carBrands.size();i++){
					if(newBrandToUpdate.equals(carBrands.get(i).toString())){
						newBrandInMem = true;
						/*		/i = carBrands.size();/*/
					}
					if(currentBrandInMemory.equalsIgnoreCase(carBrands.get(i).toString())){
						tempCount++;
					}
				}
				if(!newBrandInMem){
					try{
						FlurryAgent.logEvent("NewCar_Added_Event");
						if(!brandText.getText().toString().equals("")){
							//PushService.subscribe(context, brandText.getText().toString(), NissanTabActivity.class);
						}

					}catch(NullPointerException npx){
						npx.printStackTrace();
					}catch(IllegalArgumentException ilg){
						ilg.printStackTrace();
					}catch(Exception ex){
						ex.printStackTrace();
					}

					/*/Toast.makeText(context, "Its a new Brand Parse will subscribe it", Toast.LENGTH_SHORT).show();/*/

				}
				if(tempCount==1){
					/*/Toast.makeText(context, "Parse will unsubscribe it", Toast.LENGTH_SHORT).show();/*/
					try
					{
						//PushService.unsubscribe(context, currentBrandInMemory);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}



		}catch(Exception ex){

		}
	}




	private void emptyFormFields(){



		captureImage.setImageResource(R.drawable.capture_frame);
		modeltext.setText("");
		plateNumbertext.setText("");
		mfgdatetext.setText("");

		rcNumbertext.setText("");
		insurertext.setText("");
		policytext.setText("");

		policyDatetext.setText("");
		chassistext.setText("");
		purchaseDatetext.setText("");
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub



		if(layout.isShown()){
			/*view.showPrevious();*/
			if(isBlank() == false){
				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Neon");
				alertDialog.setMessage("Do you want to save these details ?");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						if(add_car == 1){
							getCarDetails();
							add_car = 0;

							try
							{
								FlurryAgent.logEvent("NewCar_Added_Event");

								if(!brandText.getText().toString().equals(""))
								{
									//PushService.subscribe(context, brandText.getText().toString(), NissanTabActivity.class);
								}

							}
							catch(NullPointerException npx)
							{
								npx.printStackTrace();
							}
							catch(IllegalArgumentException ilg)
							{
								ilg.printStackTrace();
							}catch(Exception ex)
							{
								ex.printStackTrace();	
							}

						}else if(update_car == 1){
							updateParse();
							saveUpdateDetails(carDatabaseId);
							update_car = 0;
						}
						getupdate(counter);
						update(counter);

						if(!(car_details.isEmpty())){
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}else{
							for(int position = 0; position < values.size(); position++){
								if(position < 1){
									car_details.add(">");
								}else{
									car_details.add(" ");
								}
							}
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}
						view.showPrevious();
						getSherlock().dispatchInvalidateOptionsMenu();


					}
				});
				alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						getupdate(counter);
						update(counter);
						if(!(car_details.isEmpty())){
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}else{
							for(int position = 0; position < values.size(); position++){
								if(position < 1){
									car_details.add(">");
								}else{
									car_details.add(" ");
								}
							}
							mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
						}
						view.showPrevious();
						getSherlock().dispatchInvalidateOptionsMenu();
					}
				});
				AlertDialog alert = alertDialog.create();
				alert.show();
			}
			else if(isBlank() == true){
				view.showPrevious();
			}

		}else{
			clearPrefernces();
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);			
		}
	}


	public void clearPrefernces()
	{
		SharedPreferences shrd1 = context.getSharedPreferences(SHARED_PREFERENCES_MAP, context.MODE_WORLD_WRITEABLE);
		shrd1.edit().clear().commit();

		SharedPreferences shrd2 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_EXPENSES, context.MODE_WORLD_WRITEABLE);
		shrd2.edit().clear().commit();

		SharedPreferences shrd3 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_MILEAGE, context.MODE_WORLD_WRITEABLE);
		shrd3.edit().clear().commit();

		SharedPreferences shrd4 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_REMINDERS, context.MODE_WORLD_WRITEABLE);
		shrd4.edit().clear().commit();

		SharedPreferences shrd5 = context.getSharedPreferences(SHARED_PREFERENCES_NAME_TRAFFIC, context.MODE_WORLD_WRITEABLE);
		shrd5.edit().clear().commit();
	}




	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */
		MenuItem menu_save = menu.add("Add");
		if(layout.isShown()){
			menu_save.setIcon(R.drawable.save_button);
		}else{
			menu_save.setIcon(R.drawable.add_button);
		}
		menu_save.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


		SubMenu subMenu1 = menu.addSubMenu("Action Item");
		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Find My Car").setIcon(R.drawable.location);
		subMenu1.add("Remainder").setIcon(R.drawable.reminder);
		subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
		subMenu1.add("Tips").setIcon(R.drawable.tips);
		if(selfTabVisible){
			subMenu1.add("My Car").setIcon(R.drawable.mycar);
		}
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();

		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Remainder")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){

			FlurryAgent.logEvent("Tips_Tab_Event");

			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Add")){

			if(layout.isShown()){
				if(update_car==1){
					updatebutton.performClick();
				}else if(add_car==1){
					savebutton.performClick();
				}
			}else{
				addcar.performClick();
			}
		}

		return true;
	}


	public boolean isBlank(){
		if( 	(modeltext.getText().toString().equals("")) 	&& 	(plateNumbertext.getText().toString().equals("")) 	&& 
				(mfgdatetext.getText().toString().equals("")) 	&&	(rcNumbertext.getText().toString().equals("")) 		&& 
				(insurertext.getText().toString().equals("")) 	&& 	(policytext.getText().toString().equals("")) 		&&
				(policyDatetext.getText().toString().equals(""))&& (chassistext.getText().toString().equals("")) 		&& 
				(purchaseDatetext.getText().toString().equals("")) && path=="" ){
			/*Toast.makeText(context, "True-- All are blank", Toast.LENGTH_SHORT).show();*/

			return true;

		}else{
			/*Toast.makeText(context, "False-- All are Not blank", Toast.LENGTH_SHORT).show();*/
			return false;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(data == null || resultCode == 0){
			path="";
		}else if(requestCode == 2){

			/*layout.setBackgroundResource(R.drawable.published_page);*/
			thumbnail = (Bitmap) data.getExtras().get("data");
			final Uri contentUri = data.getData();
			path = getPath(contentUri);
			showImage(path);

			/*
			captureImage.setImageBitmap(thumbnail);
			tabImage.setImageBitmap(thumbnail);
			tabImage.setVisibility(View.VISIBLE);*/

		}

		else if(requestCode == 10){
			Uri selectedImage = data.getData();
			try {

				path = getRealPathFromURI(selectedImage);
				showImage(path);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	// And to convert the image URI to the direct file system path of the image file
	public String getRealPathFromURI(Uri contentUri) {
		// can post image
		String [] proj={MediaStore.Images.Media.DATA};
		Cursor cursor = getContentResolver().query(contentUri,
				proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}




	private void showImage(String receivedImage)   {


		if(!(path.equalsIgnoreCase("") )){


			Log.d("===", "My car path "+ path);
			//Toast.makeText(context, "path "+path, 0).show();
			Log.i("showImage","loading:"+path);
			imagetext.setVisibility(View.GONE);
			BitmapFactory.Options bfOptions=new BitmapFactory.Options();
			bfOptions.inDither=false;                     //Disable Dithering mode
			bfOptions.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
			bfOptions.inInputShareable=true;			//Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
			bfOptions.inSampleSize=calculateInSampleSize(bfOptions, 282, 155);


			bfOptions.inTempStorage=new byte[32 * 1024]; 

			File file=new File(path);
			FileInputStream fs=null;
			try {
				fs = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				//TODO do something intelligent
				e.printStackTrace();
			}

			try {
				if(fs!=null) bm=BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfOptions);
			} catch (IOException e) {
				//TODO do something intelligent
				e.printStackTrace();
			} finally{ 
				if(fs!=null) {
					try {
						fs.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//bm=BitmapFactory.decodeFile(path, bfOptions); This one causes error: java.lang.OutOfMemoryError: bitmap size exceeds VM budget


			Matrix matrix=new Matrix();

			ExifInterface exif = null;
			try {
				exif = new ExifInterface(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
			int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
			int rotateangle = 0;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_90) 
				rotateangle = 90;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_180) 
				rotateangle = 180;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_270) 
				rotateangle = 270;

			captureImage.setScaleType(ScaleType.CENTER_CROP);   //required
			matrix.setRotate(rotateangle);
			captureImage.setImageMatrix(matrix);



			Bitmap newBit = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);



			captureImage.setImageBitmap(newBit);

			//tabImage.setScaleType(ScaleType.CENTER_CROP);
			tabImage.setImageBitmap(newBit);
			tabImage.setVisibility(View.VISIBLE);
			//bm.recycle();
			//	bm=null;
		}else{
			//Log.d("===", "My car path "+ path);
			//Toast.makeText(context, "path 1 "+path, 0).show();
			captureImage.setImageResource(R.drawable.capture_frame);
			tabImage.setImageResource(R.drawable.capture_frame);

			if(car_details.isEmpty()){
				tabImage.setVisibility(View.INVISIBLE);
				//tabImage.setVisibility(View.VISIBLE);
			}else{
				tabImage.setVisibility(View.VISIBLE);
			}

			ArrayList<ArrayList<Object>> details = new ArrayList<ArrayList<Object>>();
			try{
				db.open();
				details = db.getAllRowsAsArrays();
				db.close();
				//Log.d("Inside Update Method ", "Count " + count);
			}catch(Exception ex){

			}
			if(details.isEmpty()){
				tabImage.setVisibility(View.INVISIBLE);
				//tabImage.setVisibility(View.VISIBLE);
			}else{
				tabImage.setVisibility(View.VISIBLE);
			}


			//imagetext.setVisibility(View.VISIBLE);
		}
	}


	public void parseDoc(FileInputStream fs){

		try
		{
			brands = MyCarParser.parse(fs);
			for(int i=0; i<brands.size();i++){
				carBrands.add(brands.get(i).getBrandName());
			}

			cars.addAll(brands.get(getPosition(tempCarBrand)).allCarNames());
			ArrayAdapter<String> array = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, carBrands);
			spinnerBrand.setAdapter(array);
			spinnerBrand.setSelection(getPosition(tempCarBrand));

			array1 = null;
			array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
			spinnerModel.setAdapter(array1);

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	public void parseDoc(InputStream fs){

		try
		{
			brands = MyCarParser.parse(fs);
			for(int i=0; i<brands.size();i++){
				carBrands.add(brands.get(i).getBrandName());
			}

			cars.addAll(brands.get(0).allCarNames());
			ArrayAdapter<String> array = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, carBrands);
			spinnerBrand.setAdapter(array);
			array1 = null;
			array1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, cars);
			spinnerModel.setAdapter(array1);
		}catch(NullPointerException npx)
		{

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	private int getPosition(String tempBrand){
		int value=0;
		/*Toast.makeText(context, "CAR brand Selected " + tempBrand , Toast.LENGTH_SHORT).show();*/
		/*if(calledForCarBrand){*/
		try
		{
			for(int i=0;i<brands.size();i++){
				if(tempBrand.equalsIgnoreCase(brands.get(i).getBrandName())){
					value = i;
					/*Toast.makeText(context, "Value matched at " + i, Toast.LENGTH_SHORT).show();*/
				}
			}

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		/*	}else{
			for(int i=0;i<cars.size();i++){
				if(tempBrand.equalsIgnoreCase(cars.get(i).toString())){
					value = i;
					Toast.makeText(context, "Value matched at " + i, Toast.LENGTH_SHORT).show();
				}
			}
		}*/

		return value;
	}


	public int getCarPosition(String tempCar){
		int val=0;
		Toast.makeText(context, "inside car position method", Toast.LENGTH_SHORT).show();
		for(int x=0;x<cars.size();x++){
			if(tempCar.equalsIgnoreCase(cars.get(x))){
				Toast.makeText(context, "inside car position method if condition", Toast.LENGTH_SHORT).show();
				val = x;
				break;
			}
		}
		return val;
	}


	public void recycleBitmap()
	{
		if(galleryThumbnail!=null)
		{
			galleryThumbnail.recycle();

			galleryThumbnail=null;
		}
		if(thumbnail!=null)
		{
			thumbnail.recycle();
			thumbnail=null;
		}
	}

	private String getPath(Uri selectedImage) {
		// TODO Auto-generated method stub

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub

		imm.hideSoftInputFromWindow(modeltext.getWindowToken(), 0);
		return super.onTouchEvent(event);
	}

	class MyGestureDetector extends SimpleOnGestureListener {



		private static final int SWIPE_MIN_DISTANCE =50;
		private static final int SWIPE_MAX_OFF_PATH = 700;
		private static final int SWIPE_THRESHOLD_VELOCITY = 7;

		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return true;
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				counter++;
				if(counter > totalCars){
					counter = totalCars;
				}else{
					mycarLayout.startAnimation(inFromRightAnimation());
					/*showImage(path);*/
				}
			}

			else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE  && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				counter--;
				if(counter <= 0){
					counter = 1;
				}else{
					mycarLayout.startAnimation(inFromLeftAnimation());
					/*showImage(path);*/
				}	
			}
			return true;

		}

		private Animation outToRightAnimation() {



			Animation outtoRight = new TranslateAnimation(
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, +1.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f);
			outtoRight.setDuration(300);
			outtoRight.setInterpolator(new AccelerateInterpolator());
			return outtoRight;

		}

		/*private Animation inFromLeftAnimation() {


			Animation inFromLeft = new TranslateAnimation(
					Animation.RELATIVE_TO_PARENT, -1.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f);
			inFromLeft.setDuration(300);
			inFromLeft.setInterpolator(new AccelerateInterpolator());

			getupdate(counter);
			update(counter);
			if(!(car_details.isEmpty())){
				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			}else{
				for(int position = 0; position < values.size(); position++){
					if(position < 1){
						car_details.add(">");
					}else{
						car_details.add(" ");
					}
				}
				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			}
			Log.d("COUNTER VALUE", "COUNTER " + counter);

			return inFromLeft;
		}*/

		private Animation outToLeftAnimation() {


			Animation outtoLeft = new TranslateAnimation(
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, -1.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f);
			outtoLeft.setDuration(300);
			outtoLeft.setInterpolator(new AccelerateInterpolator());
			/*view.showNext();*/

			return outtoLeft;
		}

		/*private Animation inFromRightAnimation() {

			Animation inFromRight = new TranslateAnimation(
					Animation.RELATIVE_TO_PARENT, +1.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f,
					Animation.RELATIVE_TO_PARENT, 0.0f);
			inFromRight.setDuration(300);
			inFromRight.setInterpolator(new AccelerateInterpolator());


			getupdate(counter);
			update(counter);
			if(!(car_details.isEmpty())){
				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			}else{
				for(int position = 0; position < values.size(); position++){
					if(position < 1){
						car_details.add(">");
					}else{
						car_details.add(" ");
					}
				}
				mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			}
			return inFromRight;
		}*/

	}// End of inner class

	public Animation inFromRightAnimation() {

		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(300);
		inFromRight.setInterpolator(new AccelerateInterpolator());


		getupdate(counter);
		update(counter);

		if(!(car_details.isEmpty())){
			mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			/*tempCar = car_details.get(4);
			spinnerModel.setSelection(getCarPosition(tempCar));*/
		}else{
			for(int position = 0; position < values.size(); position++){
				if(position < 1){
					car_details.add(">");
				}else{
					car_details.add(" ");
				}
			}
			mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
		}
		showImage(path);
		return inFromRight;
	}

	public Animation inFromLeftAnimation() {


		Animation inFromLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromLeft.setDuration(300);
		inFromLeft.setInterpolator(new AccelerateInterpolator());

		getupdate(counter);
		update(counter);

		if(!(car_details.isEmpty())){
			mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
			/*tempCar = car_details.get(4);
			spinnerModel.setSelection(getCarPosition(tempCar));*/
		}else{
			for(int position = 0; position < values.size(); position++){
				if(position < 1){
					car_details.add(">");
				}else{
					car_details.add(" ");
				}
			}
			mycarList.setAdapter(new ArrayAdapterList(context,values,car_details));
		}
		showImage(path);
		Log.d("COUNTER VALUE", "COUNTER " + counter);

		return inFromLeft;
	}


	@Override
	public void onClick(View v) {/*
		// TODO Auto-generated method stub
		if(v.getId()==imgNissanTab.getId())
		{
			//Intent intent=new Intent(getApplicationContext(),NissanTabActivity.class);
			Intent intent = new Intent(context,  Nissan_main_tab.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();
		}
		if(v.getId()==imgTipsTab.getId())
		{
			FlurryAgent.logEvent("Tips_Tab_Event",articleParams);
			Intent intent=new Intent(getApplicationContext(),Tips.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();
		}
		if(v.getId()==imgToolsTab.getId())
		{
			FlurryAgent.logEvent("Tools_Tab_Event",articleParams);
			Intent intent=new Intent(getApplicationContext(),ToolsTab.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();
		}
		if(v.getId()==imgContactTab.getId())
		{
			Intent intent=new Intent(getApplicationContext(),ContactUs.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();
		}
	 */}

	private void applyRotation(float start, float end) {
		// Find the center of image
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		final float centerX = metrics.widthPixels / 2.0f;
		final float centerY = metrics.heightPixels / 2.0f;

		// Create a new 3D rotation with the supplied parameter
		// The animation listener is used to trigger the next animation
		final Flip3dAnimation rotation = new Flip3dAnimation(start, end, centerX, centerY);
		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		rotation.setAnimationListener(new DisplayNextView(layout.isShown(), view,context,centerX,centerY));

		if (layout.isShown()){
			/*view.showNext();*/
			view.startAnimation(rotation);
		} /*else {
			image2.startAnimation(rotation);
		}*/

	}

	public  int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =2;

		if (height > reqHeight || width > reqWidth) {
			/*if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}*/



			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;




		}

		/* if (height > reqHeight || width > reqWidth) {
			 inSampleSize = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
               (double) Math.max(height, width)) / Math.log(0.5)));
        }*/

		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}
	public  Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		/*// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inInputShareable=true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;

		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = true;
		return BitmapFactory.decodeResource(res, resId, options);	*/

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}





}
