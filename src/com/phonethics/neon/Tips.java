package com.phonethics.neon;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.ArrayAdapterList;
import com.phonethics.adapters.TipsAdapter;
import com.phonethics.customclass.TipsGalleryData;
import com.phonethics.neon.MycarActivity.FileFromURL;
import com.phonethics.network.FileDownloader;
import com.phonethics.network.NetworkCheck;

import com.phonethics.parser.TipsSAXXMLParser;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Tips extends SherlockActivity implements OnClickListener {

	//	public static Tips objTips;
	//
	//	int position = 0;
	//	File file;
	//	ListView listview;
	//	TextView textview;
	//	ImageView imageview;
	//
	//
	//	private ArrayList<String> tipsName		= new ArrayList<String>();
	//	private ArrayList<String> tipsThumbs		= new ArrayList<String>();
	//	private ArrayList<String> mVlink		= new ArrayList<String>();
	//	private ArrayList<String> sVlink		= new ArrayList<String>();
	//	private ArrayList<String> dVlink		= new ArrayList<String>();
	//	private ArrayList<String> locVlink		= new ArrayList<String>();
	//	private ArrayList<String> season_Vlink		= new ArrayList<String>();
	//	private ArrayList<String> howto_Vlink		= new ArrayList<String>();
	//	private ArrayList<String> allVLinks		= new ArrayList<String>();
	//	ArrayList<String> maintaince_Link,safty_Link,driving_link,location_link,season_link,howto_link,allLinks;
	//	ArrayList<String> maintaince_Id,safty_Id,driving_Id,location_Id,season_Id,howto_Id,allID;
	//	ImageView imgNissanTab;
	//	ImageView imgToolsTab;
	//	ImageView imgTipsTab;
	//	ImageView imgMycarTab;
	//	ImageView imgContactTab;
	//	Activity context;
	//	boolean selfTabVisible = true;
	//
	//
	//	String xmlurl			= "http://stage.phonethics.in/proj/neon/neon_tips.php";
	//
	//	ArrayList<TipsGalleryData> tipsGallery;
	//
	//	DefaultHttpClient 	httpClient;
	//	HttpPost 			httpPost;
	//	HttpResponse		httpRes;
	//	HttpEntity			httpEnt;
	//	String 				xml;
	//	static String TIPSXML="/sdcard/neoncache/tips.xml";
	//	static String NEONXML="/sdcard/neoncache/neongallery.xml";
	//	FileInputStream		filename;
	//
	//	NetworkCheck		netAvailable;
	//	FileFromURL 		downloadFile;
	//
	//	int										CACHE_VALIDITY;
	//
	//	// Progress Dialog
	//	private ProgressDialog pDialog;
	//
	//	public static boolean specialCase = false;
	//
	//	static TextView badge_tips_text;
	//	static TextView badge_gel_text;
	//
	//	static String type;
	//	static String badges="";
	//
	//	public static boolean call = false, clarNotify = false;
	//	public static boolean clarNotifyElse=false;
	//
	//
	//	public static final int progress_bar_type = 0; 
	//
	//	RelativeLayout parent;
	//
	//	String SHARED_PREFERENCES_MAP="FIRSTCUT";
	//	String SHARED_PREFERENCES_NAME_EXPENSES="SECONDCUT";
	//	String SHARED_PREFERENCES_NAME_MILEAGE="THRIRDCUT";
	//	String SHARED_PREFERENCES_NAME_REMINDERS="FOURTHCUT";
	//	String SHARED_PREFERENCES_NAME_TRAFFIC="FIFTHCUT";
	//
	//	Map<String, String> articleParams = new HashMap<String, String>();
	//
	//	private String gallery_type;
	//
	//	private String gallery_badges;
	//
	//	ArrayList<Integer> imagesArr;
	//	ActionBar ac;
	//	
	//	public static boolean isInOther = false;

	ActionBar ac;

	ListView listview;

	ProgressBar tipsProgress;

	Context	context;

	Activity acontext;

	FileDownloader downloader;

	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;

	FileOutputStream output;
	NetworkCheck netAvailable;

	String url = "http://stage.phonethics.in/proj/neon/neon_tips.php";
	String filePath = "/sdcard/Neon/tips.txt";

	String recievedPath, xml;

	ArrayList<String> thumbnail, section;

	TipsAdapter aryList;

	List<SectionClass> allWays = new ArrayList<SectionClass>();

	SectionClass sectionObj = null;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tips);


		context			= this;
		//		context			= this;
		acontext 		= this;

		ac = getSupportActionBar();
		ac.setTitle("Tips");

		ac.show();



		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);


		//		context=this;
		//		objTips=this;

		//Flurry Event Log.
		//articleParams.put("App", "Neon"); // Capture author info

		//		tipsGallery=new ArrayList<TipsGalleryData>();
		//
		//		CACHE_VALIDITY 		= Integer.parseInt(getResources().getString(R.string.CacheFileValidity));
		//
		//		maintaince_Link=new ArrayList<String>();
		//		safty_Link=new ArrayList<String>();
		//		driving_link=new ArrayList<String>();
		//		location_link=new ArrayList<String>();
		//		season_link=new ArrayList<String>();
		//		howto_link=new ArrayList<String>();
		//
		//
		//		badge_tips_text = (TextView) findViewById(R.id.badge_tips);
		//		badge_gel_text = (TextView) findViewById(R.id.badge_gal);
		//
		//		parent = (RelativeLayout) findViewById(R.id.RootView);
		//
		//		maintaince_Id=new ArrayList<String>();
		//		safty_Id=new ArrayList<String>();
		//		driving_Id=new ArrayList<String>();
		//		location_Id=new ArrayList<String>();
		//		season_Id=new ArrayList<String>();
		//		howto_Id=new ArrayList<String>();
		//
		//		allID=new ArrayList<String>();
		//		allLinks=new ArrayList<String>();
		//		
		/*imagesArr = new ArrayList<Integer>();

		imagesArr.add(R.drawable.maintainance);
		imagesArr.add(R.drawable.safety);
		imagesArr.add(R.drawable.drivingtips);
		imagesArr.add(R.drawable.locationbasedtips);
		imagesArr.add(R.drawable.season);
		imagesArr.add(R.drawable.howtovideos);


		 */
		listview = (ListView) findViewById(R.id.listView_tips);
		tipsProgress = (ProgressBar) findViewById(R.id.tipsProgress);

		netAvailable = new NetworkCheck(context);

		thumbnail = new ArrayList<String>();
		section = new ArrayList<String>();

		// Downloading Code ............

		//downloader = new FileDownloader(context);



		//downloader.setDirectoryName("Neon");
		//downloader.setFileName("tips.txt");



		try {

			//recievedPath = downloader.getFilePath();

			/*if(netAvailable.isNetworkAvailable()){

				File file = new File(filePath);

				if(file.exists()){

					parseXML();
				}
				else{

					MyAsyncTask download = new MyAsyncTask();
					download.execute(url);
					
				}
				
				MyAsyncTask download = new MyAsyncTask();
				download.execute(url);

			}else{

				File myDirectory = new File(filePath);

				if(myDirectory.exists()){

					parseXML();
				}
				else{

					Toast.makeText(context, "No internet connection", 0).show();
				}
			}*/
			
			File file = new File(filePath);
			if(file.exists()){
				Date lastModDate = new Date(file.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);
				if(diffDays>3){
					MyAsyncTask download = new MyAsyncTask();
					download.execute(url);
				}else{
					parseXML();		
				}
			
			}else if(netAvailable.isNetworkAvailable()){
				MyAsyncTask download = new MyAsyncTask();
				download.execute(url);
			}else{
				Toast.makeText(context, "No internet connection", 0).show();
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		} 

		//Log.d("RETRIEVED PATH","RETRIEVED PATH" +  recievedPath);
		//
		//		if(downloader.isNetworkAvailable()){
		//			if(!downloader.isFileExists("test.txt")){

		//		downloader.downloadFrom(url);
		//		downloader.startDownload();


		//		try {
		//			try {

		//recievedPath = downloader.getFilePath();
		//parseXML();

//		for(int i=0;i<allWays.size();i++){
//
//			int allWaysSize = allWays.get(0).getIdilink().size();
//
//			for(int k=0;k<allWaysSize;k++){
//
//				Log.d("FRSTLINKS","FRSTLINKS" +   allWays.get(0).getIdilink().get(k));
//			}
//
//			if(i==0){
//				break;
//			}
//		}

//		aryList = new TipsAdapter(acontext, section, thumbnail);
//		listview.setAdapter(aryList); 

		listview.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub


				int allWaysSize = allWays.get(pos).getIdilink().size();

				ArrayList<String>  imagesToPass = new ArrayList<String>();
				ArrayList<String>  vlinkToPass = new ArrayList<String>();
				ArrayList<String>  allID = new ArrayList<String>();

				ArrayList<String> tempLink=new ArrayList<String>();
				ArrayList<String> tempVLink=new ArrayList<String>();



				for(int k=0;k<allWaysSize;k++){

					Log.d("FRSTLINKS","FRSTLINKS" +   allWays.get(pos).getIdilink().get(k));
					imagesToPass.add(allWays.get(pos).getIdilink().get(k));
					vlinkToPass.add(allWays.get(pos).getIdvlink().get(k));
					allID.add(allWays.get(pos).getIdPhoto().get(k));

				}


				/*
				 *	Sorting photo Links 
				 */
				tempLink=sortLinks(allID, imagesToPass);
				imagesToPass.clear();
				for(int i=0;i<tempLink.size();i++)
				{
					imagesToPass.add(tempLink.get(i));

				}

				/*
				 *	Sorting video Links 
				 */
				tempVLink=sortLinks(allID, vlinkToPass);
				vlinkToPass.clear();
				for(int i=0;i<tempVLink.size();i++)
				{
					vlinkToPass.add(tempVLink.get(i));

				}

				Intent intent = new Intent(context, NissanGallery.class);
				intent.putStringArrayListExtra("imagesLink", imagesToPass);
				intent.putStringArrayListExtra("videoLinks", vlinkToPass);
				intent.putExtra("section", section.get(pos));
				startActivity(intent);
				finish();

			}
		});


		//			} catch (IOException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//		} catch (XmlPullParserException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}



		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		listview.setLayoutAnimation(controller);

		//textview = (TextView) findViewById(R.id.Textview_tips);
		//ArrayList<String>values = new ArrayList<String>();

		//		netAvailable = new NetworkCheck(context);
		//		try
		//		{
		//			if(netAvailable.isNetworkAvailable())
		//			{
		//				/*
		//				 * creating directory to store xml 
		//				 */
		//				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
		//
		//				if(!myDirectory.exists()) {                                 
		//					myDirectory.mkdirs();
		//				}
		//
		//				File xmlFile=new File(TIPSXML);
		//
		//				if(xmlFile.exists() && xmlFile.length()!=0)
		//				{
		//
		//
		//					Date lastModDate = new Date(xmlFile.lastModified());
		//					Date todayDate = new Date();
		//					long fileDate = lastModDate.getTime();
		//					long currentDate = todayDate.getTime();
		//					long dateDiff = currentDate - fileDate;
		//					long diffDays = dateDiff / (24 * 60 * 60 * 1000);
		//					if(diffDays > CACHE_VALIDITY){
		//						downloadFile=new FileFromURL();
		//						downloadFile.execute(xmlurl);	
		//					}
		//					else
		//					{
		//						try
		//						{
		//							filename=new FileInputStream(TIPSXML);
		//							parseDoc(filename);
		//						}
		//						catch(NullPointerException npx)
		//						{
		//							npx.printStackTrace();
		//						}
		//						catch(Exception ex)
		//						{
		//							ex.printStackTrace();
		//						}
		//
		//					}
		//
		//					/*if(xmlFile.exists() && xmlFile.length() != 0){
		//
		//						Date lastModDate = new Date(xmlFile.lastModified());
		//						Date todayDate = new Date();
		//						long fileDate = lastModDate.getTime();
		//						long currentDate = todayDate.getTime();
		//						long dateDiff = currentDate - fileDate;
		//						long diffDays = dateDiff / (24 * 60 * 60 * 1000);
		//						if(diffDays > CACHE_VALIDITY){
		//							downloadFile=new FileFromURL();
		//							downloadFile.execute(MYCAR_URL);	
		//						}else{
		//							try{
		//								filename = new FileInputStream(MYCAR_XML);
		//								parseDoc(filename);
		//							}catch(Exception ex){
		//								ex.printStackTrace();
		//							}
		//						}
		//
		//					}else{
		//						downloadFile=new FileFromURL();
		//						downloadFile.execute(MYCAR_URL);
		//					}*/
		//
		//
		//
		//
		//				}else
		//				{
		//					try
		//					{
		//						downloadFile=new FileFromURL();
		//						downloadFile.execute(xmlurl);
		//					}catch(Exception ex)
		//					{
		//						ex.printStackTrace();
		//					}
		//				}
		//			}else
		//			{
		//				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
		//
		//				if(!myDirectory.exists()) {                                 
		//					myDirectory.mkdirs();
		//				}
		//
		//				File xmlFile=new File(TIPSXML);
		//				try
		//				{
		//					if(xmlFile.exists() && xmlFile.length()!=0)
		//					{
		//						filename=new FileInputStream(TIPSXML);
		//						parseDoc(filename);
		//					}
		//					else
		//					{
		//						Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
		//					}
		//				}catch(Exception ex)
		//				{
		//					ex.printStackTrace();
		//				}
		//			}
		//
		//
		//		}catch(Exception ex)
		//		{
		//			ex.printStackTrace();
		//		}

		/*
		 * Tab bar 
		 */
		//		imgNissanTab=(ImageView)findViewById(R.id.tabNissan);
		//
		//
		//
		//		imgTipsTab=(ImageView)findViewById(R.id.tabTips);
		//		imgTipsTab.setImageResource(R.drawable.tips);
		//
		//		imgToolsTab=(ImageView)findViewById(R.id.tabTools);
		//
		//
		//		imgMycarTab=(ImageView)findViewById(R.id.tabMyCar);
		//
		//
		//		imgContactTab=(ImageView)findViewById(R.id.tabContact);
		//
		//		/*
		//		 * Regisering click listeners
		//		 */
		//		imgNissanTab.setOnClickListener(this);
		//		imgTipsTab.setOnClickListener(this);
		//		imgToolsTab.setOnClickListener(this);
		//		imgMycarTab.setOnClickListener(this);
		//		//imgContactTab.setOnClickListener(this);







		/*	gallery = (Gallery) findViewById(R.id.gallery);*/

		//		values.add("Maintenance"); values.add("Safety"); values.add("Driving Tips");
		//		values.add("Location Based Tips");values.add("Season Based Tips");values.add("How to Videos");


		// to set the size and padding of every text
		/*		imageview = (ImageView) findViewById(R.id.imageview_tips);*/



		/*
		 *  DOM Parsing
		 */

		/*try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(getAssets().open("tips.xml"));
			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("section");
			NodeList nPhoto=doc.getElementsByTagName("photo");
			System.out.println("-----------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				Node nLinkNode=nPhoto.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					//Adding Names of Tips
					tipsName.add(eElement.getAttribute("name"));
					int n = eElement.getElementsByTagName("photo").getLength();


					Log.i(" Size is ", " "+n);

				}//if ends here

			}//for loop ends here

			for(int i=0;i<nPhoto.getLength();i++)
			{

				Node nLinkNode=nPhoto.item(i);
				if (nLinkNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eLink=(Element)nLinkNode;
				//	Log.i("Links : "," "+eLink.getAttribute("Link"));
					allLinks.add(eLink.getAttribute("ilink"));
					allVLinks.add(eLink.getAttribute("vlink"));
					Log.i("Attribute: ", " "+getNodeAttr("Link",nLinkNode));
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		for(int i=0;i<allLinks.size();i++)
		{
			Log.i("-----------"+i,"----------------");
			Log.i("  Links ", " "+allLinks.get(i));
			if(allLinks.get(i).contains("maintenance"))
			{
				maintaince_Link.add(allLinks.get(i));
				mVlink.add(allVLinks.get(i));
			}
			if(allLinks.get(i).contains("saftey"))
			{
				safty_Link.add(allLinks.get(i));
				sVlink.add(allVLinks.get(i));
			}
			if(allLinks.get(i).contains("driving"))
			{
				driving_link.add(allLinks.get(i));
				dVlink.add(allVLinks.get(i));
			}
			if(allLinks.get(i).contains("location"))
			{
				location_link.add(allLinks.get(i));	
				locVlink.add(allVLinks.get(i));
			}
			if(allLinks.get(i).contains("season"))
			{
				season_link.add(allLinks.get(i));	
				season_Vlink.add(allVLinks.get(i));
			}
			if(allLinks.get(i).contains("videos"))
			{
				howto_link.add(allLinks.get(i));	
				howto_Vlink.add(allVLinks.get(i));
			}

		}
		 */		
		/*		for(int i=0;i<maintaince_Link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" Maintainance Links ", " "+maintaince_Link.get(i)+" VLinks "+mVlink.get(i));

		}
		for(int i=0;i<safty_Link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" safty_Link Links ", " "+safty_Link.get(i)+" VLinks "+sVlink.get(i));
		}
		for(int i=0;i<driving_link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" driving_link Links ", " "+driving_link.get(i)+" VLinks "+dVlink.get(i));
		}
		for(int i=0;i<location_link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" location_link Links ", " "+location_link.get(i)+" VLinks "+locVlink.get(i));
		}
		for(int i=0;i<season_link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" season_link Links ", " "+season_link.get(i)+" VLinks "+season_Vlink.get(i));
		}
		for(int i=0;i<howto_link.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i(" howto_link Links ", " "+howto_link.get(i)+" VLinks "+howto_Vlink.get(i));
		}
		 */



		/*TipsAdapter tpAdapter=new TipsAdapter(context, tipsName);
		listview.setAdapter(tpAdapter);*/


		//		listview.setOnItemClickListener(new OnItemClickListener() {
		//
		//			@Override
		//			public void onItemClick(AdapterView<?> arg0, View view, int position,long arg3) {
		//				// TODO Auto-generated method stub
		//				//textview = (TextView) findViewById(R.id.Textview_tips);
		//				/*	GalleryScrollView gallery = new GalleryScrollView();*/
		//				
		//			
		//				
		//				Intent intent = new Intent(getApplicationContext(), NissanGallery.class);
		//				intent.putExtra("positionvalue", position);
		//
		//
		//				Object o=listview.getItemAtPosition(position);
		//				String item=(String)o.toString();
		//
		//				//FlurryAgent.logEvent("Section: "+item,articleParams);
		//
		//				Map<String, String> articleParams = new HashMap<String, String>();
		//		        articleParams.put("Section", item); // Capture author info
		//		     
		//		        FlurryAgent.logEvent("Tips_Section_Event", articleParams);
		//
		//
		//				for(int i=0;i<tipsGallery.size();i++)
		//				{
		//
		//					for(int j=0;j<tipsGallery.get(i).getArr_Id().size();j++)
		//					{
		//						/*allLinks.add(tipsGallery.get(position));*/
		//
		//						if(item.equalsIgnoreCase(tipsGallery.get(i).getSectionName()))
		//						{
		//							allLinks.add(tipsGallery.get(i).getArr_Ilink().get(j));
		//							allID.add(tipsGallery.get(i).getArr_Id().get(j));
		//							allVLinks.add(tipsGallery.get(i).getArr_Vlink().get(j));
		//						}
		//
		//					}
		//				}
		//				ArrayList<String> tempLink=new ArrayList<String>();
		//				ArrayList<String> tempVLink=new ArrayList<String>();
		//
		//				/*
		//				 *	Sorting photo Links 
		//				 */
		//				tempLink=sortLinks(allID, allLinks);
		//				allLinks.clear();
		//				for(int i=0;i<tempLink.size();i++)
		//				{
		//					allLinks.add(tempLink.get(i));
		//
		//				}
		//
		//				/*
		//				 *	Sorting video Links 
		//				 */
		//				tempVLink=sortLinks(allID, allVLinks);
		//				allVLinks.clear();
		//				for(int i=0;i<tempVLink.size();i++)
		//				{
		//					allVLinks.add(tempVLink.get(i));
		//
		//				}
		//				intent.putStringArrayListExtra("links", allLinks);
		//				intent.putStringArrayListExtra("vlink", allVLinks);
		//				intent.putExtra("section", item);
		//				startActivity(intent);
		//				finish();
		//
		//
		//				/*for(int i=0;i<tipsName.size();i++)
		//				{
		//
		//				}*/
		//
		//
		//
		//				/*switch(position){
		//				case 0:
		//
		//					intent.putStringArrayListExtra("maintainance", maintaince_Link);
		//					intent.putStringArrayListExtra("vlink", mVlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//				case 1:
		//					intent.putStringArrayListExtra("safety", safty_Link);
		//					intent.putStringArrayListExtra("vlink", sVlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//				case 2:
		//					for(int i=0;i<driving_link.size();i++)
		//					{
		//
		//						Log.i("______________", "");
		//						Log.i("Driving", " Driving "+driving_link.get(i));
		//					}
		//					intent.putStringArrayListExtra("driving", driving_link);
		//					intent.putStringArrayListExtra("vlink", dVlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//				case 3:
		//					intent.putStringArrayListExtra("location", location_link);
		//					intent.putStringArrayListExtra("vlink", locVlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//				case 4:
		//					intent.putStringArrayListExtra("season", season_link);
		//					intent.putStringArrayListExtra("vlink", season_Vlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//				case 5:
		//					intent.putStringArrayListExtra("videos", howto_link);
		//					intent.putStringArrayListExtra("vlink", howto_Vlink);
		//					startActivity(intent);
		//					finish();
		//					break;
		//
		//				}*/
		//			}
		//		});

	}


	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 * Asynchronus download of XML files
	 */


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */


		SubMenu subMenu1 = menu.addSubMenu("Action Item");



		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Find My Car").setIcon(R.drawable.location);
		subMenu1.add("Reminders").setIcon(R.drawable.reminder);
		subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
				//if(selfTabVisible){
					subMenu1.add("Tips").setIcon(R.drawable.tips);
				//}
		subMenu1.add("My Car").setIcon(R.drawable.mycar);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}



		return true;
	}

	private void parseXML() throws XmlPullParserException,IOException{

		//Toast.makeText(context, "INSIDETRY", 0).show();


		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();



		try {
			
		
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			xpp.setInput(new InputStreamReader(fis));

			int eventType = xpp.getEventType();

			while(eventType!=XmlPullParser.END_DOCUMENT){

				String tagName = xpp.getName();

				switch (eventType) {

				case XmlPullParser.START_DOCUMENT:

					break;

				case XmlPullParser.START_TAG:

					//String tagName = xpp.getName();

					//Log.d("TAGS","TAGS" +  tagName);

					if(tagName.equalsIgnoreCase("section")){

						String thumb = xpp.getAttributeValue(null, "ilink");

						String sectionName = xpp.getAttributeValue(null, "name");

						thumbnail.add(thumb);

						section.add(sectionName);

						Log.d("THUMBURLS","THUMBURLS" + sectionName);



					}

					if("photo".equals(xpp.getName())) {

						sectionObj.setIdPhoto(xpp.getAttributeValue(null, "id"));
						sectionObj.setIdilink(xpp.getAttributeValue(null, "ilink"));
						sectionObj.setIdvlink(xpp.getAttributeValue(null, "vlink"));

					}
					else if("section".equals(xpp.getName())) {

						sectionObj = new SectionClass();
					}

					break;


				case XmlPullParser.END_TAG:

					Log.d("ENDTAGS","ENDTAGS" + tagName);

					if("section".equals(xpp.getName())) {

						allWays.add(sectionObj);

					}

					break;

				default:
					break;
				}

				eventType = xpp.next();
			}
			
			aryList = new TipsAdapter(acontext, section, thumbnail);
			listview.setAdapter(aryList); 



		} catch (IOException e) {
			// TODO: handle exception

			e.printStackTrace();

		} catch (XmlPullParserException ex) {
			// TODO: handle exception

			ex.printStackTrace();
		}

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public ArrayList<String> sortLinks( ArrayList<String> id,ArrayList<String> link)
	{
		Map<Integer, String> valuesMap = new HashMap<Integer, String>();
		ArrayList<String> sortedList=new ArrayList<String>();

		for(int i=0;i<id.size();i++)
		{
			valuesMap.put(Integer.parseInt(id.get(i)), link.get(i));
		}

		Map<Integer, String> sortedMap = new TreeMap<Integer, String>(valuesMap);

		for (Iterator it = sortedMap.values().iterator(); it.hasNext();) {
			String value = it.next().toString();
			sortedList.add(value);
		}

		for(int i=0;i<sortedList.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i("Links ", " "+sortedList.get(i));
		}

		return sortedList;

	}
	
	
	private class MyAsyncTask extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(url);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "Neon");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(filePath);


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1)) {


					// publishing the progress....
					// After this onProgressUpdate will be called
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

					// writing data to file
					total += count;
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
//			 mProgressDialog.dismiss();
//			postExecute = true;
			//new Schedule().bttn.performClick();
			
			try {
				parseXML();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
			tipsProgress.setVisibility(View.INVISIBLE);
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			mProgressDialog.show();
//			preExecute = true;
			/*new Schedule().getJsonData();*/
			
			
			
			tipsProgress.setVisibility(View.VISIBLE);

		}
	}
}
