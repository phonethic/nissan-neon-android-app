package com.phonethics.neon;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.NissanListAdapter;
import com.phonethics.database.DataBaseUtil;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;

import android.view.MenuInflater;
import android.view.View;

import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Remeinders extends SherlockActivity {

	public static ImageView 	addreminderImg, pendingImg, completedImg, info, header;
	public static TextView		pending_completText;
	public static ListView		remeindersList;
	DataBaseUtil				db;
	public static boolean		pendingVisible = true;
	public static String		pendingText 	= "Pending Reminders List";	
	public static String		completedText 	= "Completed Reminders List";	
	Intent 						intent;
	int							listPosition;
	LayoutInflater				inflator;
	RelativeLayout				relativeLayout;
	LinearLayout				layout_text_pending,layout_text_completed;
	ToggleButton				toggle;
	CheckBox					chechkBox;
	Context						context;
	boolean						selfTabVisible = true;


	ArrayList<Object> 			tempRemeinderData;
	ArrayList<String> 			finalReminderId;		/// use to store the database id of a reminder
	ArrayList<String> 			finalReminderOnOff;		/// use to set the reminder to be off or on
	ArrayList<String> 			finalReminderMessage;	/// use to store message of reminders
	ArrayList<String> 			finalReminderDate;		/// use to store date of reminders
	ArrayList<String> 			finalReminderTime;		/// use to store Time of Reminders
	ArrayList<String> 			finalReminderPreValue;		/// use to store Time of Reminders
	ArrayList<String> 			finalReminderPreUnit;		/// use to store Time of Reminders
	String 						packageName;
	String 						className;
	String 						baseActivity;
	ActivityManager 			activityManager;

	RelativeLayout relParent;
	String mcarid = "";

	ActionBar ac;
	
	public static boolean isInOther = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remeinders);
		context					= this;

		ac = getSupportActionBar();
		ac.setTitle("Reminders");
		//ac.setDisplayHomeAsUpEnabled(true);
		ac.show();


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);


		relParent				=(RelativeLayout)findViewById(R.id.relReminders);
		layout_text_pending		= (LinearLayout) findViewById(R.id.linear_text_1);
		layout_text_completed	= (LinearLayout) findViewById(R.id.linear_text_2);
		intent					= new Intent(getApplicationContext(), Add_remeinders.class);
		db						= new DataBaseUtil(getApplicationContext());

		addreminderImg			= (ImageView) findViewById(R.id.image_addbutton_remeinders);
		pendingImg				= (ImageView) findViewById(R.id.image_pendingbutton);
		completedImg			= (ImageView) findViewById(R.id.image_completedbutton);
		info					= (ImageView) findViewById(R.id.image_info_reminder);
		header					= (ImageView) findViewById(R.id.header);

		pending_completText		= (TextView) findViewById(R.id.textview_pending_completed);

		remeindersList			= (ListView) findViewById(R.id.lisview_remeinerds);

		relativeLayout			= (RelativeLayout) findViewById(R.id.layout_row);


		tempRemeinderData 		= new ArrayList<Object>();
		finalReminderId	 		= new ArrayList<String>();		/// use to store the database id of a reminder
		finalReminderOnOff		= new ArrayList<String>();		/// use to Set the reminder to be on or off
		finalReminderMessage	= new ArrayList<String>();		/// use to store message of reminders
		finalReminderDate 		= new ArrayList<String>();		/// use to store date of reminders
		finalReminderTime 		= new ArrayList<String>();		/// use to store Time of Reminders
		finalReminderPreValue 	= new ArrayList<String>();		/// use to store Time of Reminders
		finalReminderPreUnit 	= new ArrayList<String>();		/// use to store Time of Reminders


		addreminderImg.setVisibility(View.GONE);
		pendingImg.setVisibility(View.GONE);
		completedImg.setVisibility(View.GONE);
		header.setVisibility(View.GONE);


		activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		baseActivity = activityManager.getRunningTasks(1).get(0).baseActivity.getClassName();
		Log.d("--------------", baseActivity);
		onClicks();
		getAllReminders();

	}

	public void onClicks(){
		addreminderImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pendingVisible = true;
				intent.putExtra("IntentCall", "New_Reminder");

				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();
			}
		});

		pendingImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pendingVisible = true;
				/*relativeLayout.setBackgroundResource(R.drawable.completed_reminder_bg);*/
				getAllReminders();
			}
		});
		completedImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pendingVisible = false;
				/*relativeLayout.setBackgroundResource(R.drawable.reminder_bg1);*/
				getAllReminders();
			}
		});

		layout_text_pending.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_text_pending.setBackgroundColor(Color.parseColor("#c71444"));
				layout_text_completed.setBackgroundColor(Color.BLACK);
				pendingImg.performClick();
			}
		});
		layout_text_completed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_text_completed.setBackgroundColor(Color.parseColor("#c71444"));
				layout_text_pending.setBackgroundColor(Color.BLACK);
				completedImg.performClick();
			}
		});

	}

	public void getAllReminders(){
		ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();
		try{
			db.open();
			if(pendingVisible == true){
				reminder_details = db.getAllPendingRemeinders();
			}else if(pendingVisible == false){
				reminder_details = db.getAllCompletedRemeinders();
			}
			db.close();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			db.close();
		}

		if(reminder_details.isEmpty()){
			if(pendingVisible == true){
				/*Toast.makeText(context, "There Are No Pending Reminders", Toast.LENGTH_SHORT).show();*/
				pending_completText.setVisibility(View.GONE);
				remeindersList.setVisibility(View.GONE);
				info.setVisibility(View.VISIBLE);
			}else{
				/*Toast.makeText(context, "There Are No Completed Reminders", Toast.LENGTH_SHORT).show();*/
				pending_completText.setVisibility(View.GONE);
				remeindersList.setVisibility(View.GONE);
				info.setVisibility(View.VISIBLE);

			}
		}else{
			remeindersList.setVisibility(View.VISIBLE);
			if (pendingVisible == true) {
				pending_completText.setText(pendingText);
				pending_completText.setVisibility(View.GONE);
				info.setVisibility(View.GONE);
			}else{
				pending_completText.setText(completedText);
				pending_completText.setVisibility(View.GONE);
				info.setVisibility(View.GONE);
			}

			tempRemeinderData.clear();
			finalReminderId.clear();
			finalReminderOnOff.clear();
			finalReminderMessage.clear();
			finalReminderDate.clear();
			finalReminderTime.clear();
			finalReminderPreValue.clear();
			finalReminderPreUnit.clear();

			for(int position =0; position < reminder_details.size(); position++){		//// Will pick out the message, date and time of each reminder
				tempRemeinderData = reminder_details.get(position);		
				finalReminderId.add(tempRemeinderData.get(0).toString());
				finalReminderOnOff.add(tempRemeinderData.get(1).toString());
				finalReminderMessage.add(tempRemeinderData.get(2).toString());
				finalReminderDate.add(tempRemeinderData.get(3).toString());
				finalReminderTime.add(tempRemeinderData.get(4).toString());
				finalReminderPreValue.add(tempRemeinderData.get(5).toString());
				finalReminderPreUnit.add(tempRemeinderData.get(6).toString());
			}
			NissanListAdapter adapter = new NissanListAdapter(
					this, R.layout.listviewformat, 
					finalReminderMessage, 
					finalReminderDate, 
					finalReminderTime, 
					finalReminderId, 
					finalReminderOnOff,
					finalReminderPreValue,
					finalReminderPreUnit);
			remeindersList.setAdapter(adapter);
			/*remeindersList.setOnItemLongClickListener(new CommonListClickListener());*/
			registerForContextMenu(remeindersList);
			/*remeindersList.setOnItemClickListener(new ListClickListener());*/
		}
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*super.onBackPressed();*/
		//		if(baseActivity.equals("com.phonethics.neon.ReminderAlaramRecivedActivity")){
		//			Intent intent = new Intent(context, Remeinders.class);
		//			startActivity(intent);
		//			this.finish();
		//			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		//
		//		}
		//		else{
		//			startActivity(new Intent(context, Actionbar.class));
		//			this.finish();
		//			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		//		}


		super.onBackPressed();

		this.finish();

		/*super.onBackPressed();*/
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_reminder_setting, menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_reminder_setting, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch(item.getItemId()){
		case R.id.reminderss_settings :
			startActivity(new Intent(context, ReminderSettingActivity.class));
			return true;
			default:
				return super.onOptionsItemSelected(item);
		}*/


	/*
	 * Context Menu for Edit and Delete
	 * 
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("Options");
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.alarmcontextmenu, menu);
	}

	public boolean onContextItemSelected(android.view.MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

		switch(item.getItemId()) {
		case R.id.editMenu:
			pendingVisible = true;
			Intent intent = new Intent(getApplicationContext(), Add_remeinders.class);
			intent.putExtra("IntentCall", "Update_Reminder");
			intent.putExtra("Message", finalReminderMessage.get(info.position).toString());
			intent.putExtra("Date", finalReminderDate.get(info.position).toString());
			intent.putExtra("Time", finalReminderTime.get(info.position).toString());
			intent.putExtra("Value", finalReminderPreValue.get(info.position).toString());
			intent.putExtra("Unit", finalReminderPreUnit.get(info.position).toString());

			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();

			return true;
		case R.id.deleteMenu:
			try
			{

				String positionText = finalReminderId.get(info.position);
				try{

					db.open();
					db.deleteReminder(positionText);
					db.close();

				}catch(Exception ex){
					Log.d("DELETING REMINDER ERROR", ex.toString());
				}
				getAllReminders();

				/*if(!dataList.isEmpty()){
					dbUtil.open();
					dbUtil.deleteMileageById(Integer.parseInt(id));
					dataList.clear();
					dataList=dbUtil.getMileageDetails();
					milList.setAdapter(new MilageMeterAdapter(context, dataList));
				}
				else
				{
					//Toast.makeText(context, "No Entries", Toast.LENGTH_SHORT).show();
				}
				 */
			}catch(SQLException sqex)
			{
				sqex.printStackTrace();
			}
			finally
			{
				//dbUtil.close();
			}
			//	Toast.makeText(context, "Clicked Delete"+info.position+"ID "+id, Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onContextItemSelected(item);

		}


	}




	public class CommonListClickListener implements OnItemLongClickListener{

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				final int position, long arg3) {
			// TODO Auto-generated method stub
			listPosition = position;
			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setTitle("Neon");
			alertDialog.setMessage("SELECT");
			alertDialog.setCancelable(true);
			alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					String positionText = finalReminderId.get(listPosition);
					try{

						db.open();
						db.deleteReminder(positionText);
						db.close();

					}catch(Exception ex){
						Log.d("DELETING REMINDER ERROR", ex.toString());
					}
					getAllReminders();

				}
			});
			alertDialog.setNegativeButton("Update", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					pendingVisible = true;
					Intent intent = new Intent(getApplicationContext(), Add_remeinders.class);
					intent.putExtra("IntentCall", "Update_Reminder");
					intent.putExtra("Message", finalReminderMessage.get(position).toString());
					intent.putExtra("Date", finalReminderDate.get(position).toString());
					intent.putExtra("Time", finalReminderTime.get(position).toString());
					startActivity(intent);
					finish();
				}		
			});
			AlertDialog alert = alertDialog.create();
			alert.show();

			//Toast.makeText(getApplicationContext(), "DATABASE ID " + finalReminderId.get(position) + "List Position "+ position, Toast.LENGTH_SHORT).show();

			return false;
		}

	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		MenuItem menu_save = menu.add("Add");
		menu_save.setIcon(R.drawable.add_button);
		menu_save.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		SubMenu subMenu1 = menu.addSubMenu("Action Item");
		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Find My Car").setIcon(R.drawable.location);
		if (selfTabVisible) {
			subMenu1.add("Reminders").setIcon(R.drawable.reminder);	
		}

		subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
		subMenu1.add("Tips").setIcon(R.drawable.tips);
		subMenu1.add("My Car").setIcon(R.drawable.mycar);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);



		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);




		return true;
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){
			
			FlurryAgent.logEvent("Tips_Tab_Event");
			
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			onBackPressed();
		}*/
		if(item.getTitle().toString().equalsIgnoreCase("Add")){

			addreminderImg.performClick();
		}

		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try
		{
			unbindDrawables(relParent);
			System.gc();
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	private void unbindDrawables(View view) {
		// TODO Auto-generated method stub

		Log.d("=============++++", "CALLED_HOME");
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof RelativeLayout) {
			for (int i = 0; i < ((RelativeLayout) view).getChildCount(); i++) {
				unbindDrawables(((RelativeLayout) view).getChildAt(i));
			}
			((RelativeLayout) view).removeAllViews();
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");

		if(isInOther){

			isInOther = false;
			finish();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);

	}


	/*public class ListClickListener  implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			finalReminderMessage.get(position);
			finalReminderDate.get(position);
			finalReminderTime.get(position);
			pendingVisible = true;
			Intent intent = new Intent(getApplicationContext(), Add_remeinders.class);
			intent.putExtra("IntentCall", "Update_Reminder");
			intent.putExtra("Message", finalReminderMessage.get(position).toString());
			intent.putExtra("Date", finalReminderDate.get(position).toString());
			intent.putExtra("Time", finalReminderTime.get(position).toString());
			startActivity(intent);
			finish();
		}

	}*/


}
