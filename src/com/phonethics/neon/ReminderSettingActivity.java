package com.phonethics.neon;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.wheel.ArrayWheelAdapter;
import com.phonethics.wheel.NumericWheelAdapter;
import com.phonethics.wheel.OnWheelScrollListener;
import com.phonethics.wheel.WheelView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ReminderSettingActivity extends SherlockActivity {

	private static final String KEY_VALUE = "com.kdion.tutorial.odometer.OdometerValue";

	/*private OdometerReminder 		mOdometer1, mOdometer2;
	private TextView 				edittext,spinner_text;
	private Spinner					spin;
	private String[] 				items;
	Context 						context;
	ImageView						saveImage;

	InputMethodManager 				imm ;

	private int 					mOdometerValue;
	DataBaseUtil			db;*/

	WheelView						numberWheel,unitWheel;
	TextView						textView;
	private String[] 				unitItems;
	NumericWheelAdapter 			numberAdapter;
	boolean							currentItemZero=true;
	String							unit="";
	String[]						numberData;
	String								value="";
	ImageView						imgSave,header;
	Context 						context;
	ActionBar 						ac;
	/*OnWheelScrollListener 			unitScrollListner;*/


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reminder_setting);

		/*context 			= this;
		items				= new String[]{"No Pre Notify","Minutes","Hours","Days","Weeks"};
		mOdometer1 			= (OdometerReminder) findViewById(R.id.odometer_reminder_tens);

		spinner_text		= (TextView) findViewById(R.id.spinner_text);
		edittext 			= (TextView) findViewById(R.id.edit_reminder_setting);

		saveImage			= (ImageView) findViewById(R.id.image_savebutton_remeinders);

		db					=  new DataBaseUtil(context);

		spin				= (Spinner) findViewById(R.id.spinner);
		imm 				= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		ArrayAdapter aa 	=  new ArrayAdapter(context,android.R.layout.simple_spinner_dropdown_item,items);
		spin.setAdapter(aa);



		mOdometer1.setOnValueChangeListener(new OdometerReminder.OnValueChangeListener()
		{
			@Override
			public void onValueChange(OdometerReminder sender, int newValue)
			{
				updateOdometerValue();
				int mOdometerValue1 = mOdometer1.getValue();
				String text = String.format("%02d", mOdometerValue1);
				edittext.setText(text);
				Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

			}
		});


		if(savedInstanceState != null)
		{
			mOdometerValue = savedInstanceState.getInt(KEY_VALUE);

		}


		spinner_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mOdometer1.setVisibility(View.GONE);
				spin.performClick();

			}
		});


		spin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				mOdometer1.setVisibility(View.GONE);
				mOdometer2.setVisibility(View.GONE);
				int pos = spin.getSelectedItemPosition();
				Toast.makeText(context, "You have selected the book: " + items[pos],Toast.LENGTH_SHORT).show();
				spinner_text.setText(items[pos]);
				spin.setVisibility(View.INVISIBLE);
			}


			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		edittext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mOdometer1.setVisibility(View.VISIBLE);
				mOdometer2.setVisibility(View.VISIBLE);

			}
		});

		saveImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =  new Intent(context, Add_remeinders.class);
				intent.putExtra("numValue", edittext.getText().toString());
				intent.putExtra("numUnits", spinner_text.getText().toString());
				intent.putExtra("IntentCall", "ReminderSetting");
				startActivity(intent);
				finish();
			}
		});





	}



	private void updateOdometerValue()
	{
		mOdometerValue = mOdometer1.getValue();
		String text = String.format("%06d", mOdometerValue);
		edittext.setText(text);
	}
		 */

		

		numberWheel			= (WheelView) findViewById(R.id.numberWhhel);
		unitWheel			= (WheelView) findViewById(R.id.unitWheel);
		imgSave				= (ImageView) findViewById(R.id.image_savebutton_remeinders);
		header				= (ImageView) findViewById(R.id.header_reminder_setting);
		textView			= (TextView) findViewById(R.id.edit_reminder_setting);
		
		header.setVisibility(View.GONE);
		imgSave.setVisibility(View.GONE);
		
		unitItems			= new String[]{"No Pre Notify","Minutes","Hours","Days","Weeks"};
		numberData			= new String[31];
		context 			= this;
		
		ac = getSupportActionBar();
		ac.setTitle("Reminder Settings");
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();
		
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);
		

		textView.setText("No Pre Notify"); 
		
		for(int i=0; i<31;i++){
			if(i==0){
				numberData[i] = "";	
			}else{
				numberData[i] = "" + i;
			}

		}

		OnWheelScrollListener unitScrollListner = new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				if(unitWheel.getCurrentItem()==0){
					numberWheel.setCurrentItem(0,true);
				}
			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				if(unitWheel.getCurrentItem()!=0){
					currentItemZero = false;
				}
				unit 	= unitItems[unitWheel.getCurrentItem()];
				value 	= numberData[numberWheel.getCurrentItem()];
				if(unitWheel.getCurrentItem()==0){
					textView.setText(unit);
					numberWheel.setCurrentItem(0,true);
				}else{
					textView.setText(value+ " " + unit);
				}

			}
		};


		/*numberAdapter = new NumericWheelAdapter(this, 1, 30,"%02d");
		numberAdapter.setItemResource(R.layout.wheel_text_itm);
		numberAdapter.setItemTextResource(R.id.text);
		numberWheel.setViewAdapter(numberAdapter);
		numberWheel.addScrollingListener(unitScrollListner);*/

		ArrayWheelAdapter<String> unitAdapter = new ArrayWheelAdapter<String>(this, unitItems);
		unitAdapter.setItemResource(R.layout.reminder_wheel_text);
		unitAdapter.setItemTextResource(R.id.text1);
		unitWheel.setViewAdapter(unitAdapter);
		unitWheel.addScrollingListener(unitScrollListner);


		ArrayWheelAdapter<String> numberAdapter = new ArrayWheelAdapter<String>(this, numberData);
		numberAdapter.setItemResource(R.layout.reminder_wheel_text);
		numberAdapter.setItemTextResource(R.id.text1);
		numberWheel.setViewAdapter(numberAdapter);
		numberWheel.addScrollingListener(unitScrollListner);



		/*if(!currentItemZero){
			numberAdapter = new NumericWheelAdapter(this, 1, 30,"%02d");
			numberAdapter.setItemResource(R.layout.wheel_text_itm);
			numberAdapter.setItemTextResource(R.id.text);
			numberWheel.setViewAdapter(numberAdapter);
		}else{
			numberAdapter = new NumericWheelAdapter(this);
			numberWheel.setViewAdapter(numberAdapter);
		}
		 */

		imgSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			/*	Intent intent =  new Intent(context, Add_remeinders.class);
				intent.putExtra("numValue", value);
				intent.putExtra("numUnits", unit);
				intent.putExtra("IntentCall", "ReminderSetting");
				startActivity(intent);
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				finish();*/
				
				Intent intent = new Intent();
				intent.putExtra("numValue", value);
				intent.putExtra("numUnits", unit);
				setResult(1, intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				
			}
		});




	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*super.onBackPressed();*/
		value="";
		unit="";
		/*Intent intent =  new Intent(context, Add_remeinders.class);
		intent.putExtra("numValue", value);
		intent.putExtra("numUnits", unit);
		intent.putExtra("IntentCall", "ReminderSetting");
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		finish();*/
		
		
		Intent intent = new Intent();
		intent.putExtra("numValue", value);
		intent.putExtra("numUnits", unit);
		setResult(1, intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		MenuItem menu_save = menu.add("Save");
		menu_save.setIcon(R.drawable.save_button);
		menu_save.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Save")){
			imgSave.performClick();
		}
		
		if(item.getTitle().toString().equalsIgnoreCase("Reminder Settings")){
			onBackPressed();
		}
		return true;
	}
}
