package com.phonethics.neon;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.Formatter.BigDecimalLayoutForm;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.phonethics.customclass.TouchImageView;


public class ImageFragment extends Fragment {

	Context context;
	File cacheDir;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	String url;
	String txtInfo;
	ProgressBar prog;
	ImageLoaderConfiguration config;
	Animation fadeinanim,fadeoutanim;
	int flag;
	TouchImageView imgView;
	TextView txtView;
	String cap;
	RelativeLayout.LayoutParams params,params2; 
	String vlink;

	private static final String DEVELOPER_KEY = "AIzaSyAx4ZzOk4_5ux4E0cC5pBVn7IrIROC0uBE";

	public ImageFragment()
	{

	}
	public ImageFragment(Context context,String url,String vlink)
	{
		this.context=context;
		this.url=url;
		this.vlink=vlink;



	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		if(savedInstanceState != null){
			context=getActivity().getApplicationContext();
		}
		
		/*
		 * Setting Config for displaying image
		 */
		flag = 0;

		imageLoader =ImageLoader.getInstance();
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"neongall");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();

		config= new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)
		
		.discCache(new UnlimitedDiscCache(cacheDir))
		

		/*.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))*/

		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();




	}
	
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		imageLoader.clearMemoryCache();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub


		if(savedInstanceState != null){
			vlink = savedInstanceState.getString("vlink");
			url = savedInstanceState.getString("ImageUrl");
			context=getActivity().getApplicationContext();
		}
 
		View view=inflater.inflate(R.layout.neoncarimgelayout, container,false);
		imgView=(TouchImageView)view.findViewById(R.id.cargallery);
		prog=(ProgressBar)view.findViewById(R.id.prog);
		params=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		params2=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		params2.addRule(RelativeLayout.CENTER_IN_PARENT);
		imgView.setLayoutParams(params);
		prog.setLayoutParams(params2);

		/*if(vlink.equals(""))
		{
			imgView.setClickable(false);
		}
		else
		{*/
		imgView.setOnClickListener(new OnClickListener() {
			int count=1;
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!vlink.equals(""))
				{
					/*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(vlink));
				startActivity(intent);*/
					/*Intent intent=new Intent(context,VideoPlayer.class);
					intent.putExtra("video_url", vlink);
					startActivity(intent);*/

					/*String vid=getYoutubeVideoId(vlink);
						Intent intent=YouTubeStandalonePlayer.createVideoIntent(
								getActivity(),DEVELOPER_KEY, vid, 0, true, false);
						startActivity(intent);*/

					try
					{
						if(YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getActivity()).equals(YouTubeInitializationResult.SUCCESS))
						{
							String vid=getYoutubeVideoId(vlink);
							Intent intent=YouTubeStandalonePlayer.createVideoIntent(
									getActivity(),DEVELOPER_KEY, vid, 0, true, false);
							startActivity(intent);
						}
						else
						{
							Toast.makeText(getActivity(), "Update or Install Youtube Player",Toast.LENGTH_SHORT).show();
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(vlink));
							startActivity(intent);
						}

					}catch(ActivityNotFoundException aex)
					{

					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
				/*else
				{
					if(count%2!=0)
					{
						NissanCarGallery.relBottomBar.startAnimation(fadeoutanim);
						NissanCarGallery.relGalleryHeader.startAnimation(fadeoutanim);
						NissanCarGallery.relInfoLayout.startAnimation(fadeoutanim);
						fadeoutanim.setFillAfter(true);

							relBottomBar.setVisibility(ViewGroup.INVISIBLE);
							relGalleryHeader.setVisibility(ViewGroup.INVISIBLE);
							relInfoLayout.setVisibility(ViewGroup.INVISIBLE);
						count++;
					}
					else if(count%2==0)
					{
						NissanCarGallery.relBottomBar.startAnimation(fadeinanim);
						NissanCarGallery.relGalleryHeader.startAnimation(fadeinanim);
						NissanCarGallery.relInfoLayout.startAnimation(fadeinanim);
						fadeoutanim.setFillAfter(true);

							relBottomBar.setVisibility(ViewGroup.VISIBLE);
							relGalleryHeader.setVisibility(ViewGroup.VISIBLE);
							relInfoLayout.setVisibility(ViewGroup.VISIBLE);
						count++;
					}
				}*/
			}
		});

		//	imgView.setMaxZoom(2f);

		/*txtView=(TextView)view.findViewById(R.id.txtInfo);*/
		/*	NissanCarGallery.txtView.setText(cap);
		NissanCarGallery.txtDesc.setText(txtInfo);*/

		/*	txtView.setText(txtInfo);*/

		/*txtView.setVisibility(View.INVISIBLE);

		txtView.setText(txtInfo);
		 */

		/*imgView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				flag++;
				Toast.makeText(context, "Clicked: "+flag, Toast.LENGTH_SHORT).show();
				if(flag%2!=0)
				{

					//		txtView.setVisibility(View.VISIBLE);
					txtView.startAnimation(fadeinanim);
					fadeinanim.setFillAfter(true);


				}
				if(flag%2==0)
				{
					txtView.startAnimation(fadeoutanim);
					//			txtView.setVisibility(View.INVISIBLE);
					fadeoutanim.setFillAfter(true);

				}

			}
		});*/



		//downloading and setting image to ImageView.
		/*imageLoader.displayImage(url, imgView, options,new ImageLoadingListener() {

			@Override
			public void onLoadingStarted() {
				// TODO Auto-generated method stub
				prog.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(FailReason arg0) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(Bitmap arg0) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled() {
				// TODO Auto-generated method stub

			}
		});
		 
*/
		imageLoader.displayImage(url, imgView, options, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}
		});

		return view;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState != null){
			vlink = savedInstanceState.getString("vlink");
			url = savedInstanceState.getString("ImageUrl");
			context=getActivity().getApplicationContext();
		}

	}


	/*
	 * Getting id of video from url
	 */
	public static String getYoutubeVideoId(String youtubeUrl)
	{
		String video_id="";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http"))
		{

			String expression = "^.*((youtu.be"+ "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches())
			{
				String groupIndex1 = matcher.group(7);
				if(groupIndex1!=null && groupIndex1.length()==11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (outState.isEmpty()) {

			outState.putString("vlink", vlink);
			outState.putString("ImageUrl", url);
			context=getActivity().getApplicationContext();
		}

	}





}
