package com.phonethics.neon;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;
//import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.phonethics.adapters.DealerAdapter;
import com.phonethics.imageloader.ImageLoader;

/*import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

 */
public class Dealer_location extends SherlockFragmentActivity implements LocationListener,OnClickListener{


	Activity			actContext;
	ImageView			dealerImg;
	TextView			nameText, addText,headerText;
	String				dlrName, dlrAdd,imgUrl,latitude,longitude;
	Bitmap 				bitmap;
	Context				context;
	GoogleMap 			googleMap;
	SupportMapFragment	mapFrag;

	String 				provider,header;
	double 				cur_latitude, cur_longitude;
	ImageView 			direction, slider_inside;
	Double				latitude1, long1;
	RelativeLayout 		contact_info,rel_slider,tempParent,map_layout,list_layout,header_layout;
	Animation 			anim;
	ArrayList<String> 	extraDetails;
	ArrayList<String> 	imgUrlArr;
	ListView			listView;
	ImageLoader imgloader;
	File cacheDir;

	int header_height;
	RelativeLayout.LayoutParams relParams;
	Map<String, String> articleParams;
	//anim = AnimationUtils.loadAnimation(context, R.anim.slider_anim_open);


	private int scwidth;
	private int scheight;

	boolean addFlury = true;

	ActionBar ac;

	Button slider,slider_hide;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dealer_location);


		ac = getSupportActionBar();
		//ac.setTitle("NISSAN");

		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();




		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);



		ConnectivityManager cm 			= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo 			= cm.getActiveNetworkInfo();

		BitmapDrawable bd=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.headerbg);
		header_height=bd.getIntrinsicHeight();



		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		//anim 		= 	new TranslateAnimation(0,200,0,0);
		context		= 	this;
		actContext	= 	this;
		dealerImg	=	(ImageView) findViewById(R.id.img_dealerImg);
		nameText	=	(TextView)	findViewById(R.id.text_dealerName);
		addText		=	(TextView)	findViewById(R.id.text_dealerAdd);
		headerText		=	(TextView)	findViewById(R.id.textview_headertext);
		listView	=	(ListView)	findViewById(R.id.lstContact);

		list_layout = (RelativeLayout) findViewById(R.id.rel_List);
		header_layout = (RelativeLayout) findViewById(R.id.layout_1);

		mapFrag		= 	(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		googleMap	= 	mapFrag.getMap();
		googleMap.setMyLocationEnabled(true);

		direction 		= (ImageView) findViewById(R.id.getDirection);
		slider 			= (Button) findViewById(R.id.slider);
		slider_hide= (Button) findViewById(R.id.slider_hide);
		contact_info 	= (RelativeLayout) findViewById(R.id.rel_contactInfo);
		map_layout 	= (RelativeLayout) findViewById(R.id.map_layout);
		imgloader = new ImageLoader(context);
		imgloader.setStubId(R.drawable.logonissan);

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		scwidth = metrics.widthPixels;
		scheight = metrics.heightPixels;

		//headerText.setVisibility(View.GONE);

		/*articleParams = new HashMap<String, String>();
        articleParams.put("App Name", "Park My Car");*/


		/*	ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.discCache(new UnlimitedDiscCache(cacheDir))
		.threadPoolSize(2)
		.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) 
		.memoryCacheExtraOptions(40, 40)
		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))
		.enableLogging()

		.build();

		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.cacheInMemory()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();


		ImageLoaderConfiguration config= new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))


		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))

		.enableLogging()
		.build();

		imgloader =  ImageLoader.getInstance();
		imgloader.init(config);

		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();*/





		if (netInfo != null && netInfo.isConnected()) {

			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(provider);

			if(location!=null){
				onLocationChanged(location);
			}
			else {

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Location Check");
				alertDialogBuilder3
				.setMessage("No location found. Please check GPS Settings")
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						Dealer_location.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		}else{

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			alertDialogBuilder.setTitle("Check Connection");
			alertDialogBuilder
			.setMessage("No internet Connection Do U Want To Exit ?")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					Dealer_location.this.finish();
				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}




		Bundle extras	= getIntent().getExtras();
		if(extras!=null){
			dlrName	=	extras.getString("dlrName");
			dlrAdd	=	extras.getString("dlrAdd");
			imgUrl	=	extras.getString("imgUrl");
			latitude	=	extras.getString("latitude");
			longitude	=	extras.getString("longitude");
			extraDetails	=	extras.getStringArrayList("extraDetails");
			imgUrlArr	=	extras.getStringArrayList("imgUrlsArr");
			header = extras.getString("header");


		}

		for(int i=0; i<extraDetails.size();i++){
			if(extraDetails.get(i) != null){
				Log.i("---->>>", "Extra Info " + i + " -- " + extraDetails.get(i));
			}
		}

		/*Toast.makeText(context, imgUrl, Toast.LENGTH_SHORT).show();*/
		nameText.setText(dlrName);
		//	nameText.setTextColor(Color.parseColor("#f7941d"));
		addText.setText(dlrAdd);
		//headerText.setText(header);

		ac.setTitle(header);
		imgloader.DisplayImage(imgUrl, dealerImg);

		DealerAdapter adapter = new DealerAdapter(actContext, extraDetails);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(extraDetails.get(position).toString().startsWith("~", 0))
				{

				}
				else if(extraDetails.get(position).toString().contains("@")){
					/*Toast.makeText(context, "Yes it contains @", Toast.LENGTH_SHORT).show();*/

					if(addFlury){
						Map<String, String> emailParam = new HashMap<String, String>();
						emailParam.put("DealerStoreName", ""+dlrName);
						emailParam.put("DealerStoreEmailId", ""+extraDetails.get(position));
						//emailParam.put("App", "NSFL");
						FlurryAgent.logEvent("DealerStoreDetail_Email_Event", emailParam);

					}
					/*Intent email = new Intent(Intent.ACTION_SEND);
					email.setType("text/plain");
					email.putExtra("android.intent.extra.EMAIL",extraDetails.get(position));
					context.startActivity(Intent.createChooser(email, "Send mail.."));*/

					Intent email=new Intent(Intent.ACTION_SEND);
					String email_to=extraDetails.get(position).toString();
					email.putExtra("android.intent.extra.EMAIL",new String[]{email_to});
					email.putExtra("android.intent.extra.SUBJECT", "");
					email.setType("text/plain");

					startActivity(Intent.createChooser(email, "Send mail..."));
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


				}else if(!extraDetails.get(position).toString().contains("@") && !extraDetails.get(position).toString().contains("www") ){

					if(addFlury){
						Map<String, String> callParam = new HashMap<String, String>();
						callParam.put("DealerStoreName", ""+dlrName);
						callParam.put("DealerStoreTel", ""+changeFormat(extraDetails.get(position).toString(),false));
						//	callParam.put("App", "NSFL");
						FlurryAgent.logEvent("DealerStoreDetail_Call_Event",callParam);
					}
					Intent call = new Intent(android.content.Intent.ACTION_DIAL);
					call.setData(Uri.parse("tel:" + changeFormat(extraDetails.get(position).toString(),false)));
					context.startActivity(call);
				}
			}
		});


		/*try {
			bitmap = BitmapFactory.decodeStream((InputStream) new URL(imgUrl).getContent());
			dealerImg.setImageBitmap(bitmap);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
		if(!latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("")){
			latitude1 = Double.parseDouble(latitude);
			long1 = Double.parseDouble(longitude);
		}else{
			latitude1=(double) 0;
			long1=(double) 0;
			map_layout.setVisibility(View.GONE);

		}
		LatLng prkpos1 = new LatLng(latitude1, long1);

		/*GoogleMap googleMap;
		Marker marker1 = googleMap.addMarker(new MarkerOptions()
		.position(prkpos1)
		.title(dlrName);*/
		/*.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));*/


		Marker marker = googleMap.addMarker(new MarkerOptions()
		.position(prkpos1)
		.title(dlrName));
		marker.setVisible(true);
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(prkpos1));
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));

		direction.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String uri = "http://maps.google.com/maps?saddr=" + cur_latitude +","+ cur_longitude +"&daddr="+ latitude1 +","+ long1;

				//Toast.makeText(getApplicationContext(), "URL IS" + " " + uri, Toast.LENGTH_SHORT).show();

				if(addFlury){
					Map<String, String> dealor = new HashMap<String, String>();
					dealor.put("DealerStoreName", ""+dlrName);
					//dealor.put("App", "NSFL");
					FlurryAgent.logEvent("DealerStoreDetail_GetDirection_Event",dealor);
				}
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				startActivity(intent);
			}
		});

		slider.setOnClickListener(this);

		slider_hide.setOnClickListener(this);
		dealerImg.setOnClickListener(this);

	}

	public String changeFormat(String data, boolean breakChar){
		String finalStr="";
		if(!breakChar){
			String dataa = data.toString().replace(" ", "");
			String data1 = dataa.toString().replace("-", "");
			String data2 = data1.toString().split("/")[0];
			finalStr = data2.toString().split(",")[0];
			return finalStr;
		}else{
			finalStr = data.toString().replace(" ", "");
			return finalStr;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		cur_latitude = location.getLatitude();

		// Getting longitude of the current location
		cur_longitude = location.getLongitude();



		// Creating a LatLng object for the current location
		//latLng = new LatLng(latitude, longitude);

		/*	Toast.makeText(context, "" + cur_latitude + "\n" + cur_longitude, Toast.LENGTH_SHORT).show();*/

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub 

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==slider.getId())
		{
			anim = new TranslateAnimation(0, 0, -365 , header_height-header_height);
			anim.setDuration(1500);



			contact_info.startAnimation(anim);
			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					contact_info.bringToFront();
						 FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

					    // Replace whatever is in the fragment_container view with this fragment,
					    // and add the transaction to the back stack if necessary 
					    transaction.replace(resource, frag);

					    transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
					    // Commit the transaction
					    transaction.commit();
										map_layout.setVisibility(View.GONE);
					list_layout.setVisibility(View.VISIBLE);
					header_layout.bringToFront();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					//slider.setVisibility(View.GONE);
					slider.setImageResource(R.drawable.slider_hide);

					slider.setVisibility(View.GONE);
					Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
					slider_hide.startAnimation(anim);
					slider_hide.setVisibility(View.VISIBLE);
					relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					relParams.setMargins(0, 3*(header_height)+(header_height/3)-5, 0, 0);
					relParams.setMargins(0, 4*(header_height)+(header_height/3)-15, 0, 0);
					contact_info.setLayoutParams(relParams);

				}
			});
			anim.setFillAfter(true);

		}

		if(v.getId()==slider_hide.getId())
		{
			anim = new TranslateAnimation(0, 0, header_height-header_height,-375 );
			anim.setDuration(1500);

			contact_info.startAnimation(anim);
			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					//contact_info.bringToFront();

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					//slider.setVisibility(View.GONE);
					slider.setImageResource(R.drawable.slider_hide);

					slider_hide.setVisibility(View.GONE);
					Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
					slider.startAnimation(anim);

					slider.setVisibility(View.VISIBLE);
					contact_info.bringToFront();
					relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					relParams.setMargins(0,4*(header_height)+2, 0,0);
					contact_info.setLayoutParams(relParams);
					slider.setClickable(true);

					map_layout.setVisibility(View.VISIBLE);
					list_layout.setVisibility(View.GONE);
					header_layout.bringToFront();
				}
			});
			//anim.setFillAfter(true);

		}

		if(v.getId()==dealerImg.getId()){
			if(imgUrlArr.size()!=0){
				Intent pagerIntent = new Intent(context, DealerViewPager.class);
				pagerIntent.putStringArrayListExtra("imgUrlArr", imgUrlArr);
				startActivity(pagerIntent);
			}
			Log.d("==== URL LINK ====",""+imgUrl );
			Toast.makeText(context, imgUrl, Toast.LENGTH_SHORT).show();
		}
	}*/


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==slider.getId())
		{

			/*		anim = new TranslateAnimation(0, 0, -240 , header_height-header_height);*/
			/*relParams.setMargins(0, 3*(header_height)+(header_height/3)-5, 0, 0);*/

			int fromXDelta;
			int toXDelta;
			int fromYDelta;
			int toYDelta;

			int left;
			int top;
			int right;
			int bottom;

			if(scheight==800 || scheight==854)
			{
				fromXDelta=0;
				toXDelta=0;
				fromYDelta=-260;
				toYDelta=header_height-header_height;

				left=0;
				top=3*(header_height)+(header_height/3)-13;
				right=0;
				bottom=0;

				slide_fromUp_toDown(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);

			}
			else if(scheight==480)
			{
				fromXDelta=0;
				toXDelta=0;
				fromYDelta=-180;
				toYDelta=header_height-header_height;

				left=0;
				top=3*(header_height);
				right=0;
				bottom=0;

				slide_fromUp_toDown(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);
			}else if(scheight==1280){

				/*fromXDelta=0;
				toXDelta=0;
				fromYDelta=-header_height-header_height;
				toYDelta=-375;

				left=0;
				top=4*(header_height)+(header_height/3)-15;
				right=0;
				bottom=0;

				slide_fromUp_toDown(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);*/

				/*
				anim = new TranslateAnimation(0, 0, -365 , header_height-header_height);
				anim.setDuration(1500);



				contact_info.startAnimation(anim);
				anim.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						contact_info.bringToFront();
							 FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

						    // Replace whatever is in the fragment_container view with this fragment,
						    // and add the transaction to the back stack if necessary 
						    transaction.replace(resource, frag);

						    transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
						    // Commit the transaction
						    transaction.commit();
											map_layout.setVisibility(View.GONE);
						list_layout.setVisibility(View.VISIBLE);
						header_layout.bringToFront();
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						//slider.setVisibility(View.GONE);
						//slider.setImageResource(R.drawable.slider_hide);
				 */
				list_layout.setVisibility(View.VISIBLE);
				slider.setVisibility(View.GONE);
				//						Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				//						slider_hide.startAnimation(anim);
				slider_hide.setVisibility(View.VISIBLE);
				relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//relParams.setMargins(0, 3*(header_height)+(header_height/3)-5, 0, 0);
				relParams.setMargins(0, 4*(header_height)+(header_height/3)+10-header_height, 0, 0);
				contact_info.setLayoutParams(relParams);
				/*
					}
				});
				anim.setFillAfter(true);*/

			}

			else{

				fromXDelta=0;
				toXDelta=0;
				fromYDelta=-180;
				toYDelta=header_height-header_height;

				left=0;
				top=4*(header_height);
				right=0;
				bottom=0;

				slide_fromUp_toDown(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);
			}

		}

		if(v.getId()==slider_hide.getId())
		{

			int fromXDelta;
			int toXDelta;
			int fromYDelta;
			int toYDelta;

			int left;
			int top;
			int right;
			int bottom;

			/*anim = new TranslateAnimation(0, 0, header_height-header_height,-248 );*/
			/*relParams.setMargins(0,3*(header_height)+5, 0,0);*/

			if(scheight==800 || scheight==854)
			{
				fromXDelta=0;
				toXDelta=0;
				fromYDelta=header_height-header_height;
				toYDelta=-248;

				left=0;
				top=3*(header_height)+5;
				right=0;
				bottom=0;

				slide_fromDown_toUp(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);
			}
			else if(scheight==480)
			{
				fromXDelta=0;
				toXDelta=0;
				fromYDelta=header_height-header_height;
				toYDelta=-165;

				left=0;
				top=3*(header_height)-5;
				right=0;
				bottom=0;

				slide_fromDown_toUp(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);
			}else if(scheight==1280){

				/*fromXDelta=0;
				toXDelta=0;
				fromYDelta=-365;
				toYDelta=header_height-header_height;

				left=0;
				top=4*(header_height)+2;
				right=0;
				bottom=0;

				slide_fromDown_toUp(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);*/


				/*anim = new TranslateAnimation(0, 0, header_height-header_height,-375 );
				anim.setDuration(1500);

				contact_info.startAnimation(anim);
				anim.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						//contact_info.bringToFront();

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						//slider.setVisibility(View.GONE);
						slider.setImageResource(R.drawable.slider_hide);
				 */
				slider_hide.setVisibility(View.GONE);
				//						Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				//						slider.startAnimation(anim);

				slider.setVisibility(View.VISIBLE);
				/*contact_info.bringToFront();*/
				relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				relParams.setMargins(0,4*(header_height)+2-header_height+header_height-8, 0,0);
				contact_info.setLayoutParams(relParams);
				slider.setClickable(true);

				/*map_layout.setVisibility(View.VISIBLE);*/
				list_layout.setVisibility(View.GONE);
				header_layout.bringToFront();
				/*	}
				});


				 */
			}

			else{

				fromXDelta=0;
				toXDelta=0;
				fromYDelta=header_height-header_height;
				toYDelta=-248;

				left=0;
				top=4*(header_height);
				right=0;
				bottom=0;

				slide_fromDown_toUp(fromXDelta, toXDelta, fromYDelta, toYDelta, left, top, right, bottom);
			}

			//anim.setFillAfter(true);



		}

		if(v.getId()==dealerImg.getId()){
			if(imgUrlArr.size()!=0){
				Intent pagerIntent = new Intent(context, DealerViewPager.class);
				pagerIntent.putStringArrayListExtra("imgUrlArr", imgUrlArr);
				startActivity(pagerIntent);
			}/*
			Log.d("==== URL LINK ====",""+imgUrl );
			Toast.makeText(context, imgUrl, Toast.LENGTH_SHORT).show();*/
		}
	}


	void slide_fromUp_toDown(int fromXDelta,int toXDelta,int fromYDelta,int toYDelta,final int left,final int top,final int right,final int bottom)
	{



		/*		anim = new TranslateAnimation(0, 0, -240 , header_height-header_height);*/
		/*	anim = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta ,toYDelta);
		anim.setDuration(1500);

		contact_info.startAnimation(anim);

		anim.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				//contact_info.bringToFront();

				map_layout.setVisibility(View.GONE);
				header_layout.bringToFront();*/
		list_layout.setVisibility(View.VISIBLE);

		/*}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				//slider.setVisibility(View.GONE);
				slider.setImageResource(R.drawable.slider_hide);

		 */				slider.setVisibility(View.GONE);
		 /*Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				slider_hide.startAnimation(anim);
		  */				slider_hide.setVisibility(View.VISIBLE);
		  relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		  /*relParams.setMargins(0, 3*(header_height)+(header_height/3)-5, 0, 0);*/

		  /*relParams.setMargins(0, 3*(header_height)+(header_height/3)-5, 0, 0);*/
		  relParams.setMargins(left, top-header_height+18, right, bottom);
		  contact_info.setLayoutParams(relParams);
		  /*
			}
		});
		anim.setFillAfter(true);*/
	}

	void slide_fromDown_toUp(int fromXDelta,int toXDelta,int fromYDelta,int toYDelta,final int left,final int top,final int right,final int bottom)
	{
		/*anim = new TranslateAnimation(0, 0, header_height-header_height,-248 );*/
		//		anim = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta ,toYDelta);
		//		anim.setDuration(1500);
		//
		//		contact_info.startAnimation(anim);
		//
		//		anim.setAnimationListener(new AnimationListener() {
		//
		//			@Override
		//			public void onAnimationStart(Animation animation) {
		//				// TODO Auto-generated method stub
		//				//contact_info.bringToFront();
		//
		//			}
		//
		//			@Override
		//			public void onAnimationRepeat(Animation animation) {
		//				// TODO Auto-generated method stub
		//
		//			}
		//
		//			@Override
		//			public void onAnimationEnd(Animation animation) {
		//				// TODO Auto-generated method stub
		//				//slider.setVisibility(View.GONE);
		//				/*slider.setImageResource(R.drawable.slider_hide);*/

		slider_hide.setVisibility(View.GONE);
		//				Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
		//				slider.startAnimation(anim);

		slider.setVisibility(View.VISIBLE);
		//contact_info.bringToFront();
		relParams=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		/*relParams.setMargins(0,3*(header_height)+5, 0,0);*/
		relParams.setMargins(left, top-header_height+5+20 , right, bottom);
		contact_info.setLayoutParams(relParams);
		//				slider.setClickable(true);

		/*map_layout.setVisibility(View.VISIBLE);*/
		list_layout.setVisibility(View.GONE);
		//				//header_layout.bringToFront();
		//			}
		//		});

	}




	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		if(addFlury){
			FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");

			Map<String, String> dealor = new HashMap<String, String>();
			dealor.put("DealerStoreName", ""+dlrName);
			//dealor.put("App", "NSFL");

			FlurryAgent.logEvent("DealerStoreDetail_View_Event",dealor);
		}
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		if(addFlury){
			FlurryAgent.onEndSession(this);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase(header)){

			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		return true;
	}



}
