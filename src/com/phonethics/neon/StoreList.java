package com.phonethics.neon;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.DealersListAdapters;
//import com.flurry.android.FlurryAgent;
import com.phonethics.customclass.DealerLocationAndAddress;
import com.phonethics.network.NetworkCheck;
import com.phonethics.parser.DealerLocatorParser;

public class StoreList extends SherlockActivity {

	ExpandableListView 					citylist;
	String						DEALERS_XML	="/sdcard/neoncache/dealers.xml";
	String						DEALER_URL,	xml;
	Context						context;
	NetworkCheck				netCheck;
	DownloadFromURL				downloadFile;
	DefaultHttpClient			httpClient;
	HttpPost					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;
	FileInputStream 			filename;
	List<DealerLocationAndAddress> 				dealers = null;

	ArrayList<String> 			allDealers, dealerAddress,extraDetails,photo1, photo2,phone1,phone2,mobile1,mobile2,fax,email,website;
	ArrayList<String> 			latArr;
	ArrayList<String> 			longArr;
	ArrayList<String> 			dealersName;
	Activity					context1;
	ProgressDialog				pDialog;
	public static final int 				progress_bar_type = 1; 
	ImageView					img_nearme;
	int count=0;
	
	Map<String, String> articleParams = new HashMap<String, String>();
	ActionBar ac;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_list);

		context				= this;
		context1			= this;
		
		ac = getSupportActionBar();
		ac.setTitle("Store List");
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();
		
		

		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);
		
		//Flurry Event Log.
				articleParams.put("App", "Neon"); // Capture author info
		citylist 			= (ExpandableListView) findViewById(R.id.ListLocators);
		netCheck			= new NetworkCheck(context);
		DEALER_URL			= getString(R.string.dealer_locator_url);

		allDealers			= new ArrayList<String>();
		latArr				= new ArrayList<String>();
		longArr				= new ArrayList<String>();
		dealerAddress		= new ArrayList<String>();
		dealersName			= new ArrayList<String>();
		extraDetails		= new ArrayList<String>();

		photo1				= new ArrayList<String>();
		photo2				= new ArrayList<String>();
		phone1				= new ArrayList<String>();
		phone2				= new ArrayList<String>();
		mobile1				= new ArrayList<String>();
		mobile2				= new ArrayList<String>();
		fax					= new ArrayList<String>();
		email				= new ArrayList<String>();
		website				= new ArrayList<String>();

		//img_nearme			= (ImageView) findViewById(R.id.img_nearme);


		if(netCheck.isNetworkAvailable()){

			File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

			if(!myDirectory.exists()) {                                 
				myDirectory.mkdirs();
			}

			File	xmlFile	=	new	File(DEALERS_XML);
			if(xmlFile.exists()){
				/*Toast.makeText(context, "File Exsits", Toast.LENGTH_SHORT).show();*/
				try{
					filename = new FileInputStream(DEALERS_XML);
					parseDoc(filename);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else{

				downloadFile=new DownloadFromURL();
				downloadFile.execute(DEALER_URL);	
			}
		}else{
			File	xmlFile	=	new	File(DEALERS_XML);
			if(xmlFile.exists()){

				try{
					filename = new FileInputStream(DEALERS_XML);
					parseDoc(filename);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else{
				Toast.makeText(context, "Check internet connection", Toast.LENGTH_SHORT).show();
			}
		}


		citylist.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				return true;
			}
		});

		/*img_nearme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//FlurryAgent.logEvent("Dealer_StoreLocator_NearByMe_Map_Event",articleParams);
				
				Intent intent = new Intent(context, NearMe.class);
				intent.putStringArrayListExtra("latArr", latArr);
				intent.putStringArrayListExtra("longArr", longArr);
				intent.putStringArrayListExtra("dealersName", dealersName);
				intent.putStringArrayListExtra("dealerAddress", dealerAddress);
				intent.putStringArrayListExtra("phone1", phone1);
				intent.putStringArrayListExtra("phone2", phone2);
				intent.putStringArrayListExtra("mobile1", mobile1);
				intent.putStringArrayListExtra("mobile2", mobile2);
				intent.putStringArrayListExtra("fax", fax);
				intent.putStringArrayListExtra("email", email);
				intent.putStringArrayListExtra("website", website);
				intent.putStringArrayListExtra("photo1", photo1);
				intent.putStringArrayListExtra("photo2", photo2);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
		});*/


	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");

	}


	@Override
	protected void onStop() { 
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	


	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			/*pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);*/
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.setCancelable(true);
			pDialog.show();


			pDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Log.d("------------","**** pDIALOG OFF *****");
					Log.d("------------","**** running off *****");
					try{
						File externalFile = new File(Environment.getExternalStorageDirectory(),"neoncache/dealers.xml");
						externalFile.delete();
						externalFile = new File(Environment.getExternalStorageDirectory(),"neoncache/dealers.xml");
						if(!externalFile.exists()){
							Log.d("------------","**** No Such File *****");
						}
					}catch(Exception ex){

					}
				}
			});

			return pDialog;
		default:
			return null;
		}
	}

	public class DownloadFromURL extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {

				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(DEALER_URL);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();
				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				// Output stream to write file
				FileOutputStream output = new FileOutputStream(DEALERS_XML);

				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1) ) {
					// writing data to file
					total += count;
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();

			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub


			try {
				removeDialog(progress_bar_type);
				filename = new FileInputStream(DEALERS_XML);
				parseDoc(filename);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			super.onPostExecute(result);
		}


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Store List")){
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		
		if(item.getTitle().toString().equalsIgnoreCase("Nearby")){


			FlurryAgent.logEvent("Dealer_StoreLocator_NearByMe_Map_Event");
			Intent intent = new Intent(context, NearMe.class);
			intent.putStringArrayListExtra("latArr", latArr);
			intent.putStringArrayListExtra("longArr", longArr);
			intent.putStringArrayListExtra("dealersName", dealersName);
			intent.putStringArrayListExtra("dealerAddress", dealerAddress);
			intent.putStringArrayListExtra("phone1", phone1);
			intent.putStringArrayListExtra("phone2", phone2);
			intent.putStringArrayListExtra("mobile1", mobile1);
			intent.putStringArrayListExtra("mobile2", mobile2);
			intent.putStringArrayListExtra("fax", fax);
			intent.putStringArrayListExtra("email", email);
			intent.putStringArrayListExtra("website", website);
			intent.putStringArrayListExtra("photo1", photo1);
			intent.putStringArrayListExtra("photo2", photo2);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		
		return true;

	}

	public void parseDoc(FileInputStream fs){

		dealers = DealerLocatorParser.parse(fs);
		/*Toast.makeText(context, "File INput Dealers Size " + dealers.size(), Toast.LENGTH_SHORT).show();*/
		for(int i=0; i<dealers.size();i++){
			allDealers.add(dealers.get(i).getCityName());
			/*latArr.add(dealers.get(i).getDealerlatitude());
			longArr.add(dealers.get(i).getDealerlongitude());*/
			Log.i("===>>>", " City Name ===>>> " + dealers.get(i).getCityName());
		} 

		/*ArrayAdapter<String> array1 = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,allDealers);
		citylist.setAdapter(array1);*/
		/*DealersListAdapters	adapter = new DealersListAdapters(context1, dealers);
		citylist.setAdapter(adapter);*/

		/*for(int i=0; i<dealers.size();i++){
			int totalDealerInaCity = dealers.get(i).dlrName.size();
			for(int p=0;p<dealers.get(i).dlrName.size();p++){
				latArr.add(dealers.get(i).dlrlatitude.get(p));
				longArr.add(dealers.get(i).dlrlongitude.get(p));
			}
		}*/

		for(int p=0;p<dealers.size();p++){
			Log.d("===============", "Nissan Neon =======> City " + dealers.get(p).getCityName());
			for(int i=0;i<dealers.get(p).dlrName.size();i++){
				Log.d("======", "Nissan Neon ==> Long " + dealers.get(p).dlrName.get(i));
				Log.d("======", "Nissan Neon ==> Lat " + dealers.get(p).dlrlatitude.get(i));
				Log.d("======", "Nissan Neon ==> Long " + dealers.get(p).dlrlongitude.get(i));
				
				latArr.add(dealers.get(p).dlrlatitude.get(i));
				longArr.add(dealers.get(p).dlrlongitude.get(i));
				dealersName.add(dealers.get(p).dlrName.get(i));
				dealerAddress.add(dealers.get(p).dlrAddress.get(i));
				
				photo1.add(dealers.get(p).dlrPhoto1.get(i));
				photo2.add(dealers.get(p).dlrPhoto2.get(i));
				phone1.add(dealers.get(p).dlrPhone1.get(i));
				phone2.add(dealers.get(p).dlrPhone2.get(i));
				mobile1.add(dealers.get(p).dlrMobile1.get(i));
				mobile2.add(dealers.get(p).dlrMobile2.get(i));
				fax.add(dealers.get(p).dlrFax.get(i));
				email.add(dealers.get(p).dlrEmail.get(i));
				website.add(dealers.get(p).dlrWebsite.get(i));
				
				/*extraDetails.add(phone1);
				extraDetails.add(phone2);
				extraDetails.add(mobile1);
				extraDetails.add(mobile2);
				extraDetails.add(fax);
				extraDetails.add(email);
				extraDetails.add(website);*/
				
				count++;
			}
		}

/*		Toast.makeText(context, "Size " + dealersName.size() + "/" + count , 0).show();*/
		
		DealersListAdapters adapter1 = new DealersListAdapters(context1, dealers);
		citylist.setAdapter(adapter1);
		citylist.setGroupIndicator(null);


		for(int i=0;i<adapter1.getGroupCount();i++){
			citylist.expandGroup(i);
		}

	}
	public void parseDoc(InputStream fs){

		dealers = DealerLocatorParser.parse(fs);
		/*Toast.makeText(context, "InputStream Dealers Size " + dealers.size(), Toast.LENGTH_SHORT).show();*/
		for(int i=0; i<dealers.size();i++){
			allDealers.add(dealers.get(i).getCityName());

			/*latArr.add(dealers.get(i).dlrlatitude.size());*/
			/*longArr.add(dealers.get(i).getDealerlongitude());*/
			Log.d("===>>>", " City name  ===>>> " + dealers.get(i).getCityName());
		} 

		/*ArrayAdapter<String> array1 = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,allDealers);
		citylist.setAdapter(array1);
		 */

		/*for(int i=0; i<dealers.size();i++){
			int totalDealerInaCity = dealers.get(i).dlrName.size();
			for(int p=0;p<dealers.get(i).dlrName.size();p++){
				latArr.add(dealers.get(i).dlrlatitude.get(p));
				longArr.add(dealers.get(i).dlrlongitude.get(p));
			}
		}*/
		for(int p=0;p<dealers.size();p++){
			Log.d("===============", "Nissan Neon =======> City " + dealers.get(p).getCityName());
			for(int i=0;i<dealers.get(p).dlrName.size();i++){
				Log.d("======", "Nissan Neon ==> Long " + dealers.get(p).dlrName.get(i));
				Log.d("======", "Nissan Neon ==> Lat " + dealers.get(p).dlrlatitude.get(i));
				Log.d("======", "Nissan Neon ==> Long " + dealers.get(p).dlrlongitude.get(i));
				
				latArr.add(dealers.get(p).dlrlatitude.get(i));
				longArr.add(dealers.get(p).dlrlongitude.get(i));
				dealersName.add(dealers.get(p).dlrName.get(i));
				dealerAddress.add(dealers.get(p).dlrAddress.get(i));
				
				photo1.add(dealers.get(p).dlrPhoto1.get(i));
				photo2.add(dealers.get(p).dlrPhoto2.get(i));
				phone1.add(dealers.get(p).dlrPhone1.get(i));
				phone2.add(dealers.get(p).dlrPhone2.get(i));
				mobile1.add(dealers.get(p).dlrMobile1.get(i));
				mobile2.add(dealers.get(p).dlrMobile2.get(i));
				fax.add(dealers.get(p).dlrFax.get(i));
				email.add(dealers.get(p).dlrEmail.get(i));
				website.add(dealers.get(p).dlrWebsite.get(i));
				
				/*String phone1 = dealers.get(p).dlrPhone1.get(i);
				String phone2 = dealers.get(p).dlrPhone2.get(i);
				String mobile1 = dealers.get(p).dlrMobile1.get(i);
				String mobile2 = dealers.get(p).dlrMobile2.get(i);
				String fax = dealers.get(p).dlrFax.get(i);
				String email = dealers.get(p).dlrEmail.get(i);
				String website = dealers.get(p).dlrWebsite.get(i);
				
				extraDetails.add(phone1);
				extraDetails.add(phone2);
				extraDetails.add(mobile1);
				extraDetails.add(mobile2);
				extraDetails.add(fax);
				extraDetails.add(email);
				extraDetails.add(website);*/
				
				count++;
			}
		}
	/*	Toast.makeText(context, "Size " + dealersName.size() + "/" + count , 0).show();*/

		DealersListAdapters adapter1 = new DealersListAdapters(context1, dealers);
		citylist.setAdapter(adapter1);
		citylist.setGroupIndicator(null);


		for(int i=0;i<adapter1.getGroupCount();i++){
			citylist.expandGroup(i);
		}




	}
	
	

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */
		MenuItem menu_near = menu.add("Nearby");
		menu_near.setIcon(R.drawable.nearby);
		menu_near.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

}

