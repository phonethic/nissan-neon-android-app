package com.phonethics.neon;







import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class SplashScreen extends Activity {
	private Context context;
	VideoView vd;
	RelativeLayout	layout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = getApplicationContext();
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash_screen);
		
		
		vd= (VideoView)findViewById(R.id.videoview);
		layout = (RelativeLayout) findViewById(R.id.layout_video);
		layout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//if(vd.isPlaying()){
					vd.stopPlayback();
					Intent mainactivity = new Intent(context,  Actionbar.class);
					startActivity(mainactivity);
					finish();
					overridePendingTransition(0, 0);
				//}
			}
		});
		showVideo();
		
	}

	private void showVideo()
	{
	
		Uri uri = Uri.parse("android.resource://"+ getPackageName() +"/" +R.raw.logoanimation);
		//MediaController mc = new MediaController(this);
		//vd.setMediaController(mc);
		vd.setVideoURI(uri);
		vd.start();
		vd.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				Intent mainactivity = new Intent(context,  Actionbar.class);
				startActivity(mainactivity);
				finish();
				overridePendingTransition(0, 0);
			}
		});
		vd.setOnErrorListener(new OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				switch (what) {
				case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
					Log.e(getClass().getSimpleName(), "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK");
					break;
				case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
					Log.e(getClass().getSimpleName(), "MEDIA_ERROR_SERVER_DIED");
					break;
				case MediaPlayer.MEDIA_ERROR_UNKNOWN:
					Log.e(getClass().getSimpleName(), "MEDIA_ERROR_UNKNOWN");
					break;
				default:
					Log.e(getClass().getSimpleName(), "default");
					break;
				}
				return false;
			}

		});
	}



	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_add_remeinders, menu);
		return false;
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		Intent mainactivity = new Intent(context,  Actionbar.class);
		startActivity(mainactivity);
		finish();
		overridePendingTransition(0, 0);
		super.onRestart();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		vd=null;
		super.onDestroy();
	}
}
