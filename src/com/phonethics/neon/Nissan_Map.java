package com.phonethics.neon;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @class Nissan_Map.java
 * 
 * Shows Google Map
 */

public class Nissan_Map extends SherlockFragmentActivity implements LocationListener {


	GoogleMap googleMap;
	Button prk, direction;
	double latitude,longitude, saved_lati, saved_longi;
	LatLng latLng;
	public static final String SHARED_PREFERENCES_NAME = "save_lati_longi";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	boolean	selfTabVisible = true;
	SharedPreferences prefs;
	Editor editor ;
	Context con;
	Marker marker, marker1;
	String address;

	String provider;


	Map<String, String> articleParams = new HashMap<String, String>();

	ActionBar ac;

	public static boolean isInOther = false;


	@Override
	public void onCreate(Bundle savedInstanceState) {

		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_nissan__map);


		ac = getSupportActionBar();
		ac.setTitle("Find My Car");

		ac.show();


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);


		try
		{
			ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();

			con = this;

			//Flurry Event Log.
			//articleParams.put("App", "Neon"); // Capture author info

			prk = (Button) findViewById(R.id.park_button);
			direction = (Button) findViewById(R.id.getDirection);
			//	distance = (TextView) findViewById(R.id.current_stat);



			// Getting reference to the SupportMapFragment of activity_main.xml
			SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

			// Getting GoogleMap object from the fragment
			googleMap = fm.getMap();

			// Enabling MyLocation Layer of Google Map
			googleMap.setMyLocationEnabled(true);
			googleMap.setTrafficEnabled(true);

			// Getting LocationManager object from System Service LOCATION_SERVICE
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			// Creating a criteria object to retrieve provider


			if (netInfo != null && netInfo.isConnected()) {

				Criteria criteria = new Criteria();
				provider = locationManager.getBestProvider(criteria, false);
				Location location = locationManager.getLastKnownLocation(provider);

				if(location!=null){
					onLocationChanged(location);
				}
				else {

					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(con);
					alertDialogBuilder3.setTitle("Location Check");
					alertDialogBuilder3
					.setMessage("No location found. Please check GPS settings")
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Intent intent=new Intent(getApplicationContext(),Actionbar.class);
							startActivity(intent);
							Nissan_Map.this.finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

						}
					})
					;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();
				}
			}else{

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);
				alertDialogBuilder.setTitle("Check Connection");
				alertDialogBuilder
				.setMessage("No internet connection do you want to exit ?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						Intent intent=new Intent(getApplicationContext(),Actionbar.class);
						startActivity(intent);
						Nissan_Map.this.finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

					}
				})
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				AlertDialog alertDialog = alertDialogBuilder.create();

				alertDialog.show();
			}



			SharedPreferences shrd = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
			saved_lati = (double)shrd.getFloat(LATITUDE, 0);
			saved_longi = (double)shrd.getFloat(LONGITUDE, 0);

			address = "";
			Geocoder geoCoder = new Geocoder(
					getBaseContext(), Locale.getDefault());
			try {
				List<Address> addresses = geoCoder.getFromLocation(
						saved_lati, saved_longi, 1);

				if (addresses.size() > 0) {
					for (int index = 0; 
							index < addresses.get(0).getMaxAddressLineIndex(); index++)
						address += addresses.get(0).getAddressLine(index) + " ";
				}
			}catch (IOException e) {        
				e.printStackTrace();
			}   


			if(saved_lati!=0 && saved_longi!=0){

				//Toast.makeText(getApplicationContext(), "PArkd alrdy", Toast.LENGTH_LONG).show();

				LatLng prkpos1 = new LatLng(saved_lati, saved_longi);

				marker1 = googleMap.addMarker(new MarkerOptions()
				.position(prkpos1)
				.title("Car Parked Here")
				.snippet(address)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

				marker1.setVisible(true);


			}


			prk.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					FlurryAgent.logEvent("CarPark_Event");

					if(latLng==null){
						Toast.makeText(getApplicationContext(), "No current position available", Toast.LENGTH_SHORT).show();
					}
					else{


						if(saved_lati==0 && saved_longi==0)
						{

							prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
							editor = prefs.edit();

							editor.putFloat(LATITUDE, (float) latitude);
							editor.putFloat(LONGITUDE, (float) longitude);
							editor.commit();


							//Toast.makeText(getApplicationContext(), "1st time", Toast.LENGTH_LONG).show();
							googleMap.clear();

							saved_lati = latitude;

							saved_longi = longitude;

							/*	prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, getApplicationContext().MODE_WORLD_READABLE);
						editor = prefs.edit();

						editor.putFloat(LATITUDE, (float) latitude);
						editor.putFloat(LONGITUDE, (float) longitude);
						editor.commit();
							 */
							LatLng prkpos = new LatLng(saved_lati, saved_longi);

							address = "";
							Geocoder geoCoder = new Geocoder(
									getBaseContext(), Locale.getDefault());
							try {
								List<Address> addresses = geoCoder.getFromLocation(
										saved_lati, saved_longi, 1);

								if (addresses.size() > 0) {
									for (int index = 0; 
											index < addresses.get(0).getMaxAddressLineIndex(); index++)
										address += addresses.get(0).getAddressLine(index) + " ";
								}
							}catch (IOException e) {        
								e.printStackTrace();
							}   

							marker = googleMap.addMarker(new MarkerOptions()
							.position(prkpos)
							.title("Car Parked Here")
							.snippet(address)
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

							marker.setVisible(true);


						}

						else{


							//Toast.makeText(getApplicationContext(), "already parked", Toast.LENGTH_SHORT).show();
							AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(con);
							alertDialogBuilder1.setTitle("Nissan Car Park");
							alertDialogBuilder1
							.setMessage("This will erase your previous parking location. Do you want to proceed?")
							.setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {

									prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
									editor = prefs.edit();

									editor.putFloat(LATITUDE, (float) latitude);
									editor.putFloat(LONGITUDE, (float) longitude);
									editor.commit();


									//Toast.makeText(getApplicationContext(), "Erased", Toast.LENGTH_SHORT).show();

									saved_lati = latitude;

									saved_longi = longitude;

									googleMap.clear();

									/*	prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, getApplicationContext().MODE_WORLD_READABLE);
								editor = prefs.edit();

								editor.putFloat(LATITUDE, (float) latitude);

								editor.putFloat(LONGITUDE, (float) longitude);
								editor.commit();

								Toast.makeText(con, " " + latitude + " " + longitude, Toast.LENGTH_SHORT).show();
									 */
									LatLng prkpos2 = new LatLng(latitude, longitude);

									address = "";
									Geocoder geoCoder = new Geocoder(
											getBaseContext(), Locale.getDefault());
									try {
										List<Address> addresses = geoCoder.getFromLocation(
												saved_lati, saved_longi, 1);

										if (addresses.size() > 0) {
											for (int index = 0; 
													index < addresses.get(0).getMaxAddressLineIndex(); index++)
												address += addresses.get(0).getAddressLine(index) + " ";
										}
									}catch (IOException e) {        
										e.printStackTrace();
									}   

									marker = googleMap.addMarker(new MarkerOptions()
									.position(prkpos2)
									.title("Car Parked Here")
									.snippet(address)
									.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));


									marker.setPosition(prkpos2);

									marker.setVisible(true);


								}
							})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {

									dialog.cancel();
								}
							});

							AlertDialog alertDialog1 = alertDialogBuilder1.create();

							alertDialog1.show();
						}

					}


				}
			});

			direction.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					FlurryAgent.logEvent("GetDirection_MyCar_Event");

					if(saved_lati==0 && saved_longi==0){

						Toast.makeText(getApplicationContext(), "Park your car first before getting directions", Toast.LENGTH_LONG).show();

					}
					else{

						//Toast.makeText(getApplicationContext(), "PArkd " + latitude + "\n" + saved_lati, Toast.LENGTH_LONG).show();
						String uri = "http://maps.google.com/maps?saddr=" + latitude +","+ longitude +"&daddr="+ saved_lati +","+ saved_longi;

						//Toast.makeText(getApplicationContext(), "URL IS" + " " + uri, Toast.LENGTH_SHORT).show();

						Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
						intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
						startActivity(intent);
					}
				}
			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");

		if(isInOther){

			isInOther = false;
			finish();
		}

	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);
		

	}



	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		latitude = location.getLatitude();

		// Getting longitude of the current location
		longitude = location.getLongitude();

		// Creating a LatLng object for the current location
		latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finish();
		//	overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

		/*MenuItem subMenu2=menu.add("Add");
		subMenu2.setIcon(R.drawable.content_add);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		 */


		SubMenu subMenu1 = menu.addSubMenu("Action Item");



		subMenu1.add("Home").setIcon(R.drawable.home);
		if(selfTabVisible){
			subMenu1.add("Find My Car").setIcon(R.drawable.location);
		}
		subMenu1.add("Reminders").setIcon(R.drawable.reminder);
		subMenu1.add("Traffic Rules").setIcon(R.drawable.traffic);
		subMenu1.add("Tips").setIcon(R.drawable.tips);
		subMenu1.add("My Car").setIcon(R.drawable.mycar);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us"))
		{
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Home"))
		{
			Intent intent=new Intent(this,Actionbar.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips"))
		{
			
			FlurryAgent.logEvent("Tips_Tab_Event");
			
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}

		return true;
	}



}