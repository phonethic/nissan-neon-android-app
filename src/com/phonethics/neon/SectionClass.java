package com.phonethics.neon;

import java.util.ArrayList;

public class SectionClass {

	private String id;
	private String ilink;
	private String vlink;
	
	ArrayList<String> idPhoto = new ArrayList<String>();
	ArrayList<String> idilink= new ArrayList<String>();
	ArrayList<String> idvlink= new ArrayList<String>();
	
	public ArrayList<String> getIdPhoto() {
		return idPhoto;
	}
	
	public void setIdPhoto(String id) {
		idPhoto.add(id);
	}
	
	public ArrayList<String> getIdilink() {
		return idilink;
	}
	
	public void setIdilink(String ilink) {
		idilink.add(ilink);
	}
	
	public ArrayList<String> getIdvlink() {
		return idvlink;
	}
	
	public void setIdvlink(String vlink) {
		idvlink.add(vlink);
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getIlink() {
		return ilink;
	}
	public void setIlink(String ilink) {
		this.ilink = ilink;
	}
	public String getVlink() {
		return vlink;
	}
	public void setVlink(String vlink) {
		this.vlink = vlink;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString();
		
		 return id + ": " + id + "\n" + ilink + "-" + vlink;
	}

	
}
