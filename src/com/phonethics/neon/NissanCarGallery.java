package com.phonethics.neon;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.flurry.android.FlurryAgent;
import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.customviewpager.JazzyViewPager.TransitionEffect;
import com.phonethics.network.NetworkCheck;
import com.phonethics.parser.SAXXMLParser;
import com.phonethics.scroller.FixedSpeedScroller;

public class NissanCarGallery extends FragmentActivity implements OnClickListener {

	ArrayList<String> link,caption,description,carName,vlinks, imgLinks,imgDesc,imgCaption,vlinksArr;
	ArrayList<NeonGalleryLinks> neongallery;
	JazzyViewPager mPager;
	Context context;
	Activity acontext;
	TextView txtView,txtDesc;

	RelativeLayout relGalleryHeader,relBottomBar;
	RelativeLayout relInfoLayout;
	TextView  txtImageInfo;
	public Animation fadeinanim,fadeoutanim;

	GestureDetector gdt= new GestureDetector(new GestureListener());

	int SWIPE_MIN_DISTANCE;
	private int scwidth;

	String xmlurl			= "http://stage.phonethics.in/proj/neon/neon_gallery.php";


	DefaultHttpClient 	httpClient;
	HttpPost 			httpPost;
	HttpResponse		httpRes;
	HttpEntity			httpEnt;
	String 				xml,NEONXML="/sdcard/neoncache/neongallery1.xml";
	FileInputStream		filename;

	NetworkCheck		netAvailable;
	FileFromURL 		downloadFile;

	RelativeLayout.LayoutParams params;
	LikeUrl likeUrl;
	String status;

	// Progress Dialog
	private ProgressDialog pDialog;

	public static final int progress_bar_type = 0; 

	ImageView galBack,galForth;
	ImageView imgLike;

	int	CACHE_VALIDITY;
	String fbUserid="";
	String fbAccessToken="";
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;

	Map<String, String> articleParams = new HashMap<String, String>();

	public static boolean call = false, clarNotify = false,isBack=false;

	Bundle	bundle;
	boolean callNet = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_nissan_car_gallery);

		context=this;
		acontext=this;
		netAvailable = new NetworkCheck(context);
		mPager=(JazzyViewPager)findViewById(R.id.viewPager);
		mPager.setTransitionEffect(TransitionEffect.Standard);

		//Flurry Event Log.
		articleParams.put("App", "Neon"); // Capture author info

		CACHE_VALIDITY 		= Integer.parseInt(getResources().getString(R.string.CacheFileValidity));

		downloadFile=new FileFromURL();

		imgLike=(ImageView)findViewById(R.id.imgLike);
		likeUrl=new LikeUrl();

		galBack=(ImageView)findViewById(R.id.galback);
		galForth=(ImageView)findViewById(R.id.galforth);
		relGalleryHeader=(RelativeLayout)findViewById(R.id.galleryHeader);
		relBottomBar=(RelativeLayout)findViewById(R.id.bottomBar);
		relInfoLayout=(RelativeLayout)findViewById(R.id.rel_Info);
		txtImageInfo=(TextView)findViewById(R.id.txtImageInfo);

		fadeinanim=AnimationUtils.loadAnimation(context, R.anim.fade_in);

		fadeoutanim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

		relInfoLayout.setBackgroundColor(Color.BLACK);
		relInfoLayout.getBackground().setAlpha(120);


		relGalleryHeader.getBackground().setAlpha(120);


		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		scwidth = metrics.widthPixels;

		SWIPE_MIN_DISTANCE=scwidth/8;


		carName=new ArrayList<String>();
		link=new ArrayList<String>();
		caption=new ArrayList<String>();
		description=new ArrayList<String>();
		vlinks=new ArrayList<String>();
		imgLinks=new ArrayList<String>();
		txtView=(TextView)findViewById(R.id.txtInfo);
		txtDesc=(TextView)findViewById(R.id.txtInfoDesc);


		bundle = getIntent().getExtras();
		if(bundle!=null){
			imgLinks = bundle.getStringArrayList("imgArr");
			imgDesc	= bundle.getStringArrayList("imgDesc");
			imgCaption = bundle.getStringArrayList("imgCaption");
			vlinksArr = bundle.getStringArrayList("vlinksArr");
			carName = bundle.getStringArrayList("nameArr");
			
			Log.d("CARNAME", "CARNAME" + carName);
			//Toast.makeText(context, ""+imgLinks.size(), 0).show();
		}
		try{

			if(callNet){
				if(netAvailable.isNetworkAvailable())
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}


					File xmlFile=new File(NEONXML);


					downloadFile=new FileFromURL();
					downloadFile.execute(xmlurl);

					/*Date lastModDate = new Date(xmlFile.lastModified());
							Date todayDate = new Date();
							long fileDate = lastModDate.getTime();
							long currentDate = todayDate.getTime();
							long dateDiff = currentDate - fileDate;
							long diffDays = dateDiff / (24 * 60 * 60 * 1000);*/
					/*if(diffDays > CACHE_VALIDITY){
								try
								{
									downloadFile=new FileFromURL();
									downloadFile.execute(xmlurl);
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
							}*/
					/*else
							{
								try
								{

									 // creating directory to store xml 


									filename=new FileInputStream(NEONXML);
									neongallery=SAXXMLParser.parse(filename);
									for(int i=0; i<neongallery.size();i++)
									{
										link.add(neongallery.get(i).getLINK());
										caption.add(neongallery.get(i).getCAPTION());
										description.add(neongallery.get(i).getDESCRIPTION());
										vlinks.add(neongallery.get(i).getVLINK());
										carName.add(neongallery.get(i).getCARRNAME());
									}

									mPager.setAdapter(new CarAdapter(getSupportFragmentManager(), context, link,vlinks) );

								}
								catch(NullPointerException npx)
								{
									npx.printStackTrace();
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}

							}*/


					/*else
				{
					try
					{
						downloadFile.execute(xmlurl);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}*/
				}
			}
			else 
			{
				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

				if(!myDirectory.exists()) {                                 
					myDirectory.mkdirs();
				}

				File xmlFile=new File(NEONXML);

				try
				{
					if(xmlFile.exists() && xmlFile.length()!=0)
					{
						filename=new FileInputStream(NEONXML);
						neongallery=SAXXMLParser.parse(filename);
						for(int i=0; i<neongallery.size();i++)
						{
							link.add(neongallery.get(i).getLINK());
							caption.add(neongallery.get(i).getCAPTION());
							description.add(neongallery.get(i).getDESCRIPTION());
							vlinks.add(neongallery.get(i).getVLINK());
							carName.add(neongallery.get(i).getCARRNAME());
						}

						//mPager.setAdapter(new CarAdapter(getSupportFragmentManager(), context, link,vlinks) );
						mPager.setAdapter(new CarAdapter(getSupportFragmentManager(), context, imgLinks,vlinksArr) );
					}
					else
					{
						Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
					}
				}catch(NullPointerException npx)
				{
					npx.printStackTrace();

				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			/*mPager.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if (gdt.onTouchEvent(event)) {
						return false;
					}

					return false;
				}
			});*/
			/*			mPager.setCurrentItem(0);*/
			/*if(mPager.getCurrentItem()==0)
			{

					txtDesc.setText(description.get(0));

			}*/
			/*description.get(position).replaceAll("\n", " \n");*/
			/*	String desc=description.get(0);
			txtDesc.setText(Html.fromHtml(desc));
			txtView.setText(caption.get(0));*/
			mPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int position) {
					// TODO Auto-generated method stub
					txtImageInfo.setText((position+1)+" of "+mPager.getAdapter().getCount());
					txtView.setText(imgCaption.get(position));


					//String desc=description.get(position);/*description.get(position).replaceAll("\n", " \n");*/
					String desc=imgDesc.get(position);/*description.get(position).replaceAll("\n", " \n");*/
					//String desc= "abc";
					txtDesc.setText(Html.fromHtml(desc));
					//txtDesc.setVisibility(View.INVISIBLE);

					/*Html.fromHtml(htmlStringWithMathSymbols)*/
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int state) {
					// TODO Auto-generated method stub
					/*
					 * Dragging
					 */
					if(state==1 || state==2) 
					{
						/*
						 * Fadding out while Dragging.
						 */
						/*if(relBottomBar.isShown() || relGalleryHeader.isShown() || relInfoLayout.isShown())
						{
							relBottomBar.startAnimation(fadeoutanim);
							relGalleryHeader.startAnimation(fadeoutanim);
							relInfoLayout.startAnimation(fadeoutanim);
							fadeoutanim.setFillAfter(true);

							relBottomBar.setVisibility(ViewGroup.INVISIBLE);
							relGalleryHeader.setVisibility(ViewGroup.INVISIBLE);
							relInfoLayout.setVisibility(ViewGroup.INVISIBLE);
						}*/
					}


				}
			});

			/*	mPager.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


			try {
			Field mScroller2;
			mScroller2 = ViewPager.class.getDeclaredField("mScroller");
			mScroller2.setAccessible(true); 
			//MainFixedSpeedScroller scroller2 = new MainFixedSpeedScroller(mainPager.getContext(), new AccelerateDecelerateInterpolator());
			FixedSpeedScroller scroller = new FixedSpeedScroller(mPager.getContext(),new DecelerateInterpolator());
			// scroller.setFixedDuration(5000);
			mScroller2.set(mPager, scroller);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}


		galBack.setOnClickListener(this);
		galForth.setOnClickListener(this);
		imgLike.setOnClickListener(this);

	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");
		//FlurryAgent.logEvent("Gallery_Opened_Event", articleParams, true);

		if(clarNotify){

			NotificationManager mNotificationManager;

			mNotificationManager = 	(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

			mNotificationManager.cancelAll();

			clarNotify = false;
		}
		SharedPreferences shrd1 = context.getSharedPreferences("NissanGalleryTab", context.MODE_WORLD_WRITEABLE);
		shrd1.edit().clear().commit();

		/*
		if(clarNotifyElse){

			NotificationManager mNotificationManager;

			mNotificationManager = 	(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

			mNotificationManager.cancelAll();

			clarNotifyElse = false;
		}*/

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);


	}




	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		// TODO Auto-generated method stub
		return super.onCreateView(name, context, attrs);

	}




	public  class CarAdapter extends FragmentStatePagerAdapter
	{

		Context context;
		ArrayList<String> url=null;
		ArrayList<String>caption;
		ArrayList<String>description;
		ArrayList<String>vlink;
		public CarAdapter(FragmentManager fm,Context context,ArrayList<String>url,ArrayList<String>vlink) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.url=url;

			this.vlink=vlink;
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			//Log.i("Caption ", caption.get(position).toString()+" Desc"+description.get(position).toString());

			/*txtImageInfo.setText(position+" of "+getCount());

			txtView.setText(caption.get(position).toString());
			String desc = null;

				txtDesc.setText(description.get(position).toString());*/
			/*	Log.i("Position in Fragment000000", "0000"+position);*/
			/*Toast.makeText(context, "Position"+position, Toast.LENGTH_SHORT).show();*/
			/*desc=description.get(position).toString();*/


			/*	for(int i=0;i<desc.length();i++)
			{
				if(desc.contentEquals("\n"))
				{
					desc.replace("\n", "\n");
				}
			}*/

			/*			Log.i("Desc", desc);*/

			return new ImageFragment(context, url.get(position),vlink.get(position));
			/*	return new ImageFragment(context, url.get(position));*/


		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return url.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}


		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}



	}	



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*	Intent intent=new Intent(getApplicationContext(), NissanTabActivity.class);
		startActivity(intent);
		this.finish();*/
		try
		{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					/*if(session.isOpened())
				{	*/
					session.closeAndClearTokenInformation();
					/*exception.printStackTrace();
					}*/
					Log.i("Clearing", "Token");
				}
			});
			//FlurryAgent.endTimedEvent("Gallery_Opened_Event");
		 }catch(NullPointerException npx)
		 {
			 npx.printStackTrace();
		 }
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		/*if(isBack)
		{
			isBack=false;
			Intent intent =new Intent(context,NissanTabActivity.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		else
		{
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}*/

		/*super.onBackPressed();*/
	}

	private class GestureListener extends SimpleOnGestureListener
	{


		int count=1;


		/*@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub
			if((e1.getX() - e2.getX()) >= SWIPE_MIN_DISTANCE ) {
				if(relBottomBar.isShown() || relGalleryHeader.isShown() || relInfoLayout.isShown())
				{
					relBottomBar.startAnimation(fadeoutanim);
					relGalleryHeader.startAnimation(fadeoutanim);
					relInfoLayout.startAnimation(fadeoutanim);
					fadeoutanim.setFillAfter(true);

					relBottomBar.setVisibility(ViewGroup.GONE);
					relGalleryHeader.setVisibility(ViewGroup.GONE);
					relInfoLayout.setVisibility(ViewGroup.GONE);
				}
			}
			else if ((e2.getX() - e1.getX()) > SWIPE_MIN_DISTANCE) {
				if(relBottomBar.isShown() || relGalleryHeader.isShown() || relInfoLayout.isShown())
				{
					relBottomBar.startAnimation(fadeoutanim);
					relGalleryHeader.startAnimation(fadeoutanim);
					relInfoLayout.startAnimation(fadeoutanim);
					fadeoutanim.setFillAfter(true);

					relBottomBar.setVisibility(ViewGroup.GONE);
					relGalleryHeader.setVisibility(ViewGroup.GONE);
					relInfoLayout.setVisibility(ViewGroup.GONE);
				}
			}

			return super.onFling(e1, e2, velocityX, velocityY);
		}*/

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			// TODO Auto-generated method stub
			/*	if(!relBottomBar.isShown() || !relGalleryHeader.isShown() || !relInfoLayout.isShown())
			{
				relBottomBar.startAnimation(fadeinanim);
				relGalleryHeader.startAnimation(fadeinanim);
				relInfoLayout.startAnimation(fadeinanim);
				fadeoutanim.setFillAfter(true);

				relBottomBar.setVisibility(ViewGroup.VISIBLE);
				relGalleryHeader.setVisibility(ViewGroup.VISIBLE);
				relInfoLayout.setVisibility(ViewGroup.VISIBLE);
			}
			if(relBottomBar.isShown() || relGalleryHeader.isShown() || relInfoLayout.isShown())
			{
				relBottomBar.startAnimation(fadeoutanim);
				relGalleryHeader.startAnimation(fadeoutanim);
				relInfoLayout.startAnimation(fadeoutanim);
				fadeoutanim.setFillAfter(true);

				relBottomBar.setVisibility(ViewGroup.INVISIBLE);
				relGalleryHeader.setVisibility(ViewGroup.INVISIBLE);
				relInfoLayout.setVisibility(ViewGroup.INVISIBLE);
			}*/
			/*
			if(count%2!=0)
			{
				NissanCarGallery.relBottomBar.startAnimation(fadeoutanim);
				NissanCarGallery.relGalleryHeader.startAnimation(fadeoutanim);
				NissanCarGallery.relInfoLayout.startAnimation(fadeoutanim);
				NissanCarGallery.fadeoutanim.setFillAfter(true);

					relBottomBar.setVisibility(ViewGroup.INVISIBLE);
				relGalleryHeader.setVisibility(ViewGroup.INVISIBLE);
				relInfoLayout.setVisibility(ViewGroup.INVISIBLE);
				count++;
			}
			else if(count%2==0)
			{
				NissanCarGallery.relBottomBar.startAnimation(fadeinanim);
				NissanCarGallery.relGalleryHeader.startAnimation(fadeinanim);
				NissanCarGallery.relInfoLayout.startAnimation(fadeinanim);
				NissanCarGallery.fadeoutanim.setFillAfter(true);

				relBottomBar.setVisibility(ViewGroup.VISIBLE);
				relGalleryHeader.setVisibility(ViewGroup.VISIBLE);
				relInfoLayout.setVisibility(ViewGroup.VISIBLE);
				count++;
			}
			 */

			return super.onSingleTapConfirmed(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 * Asynchronus download of XML files
	 */

	class FileFromURL extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);

				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				DocumentBuilder	db	= dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try
				{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);


				byte data[] = new byte[1024];

				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;

					// publishing the progress....
					// After this onProgressUpdate will be called
					/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

					// writing data to file
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				filename = new FileInputStream(NEONXML);
				neongallery = SAXXMLParser.parse(filename);

				for(int i=0; i<neongallery.size();i++)
				{
					link.add(neongallery.get(i).getLINK());
					caption.add(neongallery.get(i).getCAPTION());
					description.add(neongallery.get(i).getDESCRIPTION());
					vlinks.add(neongallery.get(i).getVLINK());
					carName.add(neongallery.get(i).getCARRNAME());
				}
				for(int i=0;i<description.size();i++)
				{
					Log.i("Description " , " "+description.get(i));
				}
				//mPager.setAdapter(new CarAdapter(getSupportFragmentManager(), context, link,vlinks) );
				mPager.setAdapter(new CarAdapter(getSupportFragmentManager(), context, imgLinks,vlinksArr) );

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException npx)
			{
				npx.printStackTrace();

			}
			catch(Exception ex)
			{

			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);
		}



	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId())
		{
		case R.id.fbshare:

			// Single menu item is selected do something
			// Ex: launching new activity/screen or show alert message
			/*  Toast.makeText(AndroidMenusActivity.this, "Bookmark is Selected", Toast.LENGTH_SHORT).show();*/
			int currentIMage=mPager.getCurrentItem();


			Map<String, String> Gallery_FB_Share_Event = new HashMap<String, String>();
			Gallery_FB_Share_Event.put("App","Neon");
			Gallery_FB_Share_Event.put("CarName", ""+carName.get(currentIMage));
			FlurryAgent.logEvent("Gallery_Share_Event", Gallery_FB_Share_Event);	



			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Nissan Neon");
			/*"http://nissansunnyindia.com/neon/gallery.php#neongallery/"+image_name[0]+"/"
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, link.get(currentIMage));*/
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "http://nissansunnyindia.com/neon/gallery.php#neongallery/"+carName.get(currentIMage)+"/");
			startActivity(Intent.createChooser(sharingIntent, "Share via"));

			return true;


		default:
			return super.onOptionsItemSelected(item);
		}
		/*return super.onOptionsItemSelected(item);*/
	}
	 
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==galBack.getId())
		{
			int currentIMage=mPager.getCurrentItem();
			if(mPager.getAdapter()!=null)
			{
				if(currentIMage!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(currentIMage-1, true);
					txtImageInfo.setText(""+(mPager.getCurrentItem()+1)+" of "+mPager.getAdapter().getCount());
				}
			}
		}
		if(v.getId()==galForth.getId())
		{
			int currentIMage=mPager.getCurrentItem();
			if(mPager.getAdapter()!=null)
			{
				if(currentIMage!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(currentIMage+1, true);
					txtImageInfo.setText(""+(mPager.getCurrentItem()+1)+" of "+mPager.getAdapter().getCount());
				}
			}

		}
		if(v.getId()==imgLike.getId())
		{

			try
			{

				/*
				 * Logging in with Facebook
				 */

				try
				{

					/*					Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();*/

					login_facebook();
					FlurryAgent.logEvent("FB_Like_Event");

				}catch(NullPointerException npx)
				{
					npx.printStackTrace();
				}
				catch(Exception ex)
				{

				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


		}
	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	public void login_facebook()
	{
		final int currentIMage = mPager.getCurrentItem();
		
		Log.d("CAR", "CAR" + carName.get(currentIMage));
		/*boolean uiEnable=true;
		if((Session.getActiveSession()==null) || (Session.getActiveSession().isOpened()))
		{
			uiEnable=false;
		}
		else
		{
			uiEnable=true;
		}*/
		try
		{
			Session.openActiveSession(acontext, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						//Toast.makeText(context, "open", 0).show();
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(acontext, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									fbUserid=user.getId();
									fbAccessToken=session.getAccessToken();
									Log.i("User Name ", "Name : "+user.getName());
									Log.i("User Id ", "Id : "+user.getId());
									Log.i("User Access Token ", "Access Token : "+session.getAccessToken());
									new LikeUrl().execute(carName.get(currentIMage));
								}
							}
						});
						/*session.getAccessToken();
					Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();*/
					}
					
					if(session.isClosed()){
						
						//Toast.makeText(context, "Closed", 0).show();
					}
					
				}
			});}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	 }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	class LikeUrl extends AsyncTask<String, String, String> {

		BufferedReader bufferedReader = null;
		@Override
		protected String doInBackground(String... image_name) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("https://graph.facebook.com/"+fbUserid+"/og.likes");
			/* httppost.addHeader(name, value)*/

			Log.i("Graph : ", "https://graph.facebook.com/"+fbUserid+"/og.likes");


			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("access_token", fbAccessToken));
				nameValuePairs.add(new BasicNameValuePair("object", "http://nissansunnyindia.com/neon/gallery.php#neongallery/"+image_name[0]+"/"));
				Log.i("Url : ", "http://nissansunnyindia.com/neon/gallery.php#neongallery/"+image_name[0]+"/");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", " Like "+stringBuffer.toString());
				status=stringBuffer.toString();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			/*dismissDialog(progress_bar_type);*/
			removeDialog(progress_bar_type);
			String msg=getLikeStatus(status);
			Log.i("Msg", "msg"+msg);
			String status_msg=showMsg(status);

			if(status_msg.equalsIgnoreCase("notliked"))
			{

			}
			else
			{
				Toast toast = Toast.makeText(context, status_msg, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			if(msg.equalsIgnoreCase("liked"))
			{

			}
			else
			{
				/*Toast.makeText(context, msg, Toast.LENGTH_SHORT).setGravity(Gravity.CENTER_HORIZONTAL,0,0);*/
				Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

			}
		}


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			removeDialog(progress_bar_type);
		}


	}

	public String getLikeStatus(String status)
	{
		if(status.contains("\"code\":3501"))
		{
			int start_Offset=status.indexOf("\"code\":3501");


			String msg =status.substring(start_Offset, status.length());

			return "You already liked this image.";
		}
		else
		{
			return "liked";
		}
	}
	public String showMsg(String status)
	{
		if(status.startsWith("{\"id\":\""))
		{
			/*Like {"id":"474883652561059"}*/

			return "You have successfully liked this image on your facebook.";
		}
		else
		{
			return "notliked";
		}
	}
}
