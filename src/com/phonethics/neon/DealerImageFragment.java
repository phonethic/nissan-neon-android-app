package com.phonethics.neon;

import java.io.File;

import com.phonethics.imageloader.ImageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

/*import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;*/

public class DealerImageFragment extends Fragment {
	
	Context context;
	String url;
	/*DisplayImageOptions options;
	ImageLoaderConfiguration config;*/
	ImageLoader imageLoader;
	File cacheDir;
	ImageView imgView;
	ProgressBar prog;
	public DealerImageFragment()
	{

	}
	public DealerImageFragment(Context context,String url)
	{
		this.context=context;
		this.url=url;
	}
	@Override
	public View getView() {
		// TODO Auto-generated method stub
		return super.getView();
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		imageLoader =ImageLoader.getInstance();
		imageLoader = new ImageLoader(context);
		/*if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"dealerImg");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();*/
		
	
		
		/*config= new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))


		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))

		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
*/


		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	
		View view=inflater.inflate(R.layout.dealer_pager_imageview, container,false);
		imgView=(ImageView)view.findViewById(R.id.img_dealer_pager);
		prog=(ProgressBar)view.findViewById(R.id.prog);
		/*imageLoader.displayImage(url, imgView, new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				prog.setVisibility(View.GONE);
			}
		});*/
		imageLoader.DisplayImage(url, imgView);
		
		return view;
	}
	

}
