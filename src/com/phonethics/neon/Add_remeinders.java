package com.phonethics.neon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;
import com.phonethics.database.DataBaseUtil;
import com.phonethics.wheel.ArrayWheelAdapter;
import com.phonethics.wheel.NumericWheelAdapter;
import com.phonethics.wheel.OnWheelChangedListener;
import com.phonethics.wheel.OnWheelScrollListener;
import com.phonethics.wheel.WheelView;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;

public class Add_remeinders extends SherlockActivity {

	TextView 				date;
	TextView 				time;
	DatePicker				datePicker;
	TimePicker				timePicker;
	Calendar 				c,mainCalendar;
	DateFormat 				dateFormat;
	String 					formattedDate, message = "",isIntenetCall = "false";
	String 					isAm,preNotifyValue, preNotifyUnits,textAmpm;
	ImageView 				img7, img12, img9, img5, savebutton,now,week1,month1,month6,header;
	AlarmManager 			alarm;
	Context 				context;
	EditText 				noteText, dateText,timeText;
	DataBaseUtil 	db;
	InputMethodManager 		imm ;
	LinearLayout			datelayout,timelayout;
	RelativeLayout			dataLayout;

	int 					mYear , mMonth, mDay, hour,min,PRESENT_YEAR,textHr,textMin;
	int 					lengthOfMessage;
	boolean					isDatePicker = true, activityLaunched = true,isScrolled, KEY_BOARD_VISIBLE = true;
	int						ampmC;
	TextView 				txtHeader;

	ImageView settingsImg;

	WheelView				daywheel, monthwheel, yearwheel, hourwheel,minwheel,ampmwheel;
	String months[] = new String[] {"January", "February", "March", "April", "May","June", "July", "August", "September", "October", "November", "December"};
	String timeOfDay[] = new String[] {"AM", "PM"};
	/*	static int				HELLO_ID;*/

	Map<String, String> articleParams = new HashMap<String, String>();
	
	
	ActionBar ac;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_remeinders);

		context 				= this;
		
		ac = getSupportActionBar();
		ac.setTitle("Add New Reminder");
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();
		
		

		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);
		
		//Flurry Event Log.
				articleParams.put("App", "Neon"); // Capture author info
		txtHeader				=(TextView)findViewById(R.id.textview_headertext);
		date					= (TextView) findViewById(R.id.edittext_date);
		time					= (TextView) findViewById(R.id.edittext_time);

		datePicker				= (DatePicker) findViewById(R.id.datePicker);
		timePicker				= (TimePicker) findViewById(R.id.timepicker);
		timePicker.setIs24HourView(false);


		daywheel				= (WheelView) findViewById(R.id.dateWheel);
		monthwheel				= (WheelView) findViewById(R.id.monthWheel);
		yearwheel				= (WheelView) findViewById(R.id.yearWheel);

		hourwheel				= (WheelView) findViewById(R.id.hourWheel);
		minwheel				= (WheelView) findViewById(R.id.minWheel);
		ampmwheel				= (WheelView) findViewById(R.id.ampmWheel);

		datelayout				= (LinearLayout) findViewById(R.id.dateLayout);
		timelayout				= (LinearLayout) findViewById(R.id.timeLayout);

		img7					= (ImageView) findViewById(R.id.button_time_7);
		img12					= (ImageView) findViewById(R.id.button_time_12);
		img9					= (ImageView) findViewById(R.id.button_time_9);
		img5					= (ImageView) findViewById(R.id.button_time_5);
		savebutton 				= (ImageView) findViewById(R.id.image_savebutton_remeinders);
		now						= (ImageView) findViewById(R.id.imageview_time_now);
		week1					= (ImageView) findViewById(R.id.imageview_1week);
		month1					= (ImageView) findViewById(R.id.imageview_1month);
		month6					= (ImageView) findViewById(R.id.imageview_6month);
		settingsImg				= (ImageView) findViewById(R.id.img_settings);
		header					= (ImageView) findViewById(R.id.header);
		
		header.setVisibility(View.GONE);
		savebutton.setVisibility(View.GONE);
		
		
		noteText				= (EditText) findViewById(R.id.edittext_note);
		dateText				= (EditText) findViewById(R.id.edittext_date);
		timeText				= (EditText) findViewById(R.id.edittext_time);
		
		/*dataLayout				= (RelativeLayout) findViewById(R.id.layout_buttons);*/

		db						= new DataBaseUtil(getApplicationContext());

		imm 					= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		alarm 					= (AlarmManager)getSystemService(Context.ALARM_SERVICE);

		c						= Calendar.getInstance();
		mainCalendar            = Calendar.getInstance();

		mYear 					= c.get(Calendar.YEAR);
		PRESENT_YEAR			= c.get(Calendar.YEAR);
		mMonth 					= c.get(Calendar.MONTH);
		mDay 					= c.get(Calendar.DAY_OF_MONTH);
		hour 					= c.get(Calendar.HOUR_OF_DAY);
		min 					= c.get(Calendar.MINUTE);
		ampmC					= c.get(Calendar.AM_PM);
		if(hour > 12){
			if(min<10){
				timeText.setText((hour -12)  + " : 0" + min + "  " + timeOfDay[ampmC]);
			}else{
				timeText.setText((hour -12)  + " : " + min + "  " + timeOfDay[ampmC]);
			}
			/*timeText.setText((hour - 12) + " : " + min + "  " + timeOfDay[ampmC]);*/
		}else{
			if(min<10){
				timeText.setText(hour  + " : 0" + min + "  " + timeOfDay[ampmC]);
			}else{
				timeText.setText(hour  + " : " + min + "  " + timeOfDay[ampmC]);
			}
		}

		/*dataLayout.setVisibility(View.GONE);*/
		noteText.setCursorVisible(true);



		Date dt 				= c.getTime();
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate 			= format.format(dt);

		timePicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
		timePicker.setCurrentMinute(c.get(Calendar.MINUTE));

		Bundle extras				= getIntent().getExtras();
		isIntenetCall				= extras.getString("IntentCall");
		if(extras != null){
			if(isIntenetCall.equals("ReminderSetting")){
				preNotifyValue			= extras.getString("numValue");
				preNotifyUnits			= extras.getString("numUnits");
				if(preNotifyValue.equals("")){
					preNotifyValue = "0";
					preNotifyUnits="No Pre Notify";
				}
				setDate(mYear,mMonth,mDay,c );
				setTime(hour, min);
			}
			else if(isIntenetCall.equals("Update_Reminder")){

				message				= extras.getString("Message");
				String date			= extras.getString("Date");
				String time			= extras.getString("Time");
				preNotifyValue = extras.getString("Value");
				preNotifyUnits = extras.getString("Unit");

				txtHeader.setText("EDIT REMINDER");
				noteText.setText(message);
				/*dateText.setText(date);
				timeText.setText(time);*/
				preNotifyValue = "0";
				preNotifyUnits="No Pre Notification";
				setDate(mYear,mMonth,mDay,c );
				setTime(hour, min);

			}else if(isIntenetCall.equals("New_Reminder")){

				preNotifyValue = "0";
				preNotifyUnits = "No Pre Notification";
				setDate(mYear,mMonth,mDay,c );
				setTime(hour, min);
			}

		}

		/*isIntenetCall			= extras.getString("IntentCall");

		if(isIntenetCall.equals("Update_Reminder")){

			message				= getIntent().getStringExtra("Message");
			String date			= getIntent().getStringExtra("Date");
			String time			= getIntent().getStringExtra("Time");

			noteText.setText(message);
			dateText.setText(date);
			timeText.setText(time);

		}else if(isIntenetCall.equals("New_Reminder")){

			setDate(mYear,mMonth,mDay,c );
			setTime(hour, min);
		}*/

		OnWheelChangedListener listener = new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updateDays(yearwheel, monthwheel, daywheel);
			}
		};

		OnWheelScrollListener scrollListener = new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				/*Log.d("++++++++++++++++++", "========== scroll fineshed =========");*/

				int 	textYear  		= 	PRESENT_YEAR + yearwheel.getCurrentItem();
				String 	textMonth	 	= 	months[monthwheel.getCurrentItem()];
				int 	textDay 		= 	daywheel.getCurrentItem()+1;

				/*int 	textHr 			= 	hourwheel.getCurrentItem()+1;
				int 	textMin 		= 	minwheel.getCurrentItem();
				String 	textAmpm	 	= 	timeOfDay[ampmwheel.getCurrentItem()];*/

				dateText.setText(textDay + " - " + textMonth + " - " + textYear);
				/*	timeText.setText(textHr + " : " + textMin + "  " + textAmpm);*/

				Log.d("++++++++++++++++++", "========== scroll fineshed ========= " + textDay);
				setDate(textYear, monthwheel.getCurrentItem(), textDay, c);
				/*c.get(Calendar.Mo)*/
				/*setTime(textHr, textMin);*/

			}
		};

		OnWheelScrollListener timeScroller = new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub

				textHr 			= 	hourwheel.getCurrentItem()+1;
				textMin 		= 	minwheel.getCurrentItem();
				textAmpm	 	= 	timeOfDay[ampmwheel.getCurrentItem()];

				/*timeText.setText(textHr + " : " + textMin + "  " + textAmpm);*/
				activityLaunched = false;
				isScrolled = true;
				setTime(textHr, textMin);


			}
		};

		monthwheel.setViewAdapter(new DateArrayAdapter(this, months, mMonth));
		monthwheel.setCurrentItem(mMonth);
		monthwheel.addChangingListener(listener);
		monthwheel.addScrollingListener(scrollListener);

		/*monthwheel.setOnDragListener(l)*/




		yearwheel.setViewAdapter(new DateNumericAdapter(this, mYear, mYear + 10, 0));
		yearwheel.setCurrentItem(mYear);
		yearwheel.addChangingListener(listener);
		yearwheel.addScrollingListener(scrollListener);


		updateDays(yearwheel, monthwheel, daywheel);
		daywheel.setCurrentItem(mDay - 1);
		daywheel.addScrollingListener(scrollListener);


		NumericWheelAdapter hourAdapter = new NumericWheelAdapter(this, 1, 12,"%02d");
		hourAdapter.setItemResource(R.layout.wheel_text_itm);
		hourAdapter.setItemTextResource(R.id.text);
		hourwheel.addScrollingListener(timeScroller);
		hourwheel.setViewAdapter(hourAdapter);
		if(c.get(Calendar.HOUR)==0){
			hourwheel.setCurrentItem(11);
		}else{
			hourwheel.setCurrentItem(c.get(Calendar.HOUR)-1);
		}
		/*hourwheel.setCurrentItem(c.get(Calendar.HOUR));*/


		NumericWheelAdapter minAdapter = new NumericWheelAdapter(this, 0, 59, "%02d");
		minAdapter.setItemResource(R.layout.wheel_text_itm);
		minAdapter.setItemTextResource(R.id.text);
		minwheel.setViewAdapter(minAdapter);
		minwheel.setCyclic(true);
		minwheel.setCurrentItem(c.get(Calendar.MINUTE));
		minwheel.addScrollingListener(timeScroller);



		ArrayWheelAdapter<String> ampmAdapter = new ArrayWheelAdapter<String>(this, timeOfDay);
		ampmAdapter.setItemResource(R.layout.wheel_text_itm);
		ampmAdapter.setItemTextResource(R.id.text);
		ampmwheel.setViewAdapter(ampmAdapter);
		/*if(hour>=12){
			ampmwheel.setCurrentItem(1);
		}else{
			ampmwheel.setCurrentItem(0);
		}*/
		ampmwheel.setCurrentItem(ampmC);
		ampmwheel.addScrollingListener(timeScroller);


		onClicks();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		KEY_BOARD_VISIBLE = false;
		/*dataLayout.setVisibility(View.VISIBLE);*/
		noteText.setCursorVisible(false);
		noteText.clearFocus();
		imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
		return super.onTouchEvent(event);
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_reminder_setting, menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_reminder_setting, menu);
		return false;
	}*/

	/*@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK){
			dataLayout.setVisibility(View.VISIBLE);
			imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
			noteText.setCursorVisible(false);
			noteText.clearFocus();
			KEY_BOARD_VISIBLE = false;
		}
		return super.dispatchKeyEvent(event);
	}*/

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch(item.getItemId()){
		case 1:R.id.reminderss_settings :
			startActivityForResult(new Intent(context, ReminderSettingActivity.class),1);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			
			return false;
		default:
			return super.onOptionsItemSelected(item);
		}
	}*/
	
	
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if((keyCode==event.KEYCODE_BACK))
		{
			Log.i("--------", "Key code back");
			dataLayout.setVisibility(View.VISIBLE);
			KEY_BOARD_VISIBLE = false;
			noteText.setCursorVisible(false);
			noteText.clearFocus();
			imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
		}

		return super.onKeyDown(keyCode, event);
	}*/

	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);

	}

	
	
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		MenuItem menu_save = menu.add("Save");
		menu_save.setIcon(R.drawable.save_button);
		menu_save.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Save")){
			savebutton.performClick();
		}
		
		if(item.getTitle().toString().equalsIgnoreCase("Add New Reminder")){
			onBackPressed();
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == 1){
			preNotifyValue			= data.getStringExtra("numValue");
			preNotifyUnits			= data.getStringExtra("numUnits");
			if(preNotifyValue.equals("")){
				preNotifyValue = "0";
				preNotifyUnits="No Pre Notify";
			}
			setDate(mYear,mMonth,mDay,c );
			setTime(hour, min);
		}
	}

	public void onClicks(){
		noteText.requestFocus();
		noteText.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(noteText, 0);
				KEY_BOARD_VISIBLE =true;
			}
		}, 200);

		noteText.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if ( actionId == EditorInfo.IME_ACTION_DONE){
				/*	dataLayout.setVisibility(View.VISIBLE);*/
					KEY_BOARD_VISIBLE = false;
					noteText.setCursorVisible(false);
					noteText.clearFocus();
					imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
				}
				return false;
			}
		});
		timeText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*datePicker.setVisibility(View.GONE);
				timePicker.setVisibility(View.VISIBLE);*/
			/*	dataLayout.setVisibility(View.VISIBLE);*/

				datelayout.setVisibility(View.GONE);
				timelayout.setVisibility(View.VISIBLE);
				KEY_BOARD_VISIBLE = false;
				isDatePicker = false;
				/*noteText.setFocusable(false);*/
				noteText.setCursorVisible(false);
				noteText.clearFocus();
				imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
			}
		});

		dateText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*datePicker.setVisibility(View.VISIBLE);
				timePicker.setVisibility(View.GONE);*/

			/*	dataLayout.setVisibility(View.VISIBLE);*/

				datelayout.setVisibility(View.VISIBLE);
				timelayout.setVisibility(View.GONE);
				KEY_BOARD_VISIBLE = false;
				isDatePicker = true;
				/*noteText.setFocusable(false);*/
				noteText.setCursorVisible(false);
				noteText.clearFocus();
				imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
			}
		});

		settingsImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(context, ReminderSettingActivity.class),1);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		timePicker.setOnTimeChangedListener(new OnTimeChangedListener() {

			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				setTime(hourOfDay, minute);
			}
		});



		img7.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*timePicker.setVisibility(View.VISIBLE);
				datePicker.setVisibility(View.GONE);

				timeText.setText("   Time :  " +  "7 : 00  AM");
				timePicker.setCurrentHour(7);
				timePicker.setCurrentMinute(00);
				isDatePicker = false;*/

				datelayout.setVisibility(View.GONE);
				timelayout.setVisibility(View.VISIBLE);
				timeText.setText("7 : 00  AM");
				hourwheel.setCurrentItem(6,true);
				minwheel.setCurrentItem(0,true);
				ampmwheel.setCurrentItem(0,true);
				isDatePicker = false;



			}
		});
		img12.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*timePicker.setVisibility(View.VISIBLE);
				datePicker.setVisibility(View.GONE);
				timeText.setText("   Time :  " +  "12 : 00  PM");
				timePicker.setCurrentHour(12);
				timePicker.setCurrentMinute(00);
				isDatePicker = false;*/


				datelayout.setVisibility(View.GONE);
				timelayout.setVisibility(View.VISIBLE);
				timeText.setText("12 : 00  PM");
				hourwheel.setCurrentItem(11,true);
				minwheel.setCurrentItem(0,true);
				ampmwheel.setCurrentItem(1,true);
				isDatePicker = false;

			}
		});
		img5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*timePicker.setVisibility(View.VISIBLE);
				datePicker.setVisibility(View.GONE);
				timeText.setText("   Time :  " +  "5 : 00  PM");
				timePicker.setCurrentHour(17);
				timePicker.setCurrentMinute(00);

				isDatePicker = false;*/


				datelayout.setVisibility(View.GONE);
				timelayout.setVisibility(View.VISIBLE);
				timeText.setText("5 : 00  PM");
				hourwheel.setCurrentItem(4,true);
				minwheel.setCurrentItem(0,true);
				ampmwheel.setCurrentItem(1,true);
				isDatePicker = false;
			}
		});
		img9.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*timePicker.setVisibility(View.VISIBLE);
				datePicker.setVisibility(View.GONE);
				time.setText("   Time :  " +  "9 : 00  PM");
				timePicker.setCurrentHour(21);
				timePicker.setCurrentMinute(00);
				isDatePicker = false;*/


				datelayout.setVisibility(View.GONE);
				timelayout.setVisibility(View.VISIBLE);
				timeText.setText("9 : 00  PM");
				hourwheel.setCurrentItem(8,true);
				minwheel.setCurrentItem(0,true);
				ampmwheel.setCurrentItem(1,true);
				isDatePicker = false;


			}
		});
		now.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*		datePicker.setVisibility(View.VISIBLE);
				timePicker.setVisibility(View.GONE);
				 */

				datelayout.setVisibility(View.VISIBLE);
				timelayout.setVisibility(View.GONE);

				Calendar cal = Calendar.getInstance();
				int mYear 		= cal.get(Calendar.YEAR);
				int mMonth 		= cal.get(Calendar.MONTH);
				int mDay 		= cal.get(Calendar.DAY_OF_MONTH);
				int mHour		= cal.get(Calendar.HOUR_OF_DAY);
				int	mMin		= cal.get(Calendar.MINUTE);
				int ampm		= cal.get(Calendar.AM_PM);
				int hHour		= cal.get(Calendar.HOUR);

				Log.d("Calender", "" + mYear + " " + mMonth + " "  + mDay +" ");

				yearwheel.setCurrentItem(0,true);
				monthwheel.setCurrentItem(mMonth,true);
				daywheel.setCurrentItem(mDay-1,true);
				Log.d("---------", "Hour value = " + hHour);
				if(hHour==0){
					hourwheel.setCurrentItem(11,true);
				}else{
					hourwheel.setCurrentItem(hHour-1,true);
				}
				minwheel.setCurrentItem(mMin,true);
				ampmwheel.setCurrentItem(ampm,true);

				datePicker.init(mYear, mMonth, mDay, new CommonDateListener());
				Date dt = cal.getTime();
				SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
				formattedDate = format.format(dt);
				dateText.setText(formattedDate);
				setTime(mHour, mMin);

				timePicker.setCurrentHour(mHour);
				timePicker.setCurrentMinute(mMin);

				if(isDatePicker == true){
					/*datePicker.setVisibility(View.VISIBLE);
					timePicker.setVisibility(View.GONE);*/

					datelayout.setVisibility(View.VISIBLE);
					timelayout.setVisibility(View.GONE);

				}else{
					/*datePicker.setVisibility(View.GONE);
					timePicker.setVisibility(View.VISIBLE);*/

					datelayout.setVisibility(View.GONE);
					timelayout.setVisibility(View.VISIBLE);
				}
			}
		});
		week1.setOnClickListener(new OnClickListener() {
			/**
			 * Setting the Date to the next Week
			 */
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*datePicker.setVisibility(View.VISIBLE);
				timePicker.setVisibility(View.GONE);*/

				datelayout.setVisibility(View.VISIBLE);
				timelayout.setVisibility(View.GONE);

				isDatePicker = true;
				Calendar c = Calendar.getInstance();
				int mYear 		= c.get(Calendar.YEAR);
				int mMonth 		= c.get(Calendar.MONTH);
				int mDay 		= c.get(Calendar.DAY_OF_MONTH);

				c.add(Calendar.DAY_OF_YEAR , 6);
				mYear 		= c.get(Calendar.YEAR);
				mMonth 		= c.get(Calendar.MONTH);
				mDay 		= c.get(Calendar.DAY_OF_MONTH);
				setDate(mYear,mMonth,mDay,c );


			}
		});
		month1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*datePicker.setVisibility(View.VISIBLE);
				timePicker.setVisibility(View.GONE);*/

				datelayout.setVisibility(View.VISIBLE);
				timelayout.setVisibility(View.GONE);

				isDatePicker = true;

				Calendar c = Calendar.getInstance();
				int mYear 		= c.get(Calendar.YEAR);
				int mMonth 		= c.get(Calendar.MONTH);
				int mDay 		= c.get(Calendar.DAY_OF_MONTH);

				c.add(Calendar.MONTH, 1);
				mYear 		= c.get(Calendar.YEAR);
				mMonth 		= c.get(Calendar.MONTH);
				mDay 		= c.get(Calendar.DAY_OF_MONTH);
				setDate(mYear,mMonth,mDay,c );

			}
		});
		month6.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*datePicker.setVisibility(View.VISIBLE);
				timePicker.setVisibility(View.GONE);*/

				datelayout.setVisibility(View.VISIBLE);
				timelayout.setVisibility(View.GONE);


				isDatePicker = true;
				Calendar c = Calendar.getInstance();
				int mYear 		= c.get(Calendar.YEAR);
				int mMonth 		= c.get(Calendar.MONTH);
				int mDay 		= c.get(Calendar.DAY_OF_MONTH);

				c.add(Calendar.MONTH, 6);
				mYear 		= c.get(Calendar.YEAR);
				mMonth 		= c.get(Calendar.MONTH);
				mDay 		= c.get(Calendar.DAY_OF_MONTH);

				setDate(mYear,mMonth,mDay,c );

			}
		});

		noteText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				noteText.setCursorVisible(true);
				noteText.setFocusable(true);
				imm.showSoftInput(v, 0);

			}
		});

		savebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				FlurryAgent.logEvent("Reminder_Added_Event");
				Calendar tempCal = Calendar.getInstance();
				if(tempCal.compareTo(c) == 1){
					Toast.makeText(context, "Time Has Already Passed. Set Proper Time.", Toast.LENGTH_SHORT).show();
				}else{
					addNewRemeinder();
					Intent intent = new Intent(context, Remeinders.class);
					startActivity(intent);
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					finish();
				}
			}
		});
	}

	public class CommonDateListener implements OnDateChangedListener
	{
		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub

			mYear 	= year;
			mMonth 	= monthOfYear;
			mDay 	= dayOfMonth;
			c.set(mYear, mMonth, mDay);

			Date dt = c.getTime();
			SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
			formattedDate = format.format(dt);

			dateText.setText(formattedDate);

		}
	}

	public void setDate(int sYear, int sMonth, int sDay, Calendar c){
		mYear 	= sYear;
		mMonth	= sMonth;
		mDay 	= sDay;



		datePicker.init(mYear, mMonth, mDay, new CommonDateListener());
		Date dt = c.getTime();

		yearwheel.setCurrentItem((sYear - PRESENT_YEAR),true);
		monthwheel.setCurrentItem(mMonth,true);
		daywheel.setCurrentItem(mDay -1,true );


		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = format.format(dt);
		dateText.setText(formattedDate);
	}

	public void setTime(int sHour, int sMin){

		/*if(sHour < 12){
			hour 	= sHour + 12;
		}else{
			hour 	= sHour;
		}*/
		if(isScrolled && sHour < 12 && (ampmwheel.getCurrentItem()==1) ){
			hour 	= sHour + 12;
		}else if(isScrolled && sHour == 12 && ampmwheel.getCurrentItem()==0 ){
			hour 	= 0;
		}else{
			hour = sHour;
		}

		/*hour 	= sHour;*/
		min		= sMin;
		Log.d("^^^^^^^^^^^", "HOUR VALUE  " + hour);
		c.set(mYear, mMonth, mDay, hour, sMin);


		/*if(hour > 12){
			sHour = sHour - 12;
			timeText.setText(sHour + " : " + sMin + "  PM");
		}else if(sHour == 12){
			timeText.setText(sHour + " : " + sMin + "  PM");
		}else{
			timeText.setText(sHour + " : " + sMin + "  AM");
		}*/

		/*	textHr 			= 	hourwheel.getCurrentItem()+1;
		textMin 		= 	minwheel.getCurrentItem();
		textAmpm	 	= 	timeOfDay[ampmwheel.getCurrentItem()];*/
		if(!activityLaunched){
			if(sHour > 12){
				if(sMin<10){
					timeText.setText((sHour -12)  + " : 0" + sMin + "  " + textAmpm);
				}else{
					timeText.setText((sHour -12)  + " : " + sMin + "  " + textAmpm);
				}
			}else{
				if(sMin<10){
					timeText.setText(sHour  + " : 0" + sMin + "  " + textAmpm);
				}else{
					timeText.setText(sHour  + " : " + sMin + "  " + textAmpm);
				}

			}
		}
	}

	public void addNewRemeinder(){

		/*String message 	= noteText.getText().toString().substring(lengthOfMessage, noteText.getText().length());*/
		if(isIntenetCall.equals("Update_Reminder")){
			message 		= noteText.getText().toString();
			String date		= formattedDate;
			String time		= hour + ":" + min;
			if(message.equals("")){
				message = "Reminder";
			}

			try{
				db.open();
				db.addRemeinder(message, date, time,preNotifyValue,preNotifyUnits);
				db.close();
			}catch(SQLException sqlex){
				Log.d("ADD SQLERROR",sqlex.toString());
			}catch(Exception ex){
				Log.d("ADD Exception",ex.toString());
			}

		}else if(isIntenetCall.equals("New_Reminder")){

			message 		= noteText.getText().toString();
			String date		= formattedDate;
			String time		= hour + ":" + min;
			if(message.equals("")){
				message = "Reminder";
			}

			try{
				db.open();
				db.addRemeinder(message, date, time, preNotifyValue, preNotifyUnits);
				db.close();
			}catch(SQLException sqlex){
				Log.d("ADD SQLERROR",sqlex.toString());
			}catch(Exception ex){
				Log.d("ADD Exception",ex.toString());
			}
		}else if(isIntenetCall.equals("ReminderSetting")){
			message 		= noteText.getText().toString();
			String date		= formattedDate;
			String time		= hour + ":" + min;
			if(message.equals("")){
				message = "Reminder";
			}

			try{
				db.open();
				db.addRemeinder(message, date, time,preNotifyValue,preNotifyUnits);
				db.close();
			}catch(SQLException sqlex){
				Log.d("ADD SQLERROR",sqlex.toString());
			}catch(Exception ex){
				Log.d("ADD Exception",ex.toString());
			}
		}
	}

	@Override
	public void onBackPressed() { 
		// TODO Auto-generated method stub
		
		/*super.onBackPressed();*/
	/*	Log.i("###########", "Back Mthod");
		if(KEY_BOARD_VISIBLE){
			Log.i("###########", "Back Mthod if condition");
			dataLayout.setVisibility(View.VISIBLE);
			imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(noteText.getWindowToken(), 0);
			noteText.setCursorVisible(false);
			noteText.clearFocus();
			KEY_BOARD_VISIBLE = false;
		}else{
			Log.i("###########", "Back Mthod ELSE condition");
			Intent intent = new Intent(context, Remeinders.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();
		}*/
		Intent intent = new Intent(context, Remeinders.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}



	/**
	 * Adapter for string based wheel. Highlights the current value.
	 */
	private class DateArrayAdapter extends ArrayWheelAdapter<String> {
		// Index of current item
		int currentItem;
		// Index of item to be highlighted
		int currentValue;

		/**
		 * Constructor
		 */
		public DateArrayAdapter(Context context, String[] items, int current) {
			super(context, items);
			this.currentValue = current;
			setTextSize(16);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			if (currentItem == currentValue) {
				view.setTextColor(0xFF0000F0);
			}
			view.setTypeface(Typeface.SANS_SERIF);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}

	/**
	 * Updates day wheel. Sets max days according to selected month and year
	 */
	void updateDays(WheelView year, WheelView month, WheelView day) {
		mainCalendar = Calendar.getInstance();
		mainCalendar.set(Calendar.YEAR, mainCalendar.get(Calendar.YEAR) + year.getCurrentItem());
		mainCalendar.set(Calendar.MONTH, month.getCurrentItem());

		int maxDays = mainCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		day.setViewAdapter(new DateNumericAdapter(this, 1, maxDays, mainCalendar.get(Calendar.DAY_OF_MONTH) - 1));
		int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
		day.setCurrentItem(curDay - 1, true);
		int today = mainCalendar.get(Calendar.DAY_OF_MONTH);
		Log.d("=============", "===== Listner Called ===== " + today);
	}


	/**
	 * Adapter for numeric wheels. Highlights the current value.
	 */
	private class DateNumericAdapter extends NumericWheelAdapter {
		// Index of current item
		int currentItem;
		// Index of item to be highlighted
		int currentValue;

		/**
		 * Constructor
		 */
		public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
			super(context, minValue, maxValue);
			this.currentValue = current;
			setTextSize(16);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			if (currentItem == currentValue) {
				view.setTextColor(0xFF0000F0);
			}
			view.setTypeface(Typeface.SANS_SERIF);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}


}
