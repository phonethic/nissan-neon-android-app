package com.phonethics.neon;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;
//import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MailLink extends SherlockFragmentActivity implements LocationListener {


	Context con;
	String provider;
	GoogleMap googleMap;
	double latitude,longitude;
	LatLng latLng;
	EditText number;
	Dialog dialog;
	String uri,mobno;
	Button send;
	private String address;

	public static final int progress_bar_type = 1;
	public ProgressDialog pDialog;

	Map<String, String> articleParams = new HashMap<String, String>();

	ActionBar ac;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mail_link);

		con = this;
		//Flurry Event Log.
		articleParams.put("App", "Neon"); // Capture author info

		/*		showDialog(progress_bar_type);*/

		dialog = new Dialog(con, android.R.style.Theme_Translucent_NoTitleBar);



		//final Dialog dialog = new Dialog(LAtestTab.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);*/
		dialog.setContentView(R.layout.phone_no_popup);



		send = (Button) dialog.findViewById(R.id.send_phn_no);
		number = (EditText) dialog.findViewById(R.id.phone_num);

		ac = getSupportActionBar();
		ac.setTitle("Email Breakdown Location");
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);



		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

		// Getting GoogleMap object from the fragment
		googleMap = fm.getMap();

		// Enabling MyLocation Layer of Google Map
		googleMap.setMyLocationEnabled(true);
		googleMap.setTrafficEnabled(true);

		// Getting LocationManager object from System Service LOCATION_SERVICE
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// Creating a criteria object to retrieve provider





		if (netInfo != null && netInfo.isConnected()) {

			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(provider);

			if(location!=null){
				onLocationChanged(location);
				/*				pDialog.dismiss();*/
				dialog.show();
				/*Toast.makeText(con, "lat" + " " + latitude + "\nlon" + " " + longitude, Toast.LENGTH_LONG).show();*/
			}
			else {

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(con);
				alertDialogBuilder3.setTitle("Location Check");
				alertDialogBuilder3
				.setMessage("No location found. Please check GPS settings")
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						MailLink.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		}else{

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);
			alertDialogBuilder.setTitle("Check Connection");
			alertDialogBuilder
			.setMessage("No internet connection do you want to exit ?")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					MailLink.this.finish();
				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}


		address = "";
		Geocoder geoCoder = new Geocoder(
				getBaseContext(), Locale.getDefault());
		try {
			List<Address> addresses = geoCoder.getFromLocation(
					latitude, longitude, 1);

			if (addresses.size() > 0) {
				for (int index = 0; 
						index < addresses.get(0).getMaxAddressLineIndex(); index++)
					address += addresses.get(0).getAddressLine(index) + " ";
			}
		}catch (IOException e) {        
			e.printStackTrace();
		}   


		uri = "http://maps.google.com/maps?saddr=" + latitude +","+ longitude ;

		send.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{

				FlurryAgent.logEvent("Breakdown_Location_Event");
				try
				{

					Intent email=new Intent(Intent.ACTION_SEND);

					email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
					email.putExtra("android.intent.extra.SUBJECT", "Break Down" + " " + number.getText().toString());
					email.putExtra("android.intent.extra.TEXT",address+"\n "+uri);
					email.setType("text/plain");
					startActivity(Intent.createChooser(email, "Send mail..."));
				}

				catch(ActivityNotFoundException aex)
				{

				}catch(Exception ex)
				{

				}
				dialog.cancel();
			}
		});

	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "4233FF2X3RJVPXW26C95");

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//FlurryAgent.onEndSession(this);


	}



	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		latitude = location.getLatitude();

		// Getting longitude of the current location
		longitude = location.getLongitude();



		// Creating a LatLng object for the current location
		latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	/*@Override
	protected Dialog onCreateDialog(int id)
	{
	switch (id)
	{
	case progress_bar_type:
	pDialog = new ProgressDialog(this);
	pDialog.setMessage("Please wait...");
	pDialog.setIndeterminate(false);
	pDialog.setMax(100);
	pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	pDialog.setCanceledOnTouchOutside(false);
	pDialog.setCancelable(true);
	pDialog.show();


	return pDialog;
	default:
	return null;
	}
	}*/


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Email Breakdown Location"))
		{

			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}


		return true;
	}
}


