package com.phonethics.neon;

import java.lang.reflect.Field;
import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.scroller.FixedSpeedScroller;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.DecelerateInterpolator;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class NissanGallery extends SherlockFragmentActivity implements OnClickListener {

	ImageView projectImageView;
	Bitmap bm;

	private int number;

	private ArrayList<String> videos;
	private ArrayList<String> arr;
	private int position;
	private TextView textview;
	String TITLE="";


	/*ImageView imgNissanTab;
	ImageView imgToolsTab;
	ImageView imgTipsTab;
	ImageView imgMycarTab;
	ImageView imgContactTab;*/

	ViewPager mPager;
	Context context;

	ActionBar ac;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_nissan_gallery);

		context=this;

		ac = getSupportActionBar();



		position = getIntent().getIntExtra("positionvalue", 0);
		/*		gallery = (Gallery)findViewById(R.id.gallery);*/
		arr =	new ArrayList<String>();
		videos=	new ArrayList<String>();
		textview = (TextView) findViewById(R.id.textview_headertext);


		mPager=(ViewPager)findViewById(R.id.viewPagerTips);

		arr = getIntent().getStringArrayListExtra("imagesLink");
		videos= getIntent().getStringArrayListExtra("videoLinks");
		mPager.setAdapter(new TipsGallery(getSupportFragmentManager(), context, arr,videos));
		mPager.setOffscreenPageLimit(4);
		textview.setText(getIntent().getStringExtra("section"));
		TITLE = getIntent().getStringExtra("section");

		ac.setTitle(TITLE);
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();



		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);


		try {
			Field mScroller2;
			mScroller2 = ViewPager.class.getDeclaredField("mScroller");
			mScroller2.setAccessible(true); 
			/*MainFixedSpeedScroller scroller2 = new MainFixedSpeedScroller(mainPager.getContext(), new AccelerateDecelerateInterpolator());*/
			FixedSpeedScroller scroller = new FixedSpeedScroller(mPager.getContext(),new DecelerateInterpolator());
			// scroller.setFixedDuration(5000);
			mScroller2.set(mPager, scroller);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}


	}

	public void setint(int position) {
		// TODO Auto-generated method stub
		number = position;
		Log.d("NUmber","Number" + number );
	}




	public void recycleBitmap()
	{
		if(bm!=null)
		{
			bm.recycle();

			bm=null;
		}
	}



	public  class TipsGallery extends FragmentPagerAdapter
	{

		Context context;
		ArrayList<String> url=null;
		ArrayList<String>vlink=null;
		public TipsGallery(FragmentManager fm,Context context,ArrayList<String>url,ArrayList<String>vlink) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.url=url;
			this.vlink=vlink;

		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub

			return new TipsFragment(context, url.get(position),vlink.get(position));

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return url.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}



	}	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(getApplicationContext(), Tips.class);
		startActivity(intent);
		this.finish();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase(TITLE)){
			onBackPressed();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;

	}




	@Override
	public void onClick(View v) {

	}

}
