package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.phonethics.customclass.CarBrands;





import android.util.Log;

public class CarModelParsing extends DefaultHandler{
	
	private CarBrands tempBrand;
	private ArrayList<CarBrands> brandsAndModels;
	private String tempData="";
	
	
	
	
	public ArrayList<CarBrands> getParsedData(){
		return brandsAndModels;
	}
	
	
	public void startDocument() throws SAXException{
		Log.d("Document", "Inside XML Document");
		this.brandsAndModels = new ArrayList<CarBrands>();
		tempBrand = new CarBrands();

	}
	
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		
		
		if(qName.equalsIgnoreCase("section")){
			tempBrand = new CarBrands();
			String brandName = attributes.getValue("name");
			tempBrand.setBrandName(brandName);
			Log.d("!!!!!", "---" + brandName );
		}else if(qName.equalsIgnoreCase("car")){
			tempBrand.setModelName(attributes.getValue("model"));
			Log.d("!!!!!", "---" + attributes.getValue("name") );
		}
	
		
	}
	
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
		/*tempBrand.setModelName(new String(ch, start, length));*/
		tempData = new String(ch, start, length);
	}
	
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if(qName.equalsIgnoreCase("section")){
			brandsAndModels.add(tempBrand);
		}
	}
	
	public void endDocument() throws SAXException {
		// Nothing to do
	}

	

	

	
	
	

}
