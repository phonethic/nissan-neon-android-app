package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.phonethics.customclass.TipsGalleryData;

public class TipsGalleryDataParser extends DefaultHandler{

	TipsGalleryData tempNeonLinks;
	String links,caption,id,vlinks, thumb;
	ArrayList<TipsGalleryData>tipsLinks;
	boolean isInDesc=false;
	boolean isInChar=false;
	public ArrayList<TipsGalleryData> getParsedData()
	{

		return tipsLinks;

	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		this.tipsLinks=new ArrayList<TipsGalleryData>();
		tempNeonLinks=new TipsGalleryData();
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);

		if(qName.equalsIgnoreCase("section"))
		{
			tempNeonLinks=new TipsGalleryData();
			caption=attributes.getValue("name");

			thumb = attributes.getValue("ilink");
			
			Log.d("CHKURL","CHKURL" +  thumb);

			/*			description=attributes.getValue("description");*/
		/*	vlinks=attributes.getValue("vlink");*/
			/*Log.i("Links : ", " "+links);
			Log.i("Captions : ", " "+caption);*/

			/*if(description.equalsIgnoreCase(""))
			{
				description=" ";
			}
			if(vlinks.equalsIgnoreCase(""))
			{
				vlinks=" ";
			}*/

				
			/*tempNeonLinks.setLINK(links);
			tempNeonLinks.setCAPTION(caption);
				tempNeonLinks.setDESCRIPTION(description);
			tempNeonLinks.setVLINK(vlinks);*/
			
			tempNeonLinks.setSectionName(caption);
			tempNeonLinks.setThumb(thumb);

		}
		else if(qName.equalsIgnoreCase("photo"))
		{
			isInDesc=true;
			isInChar=false;
			id=attributes.getValue("id");
			links=attributes.getValue("ilink");
			vlinks=attributes.getValue("vlink");
		/*	if(vlinks.contains(""))
			{
				vlinks="null";
			}
					*/
			tempNeonLinks.setId(id);
			tempNeonLinks.setIlink(links);
			tempNeonLinks.setVlink(vlinks);

		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
	
		/*tempNeonLinks.setDESCRIPTION(new String(ch,start,length));*/
		/*super.characters(ch, start, length);*/

	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if (qName.equalsIgnoreCase("section")) {
			// add it to the list
/*			Log.d("------ ---- " ,"-----------  " + "  ----------");
			Log.d("ParserLink ---- " ," "+ tempNeonLinks.getLINK());
			Log.d("ParserCaption -- "," "+tempNeonLinks.getCAPTION());
			Log.d("ParserDescription -- "," "+tempNeonLinks.getDESCRIPTION());
			Log.d("VLink -- "," "+tempNeonLinks.getVLINK());*/
			tipsLinks.add(tempNeonLinks);

		}
	/*	else if(qName.equalsIgnoreCase("description"))
		{
			neonLinks.add(tempNeonLinks);
			isInDesc=false;
			if(isInChar==false)
			{
				tempNeonLinks.setDESCRIPTION(" ");
			}

		}*/
	}




}
