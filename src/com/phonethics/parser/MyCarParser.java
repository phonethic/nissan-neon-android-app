package com.phonethics.parser;

import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.phonethics.customclass.CarBrands;

import android.util.Log;


public class MyCarParser {
	public static ArrayList<CarBrands> parse(InputStream is){
		ArrayList<CarBrands> brandsAndCars = null;
		
		 try {
				
			 
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
	                    .getXMLReader();
	            // create a ExampleHandler
	            CarModelParsing saxHandler = new CarModelParsing();
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	            /*projects = saxHandler.getParsedData();*/
	            
	            brandsAndCars = saxHandler.getParsedData();
	            
	 
	        } catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	        }
		
		return brandsAndCars;
	}
	
}
