package com.phonethics.parser;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.phonethics.customclass.NeonGalleryLinks;

import android.net.ParseException;
import android.util.Log;

public class SAXXMLParser {
	 public static ArrayList<NeonGalleryLinks> parse(InputStream is) {
		 ArrayList<NeonGalleryLinks> neonLinks = null;
		 try {
			
			 
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();

	            NeonGalleryLinkParser saxHandler = new NeonGalleryLinkParser();
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	            /*projects = saxHandler.getParsedData();*/
	            
	            neonLinks=saxHandler.getParsedData();
	            
	 
	        }
		 catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	            ex.printStackTrace();
	        }
		 return neonLinks;
	 }


}