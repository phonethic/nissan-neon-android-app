package com.phonethics.parser;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.phonethics.customclass.TipsGalleryData;


import android.util.Log;

public class TipsSAXXMLParser {

	 public static ArrayList<TipsGalleryData> parse(InputStream is) {
		 ArrayList<TipsGalleryData> tipsLinks = null;
		 try {
			
			 
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
	                    .getXMLReader();

	            TipsGalleryDataParser saxHandler = new TipsGalleryDataParser();
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	          
	            tipsLinks=saxHandler.getParsedData();
	            
	 
	        }
		 catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	            ex.printStackTrace();
	        }
		 return tipsLinks;
	 }
}
