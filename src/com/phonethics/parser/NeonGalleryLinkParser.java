package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.phonethics.customclass.NeonGalleryLinks;

import android.text.GetChars;
import android.util.Log;

public class NeonGalleryLinkParser extends DefaultHandler{

	NeonGalleryLinks 								tempNeonLinks;
	String 											links,caption,description,vlinks,carnames,group,thumbnail,order;
	ArrayList<NeonGalleryLinks>						neonLinks;
	boolean isInDesc=false;
	boolean isInChar=false;
	
	
	public ArrayList<NeonGalleryLinks> getParsedData()
	{

		return neonLinks;

	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		this.neonLinks=new ArrayList<NeonGalleryLinks>();
		tempNeonLinks=new NeonGalleryLinks();
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);

		if(qName.equalsIgnoreCase("photo"))
		{
			tempNeonLinks=new NeonGalleryLinks();
			links=attributes.getValue("ilink");
			caption=attributes.getValue("caption");
			/*			description=attributes.getValue("description");*/
			vlinks=attributes.getValue("vlink");
			carnames=attributes.getValue("name");
			group = attributes.getValue("group");
			thumbnail = attributes.getValue("thumbnail");
			order   = attributes.getValue("order");
			
			/*Log.i("Links : ", " "+links);
			Log.i("Captions : ", " "+caption);*/

			/*if(description.equalsIgnoreCase(""))
			{
				description=" ";
			}
			if(vlinks.equalsIgnoreCase(""))
			{
				vlinks=" ";
			}*/


			tempNeonLinks.setLINK(links);
			tempNeonLinks.setCAPTION(caption);
			/*	tempNeonLinks.setDESCRIPTION(description);*/
			tempNeonLinks.setVLINK(vlinks);
			tempNeonLinks.setCARRNAME(carnames);
			tempNeonLinks.setGROUP(group);
			tempNeonLinks.setThumbnail(thumbnail);
			tempNeonLinks.setOrder(order);

		}
		else if(qName.equalsIgnoreCase("description"))
		{
			isInDesc=true;
			isInChar=false;

		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		if(isInDesc)
		{
			isInChar=true;
			tempNeonLinks.setDESCRIPTION(new String(ch,start,length));

		}

		/*tempNeonLinks.setDESCRIPTION(new String(ch,start,length));*/
		/*super.characters(ch, start, length);*/

	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if (qName.equalsIgnoreCase("photo")) {
			// add it to the list
/*			Log.d("------ ---- " ,"-----------  " + "  ----------");
			Log.d("ParserLink ---- " ," "+ tempNeonLinks.getLINK());
			Log.d("ParserCaption -- "," "+tempNeonLinks.getCAPTION());
			Log.d("ParserDescription -- "," "+tempNeonLinks.getDESCRIPTION());
			Log.d("VLink -- "," "+tempNeonLinks.getVLINK());*/
			neonLinks.add(tempNeonLinks);

		}
		else if(qName.equalsIgnoreCase("description"))
		{
			/*neonLinks.add(tempNeonLinks);*/
			isInDesc=false;
			if(isInChar==false)
			{
				tempNeonLinks.setDESCRIPTION(" ");
			}

		}
	}




}
