package com.phonethics.services;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

public class DownloadService extends IntentService{

	ResultReceiver download;
	String 	xml,NEONXML		="/sdcard/neoncache/micra.xml";
	public DownloadService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public DownloadService()
	{
		super("DownloadService");
	}
	
	public void setFileName(String fileName){
		NEONXML = fileName;
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		download = intent.getParcelableExtra("downloadFile");
		String URL	=	intent.getStringExtra("URL");
		NEONXML		=	intent.getStringExtra("FILENAME");
		
		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;
		
		Log.d("SERVICESTART", "SERVICESTART");
		
		int count;

		try {
			httpClient 	=	new DefaultHttpClient();
			httpPost 	= 	new HttpPost(URL);
			httpRes		=	httpClient.execute(httpPost);
			httpEnt		=	httpRes.getEntity();
			xml 		=	EntityUtils.toString(httpEnt);

			DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
			DocumentBuilder			db	= dbf.newDocumentBuilder();
			InputSource				is	= new InputSource();
			is.setCharacterStream(new StringReader(xml));

			URL url = new URL(URL);
			URLConnection connection = url.openConnection();
			connection.connect();

			// getting file length
			int lenghtOfFile = connection.getContentLength();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			// Output stream to write file

			try{
				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
				if(!myDirectory.exists()) {                                 
					myDirectory.mkdirs();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			FileOutputStream output = new FileOutputStream(NEONXML);
			byte data[] = new byte[1024];
			long total = 0;
			while (   ((count = input.read(data)) != -1)  ) {
				total += count;
				output.write(data, 0, count);
			}
			output.flush();
			output.close();
			input.close();
			
			Bundle bundle = new Bundle();
			bundle.putBoolean("downloadCompleteFromService", true);
			download.send(0, bundle);
			
		}catch(SocketException socketException){
			Log.d("***", "Socket Exception");

		}catch(ConnectionClosedException con){

		}catch(ConnectTimeoutException timeOut){
			Log.d("***", "connection time out");

		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e){
			Log.e("Error: ", " "+e.getMessage());
		}
		
	}

	

}
