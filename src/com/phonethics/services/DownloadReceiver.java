package com.phonethics.services;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DownloadReceiver extends ResultReceiver{

	DownloadResult result;
	public DownloadReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(result!=null)
		{
			result.onReceiveFileResult(resultCode, resultData);
		}
	}
	public interface DownloadResult 
	{
		public void onReceiveFileResult(int resultCode, Bundle resultData);
	}
	public void setReceiver(DownloadResult rec)
	{
		result=rec;
	}

}
