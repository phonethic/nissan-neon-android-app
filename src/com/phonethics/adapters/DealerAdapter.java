package com.phonethics.adapters;

import java.util.ArrayList;

//import com.flurry.android.FlurryAgent;
import com.phonethics.neon.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DealerAdapter extends ArrayAdapter<String>  {

	ArrayList<String> extraData;
	Activity context;
	private static LayoutInflater inflator = null;

	public DealerAdapter(Activity context, ArrayList<String> extraData){
		super(context,R.layout.dealer_details_layout, extraData);
		this.context = context;
		this.extraData = extraData;

	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View rowView = convertView;

		if(convertView == null){
			inflator = context.getLayoutInflater();

			rowView = inflator.inflate(R.layout.dealer_details_layout, null);

			ViewHolder holder = new ViewHolder();

			holder.text = (TextView) rowView.findViewById(R.id.Textview_number);
			holder.baclImg = (ImageView) rowView.findViewById(R.id.img_back);
			holder.relative = (RelativeLayout) rowView.findViewById(R.id.contact_layout);
			rowView.setTag(holder);
		}
		ViewHolder hold = (ViewHolder) rowView.getTag();
		if(position==0 || position==1 || position==2 || position==3){
			//hold.baclImg.setBackgroundResource(R.drawable.calltoggle);
			hold.baclImg.setImageResource(R.drawable.cont);
			
		}else if(position==4){
			
			//hold.baclImg.setBackgroundResource(R.drawable.faxtoggle);//fax
			hold.baclImg.setImageResource(R.drawable.fax);//fax
			
		}else if(position==5||position==6){
			//hold.baclImg.setBackgroundResource(R.drawable.emailtoggle);
			
			hold.baclImg.setImageResource(R.drawable.mail);
		}

		/*String data = extraData.get(position).toString().replace(" ", "");
		String data = extraData.get(position);
		if(data.toString().contains("/")){
			Toast.makeText(context, "Yes it contains / char", Toast.LENGTH_SHORT).show();
			data = extraData.get(position).toString().split("/")[0];
		}
		data = extraData.get(position).toString().replace(" ", "");
		data = extraData.get(position).toString().split("/")[0];*/

		if(!extraData.get(position).equalsIgnoreCase("") && !extraData.get(position).equalsIgnoreCase("0") && extraData.get(position).toString().length()>7){

			hold.baclImg.setVisibility(View.VISIBLE);
			hold.text.setVisibility(View.VISIBLE);
			/*String data = extraData.get(position).toString().replace(" ", "");
			String data1 = data.toString().replace("-", "");
			String data2 = data1.toString().split("/")[0];*/
			String finalStr = changeFormat(extraData.get(position).toString(),true);
			finalStr=isFax(finalStr);
			hold.text.setText(finalStr);

		}else{
			hold.baclImg.setVisibility(View.GONE);
			hold.text.setVisibility(View.GONE);
		}

		/*hold.baclImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(extraData.get(position).toString().contains("@")){
					Toast.makeText(context, "Yes it contains @", Toast.LENGTH_SHORT).show();
					
					Intent email = new Intent(Intent.ACTION_SEND);
					email.setType("text/plain");
					email.putExtra("android.intent.extra.EMAIL",extraData.get(position));
					context.startActivity(Intent.createChooser(email, "Send mail.."));
				}else if(!extraData.get(position).toString().contains("@") && !extraData.get(position).toString().contains("www") ){
					Intent call = new Intent(android.content.Intent.ACTION_DIAL);
					call.setData(Uri.parse("tel:" + changeFormat(extraData.get(position).toString(),false)));
					context.startActivity(call);
				}
			}
		});*/

		return rowView;
	}

	static class ViewHolder{

		public TextView text;
		public ImageView baclImg;
		public RelativeLayout relative;

	}

	public String changeFormat(String data, boolean breakChar){
		String finalStr="";
		if(!breakChar){
			String dataa = data.toString().replace(" ", "");
			String data1 = dataa.toString().replace("-", "");
			String data2 = data1.toString().split("/")[0];
			finalStr = data2.toString().split(",")[0];
			return finalStr;
		}else{
			finalStr = data.toString().replace(" ", "");
			return finalStr;
		}
	}
	public String isFax(String data)
	{
		if(data.contains("~"))
		{
		return data.substring(1);
		}
		else
		{
			return data;
		}
	}

}
