package com.phonethics.adapters;

import com.phonethics.neon.R;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {
	private Activity mContext;
	LayoutInflater inflator = null;
	private int picheight;
	private int picwidth;
	// Keep all Images in array
	public Integer[] mThumbIds = {
			R.drawable.pic_1, R.drawable.pic_2,
			R.drawable.pic_3, R.drawable.pic_4,
			R.drawable.pic_5, R.drawable.pic_6,
			R.drawable.pic_7, R.drawable.pic_8,
			R.drawable.pic_9, R.drawable.pic_10,
			R.drawable.pic_11, R.drawable.pic_12,
			R.drawable.pic_13, R.drawable.pic_14,
			R.drawable.pic_15,R.drawable.pic_16,
			R.drawable.pic_17,R.drawable.pic_18,
			R.drawable.pic_19,R.drawable.pic_20,
			R.drawable.pic_21,R.drawable.pic_22,
			R.drawable.pic_23,R.drawable.pic_24,
			R.drawable.pic_25,R.drawable.pic_26,
			R.drawable.pic_27,R.drawable.pic_28,
			R.drawable.pic_29,R.drawable.pic_30,
			R.drawable.pic_31,R.drawable.pic_32,
			R.drawable.pic_33,R.drawable.pic_34,
			R.drawable.pic_35,R.drawable.pic_36,
			R.drawable.pic_37,R.drawable.pic_38,
			R.drawable.pic_39,R.drawable.pic_40,
			R.drawable.pic_41,R.drawable.pic_42,
			R.drawable.pic_43,R.drawable.pic_44,
			R.drawable.pic_45,R.drawable.pic_46,
			R.drawable.pic_47,R.drawable.pic_48,
			R.drawable.pic_49,R.drawable.pic_50,
			R.drawable.pic_51,R.drawable.pic_52,
			R.drawable.pic_53,
	};

	public String[] str_ary = {
			"Straight No Entry", "No Parking",
			"No Stopping or Standing","Horns Prohibited",
			"School Ahead", "Narrow Road Ahead",
			"Narrow Bridge", "Right Hand Curve",
			"Left Hand Curve", "Compulsory Sound Horn",
			"One Way Signs" , "One Way Signs",
			"Vehicles Prohibited In Both Directions", "All Motor Vehicles Prohibited",
			"Tonga Prohibited", "Truck Prohibited",
			"Bullock Cart And Hand Cart Prohibited", "Hand Cart Prohibited",
			"Bullock Cart Prohibited", "Cycle Prohibited",
			"Pedestrian Prohibited", "Right Turn Prohibited",
			"Left Turn Prohibited", "U-turn Prohibited",
			"Speed Limit", "Width Limit",
			"Height Limit", "Length Limit",
			"Load Limit", "Axe Load Limit",
			"Bus Stop", "Restriction End Sign",
			"Compulsory Keep Left", "Compulsory Turn Left",
			"Compulsory Turn Right Ahead", "Compulsory Ahead or Turn Left",
			"Compulsory Ahead or Turn Right", "Compulsory Ahead Only",
			"Compulsory Cycle Track" , "Move On",
			"Stop", "Give way",
			"Bump ahead", "Ferry",
			"Right HairPin Bend", "Left HairPin Bend",
			"Left Reverse Bend", "Right Reverse Bend",
			"Steep Ascent" , "Steep Descent",
			"Road Widens Ahead", "Slippery Ahead", "Cycle Crossing"
	};


	// Constructor
	public ImageAdapter(Activity c){
		mContext = c;
		BitmapDrawable bdn=(BitmapDrawable) mContext.getResources().getDrawable(R.drawable.pic_1);
		picheight = bdn.getIntrinsicHeight();
		picwidth=bdn.getIntrinsicWidth();
	}
	public ImageAdapter()
	{

	}
	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];

	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		

		/*ImageView imageView = new ImageView(mContext);
		//	TextView txt = new TextView(mContext);
		imageView.setImageResource(mThumbIds[position]);
		imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
		imageView.setLayoutParams(new GridView.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));*/

		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			inflator = mContext.getLayoutInflater();
			convertView = inflator.inflate(R.layout.trafficgridimage, null);
			holder.imageView=(ImageView)convertView.findViewById(R.id.imgTraffic);
			convertView.setTag(holder);

		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		hold.imageView.setImageBitmap(decodeSampledBitmapFromResource(mContext.getResources(), mThumbIds[position], picwidth, picheight));
		//    txt.setLayoutParams(new GridView.LayoutParams(500, 500));
		return convertView;
	}

	static class ViewHolder
	{
		public ImageView imageView;
	}
	
	public  int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}
		}

		/* if (height > reqHeight || width > reqWidth) {
			 inSampleSize = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
               (double) Math.max(height, width)) / Math.log(0.5)));
        }*/

		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}
	public  Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		/*// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inInputShareable=true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = true;
		return BitmapFactory.decodeResource(res, resId, options);	*/

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}


}
