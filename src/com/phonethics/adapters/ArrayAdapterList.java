package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.neon.R;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ArrayAdapterList extends ArrayAdapter<String> {

	private Activity context;
	private static LayoutInflater inflator = null;
	private  ArrayList<String> data;
	private  ArrayList<String> values;
	boolean singleConstructor= false;
	private int bitmapHeight;
	private int bitmapWidth;

	public ArrayAdapterList(Activity context, ArrayList<String> data, ArrayList<String> values){
		super(context,R.layout.text_mycar_row, data);
		this.context = context;
		this.data = data;
		this.values = values;
		singleConstructor = false;
	}

	public ArrayAdapterList(Activity context, ArrayList<String> data){
		super(context,R.layout.text_mycar_row, data);
		this.context = context;
		this.data = data;
		this.values = new ArrayList<String>();
		singleConstructor= true;
		for(int i=0; i < data.size(); i++){
			values.add("");
		}
		

		BitmapDrawable bd=(BitmapDrawable)context.getResources().getDrawable(R.drawable.cont);
		bitmapHeight = bd.getIntrinsicHeight();
		bitmapWidth=bd.getIntrinsicWidth();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View rowView = convertView;

		if(convertView == null){
			inflator = context.getLayoutInflater();
			if(singleConstructor){
				rowView = inflator.inflate(R.layout.contact_layout, null);
			}else{
				rowView = inflator.inflate(R.layout.text_mycar_row, null);
			}
			ViewHolder holder = new ViewHolder();

			holder.text = (TextView) rowView.findViewById(R.id.Textview_mycar);
			holder.car_text = (TextView) rowView.findViewById(R.id.Textview_carDetails);
			holder.relative = (RelativeLayout) rowView.findViewById(R.id.contact_layout);
			//holder.contactImages=(ImageView)rowView.findViewById(R.id.contactImages);
			rowView.setTag(holder);
		}

		ViewHolder hold = (ViewHolder) rowView.getTag();
		hold.text.setText(data.get(position));
		
		hold.car_text.setText(values.get(position));
		if(singleConstructor){
		
			if(position==0){
				hold.relative.setBackgroundResource(R.drawable.cont);
					/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.calltoggle, bitmapWidth, bitmapHeight));*/
				/*hold.contactImages.setImageResource( R.drawable.calltoggle);*/
			}else if(position==1){
				hold.relative.setBackgroundResource(R.drawable.cont);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.calltoggle, bitmapWidth, bitmapHeight));*/
				/*hold.contactImages.setImageResource( R.drawable.calltoggle);*/
			}else if(position==2){
				hold.relative.setBackgroundResource(R.drawable.mail);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.emailtoggle, bitmapWidth, bitmapHeight));*/
			/*	hold.contactImages.setImageResource( R.drawable.emailtoggle);*/
			}else if(position==3){
				hold.relative.setBackgroundResource(R.drawable.cont);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.calltoggle, bitmapWidth, bitmapHeight));*/
		/*		hold.contactImages.setImageResource( R.drawable.calltoggle);*/
			}else if(position==4){
				hold.relative.setBackgroundResource(R.drawable.location_new);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.locationtoggle, bitmapWidth, bitmapHeight));*/
				/*hold.contactImages.setImageResource( R.drawable.locationtoggle);*/
			}else if(position==5){
				hold.relative.setBackgroundResource(R.drawable.fb);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.facebooktoggle, bitmapWidth, bitmapHeight));*/
				/*hold.contactImages.setImageResource( R.drawable.facebooktoggle);*/
			}else if(position==6){
				hold.relative.setBackgroundResource(R.drawable.location_new);
				/*hold.contactImages.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), R.drawable.locationtoggle, bitmapWidth, bitmapHeight));*/
				/*hold.contactImages.setImageResource( R.drawable.locationtoggle);*/
			}
		}


			return rowView;
		}

		static class ViewHolder{

			public TextView text;
			public TextView car_text;
			public RelativeLayout relative;
			public ImageView contactImages;

		}
		
		
		
		public  int calculateInSampleSize(
				BitmapFactory.Options options, int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize =1;

			if (height > reqHeight || width > reqWidth) {
				if (width > height) {
					inSampleSize = Math.round((float)height / (float)reqHeight);
				} else {
					inSampleSize = Math.round((float)width / (float)reqWidth);
				}
			}

			/* if (height > reqHeight || width > reqWidth) {
				 inSampleSize = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
	               (double) Math.max(height, width)) / Math.log(0.5)));
	        }*/

			Log.i("", "Sample Size"+inSampleSize);
			return inSampleSize;
		}
		public  Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
				int reqWidth, int reqHeight) {

			/*// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			options.inPurgeable=true;
			options.inDither=false;
			options.inInputShareable=true;
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			options.inScaled = false;
			BitmapFactory.decodeResource(res, resId, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = true;
			return BitmapFactory.decodeResource(res, resId, options);	*/

			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			options.inPurgeable=true;
			options.inDither=false;
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			options.inScaled = false;
			BitmapFactory.decodeResource(res, resId, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeResource(res, resId, options);
		}
	}
