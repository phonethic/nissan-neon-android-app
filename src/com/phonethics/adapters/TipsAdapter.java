package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.imageloader.ImageLoader;
import com.phonethics.neon.R;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TipsAdapter extends ArrayAdapter<String>{
	
	private Activity context;
	private ArrayList<String> data;
	private static LayoutInflater inflater=null;
	private boolean checkAll=false;
	int tot = 0;
	ArrayList<String> imagesArr;
	
	ImageLoader thumbLoad;
	/*	ViewHolder viewHolder;*/
	public TipsAdapter(Activity context,ArrayList<String>data,ArrayList<String> imagesArr)
	{
		super(context,R.layout.tipsrow,data);
		this.context=context;
		this.data=data;
		this.imagesArr = imagesArr;
		thumbLoad = new ImageLoader(context);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View rowView=convertView;
		if(convertView==null)
		{
			inflater=context.getLayoutInflater();
			rowView=inflater.inflate(R.layout.tipsrow, null);
			ViewHolder viewHolder=new ViewHolder();
			viewHolder.txtTips=(TextView)rowView.findViewById(R.id.textview_tips);
			viewHolder.img = (ImageView)rowView.findViewById(R.id.imageView1);
			rowView.setTag(viewHolder);
		}
		ViewHolder holder=(ViewHolder)rowView.getTag();
		holder.txtTips.setText(data.get(position));
		thumbLoad.DisplayImage(imagesArr.get(position), holder.img);
//		holder.img.setBackgroundResource(imagesArr.get(position));
		return rowView;
	}
	

	static class ViewHolder
	{
		public TextView txtTips;
		public ImageView	img;

	}
	

}
