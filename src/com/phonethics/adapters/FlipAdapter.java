package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.imageloader.ImageLoader;
import com.phonethics.neon.NissanCarGallery;
import com.phonethics.neon.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;



public class FlipAdapter extends BaseAdapter {

	private Activity 				actcontext;
	private Context					context;
	private static LayoutInflater 	inflator = null;
	private ArrayList<String> 		imgString;
	ImageLoader						imgLoader;
	int 							pos=0;
	ArrayList<String> 				imgDesc, imgCaption, vlinkArr, allNames;

	public FlipAdapter(Activity actcontext,Context context, ArrayList<String> imgString,ArrayList<String> imgDesc,ArrayList<String> imgCaption,ArrayList<String> vlinkArr, ArrayList<String> allNames){
		this.context = context;
		this.actcontext = actcontext;
		this.imgString = imgString;
		
		this.imgDesc = imgDesc;
		this.imgCaption = imgCaption;
		this.vlinkArr = vlinkArr;
		this.allNames = allNames;
		
		imgLoader =  new ImageLoader(this.context);

	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imgString.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return imgString.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		pos = position;

		if(convertView == null){
			inflator = actcontext.getLayoutInflater();
			convertView = inflator.inflate(R.layout.imageview_flip, null);
			ViewHolder holder = new ViewHolder();
			holder.img_flip = (ImageView) convertView.findViewById(R.id.imgview_flip);
			convertView.setTag(holder);
		}


		ViewHolder hold = (ViewHolder) convertView.getTag();

		/*if(pos>3){
			pos = 0;
			//imgLoader.DisplayImage(getItem(pos).toString(), hold.img_flip);
		}*/
		imgLoader.DisplayImage(getItem(pos).toString(), hold.img_flip);
		hold.img_flip.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(context, "click", 0).show();
				Intent intent = new Intent(context, NissanCarGallery.class);
				intent.putStringArrayListExtra("imgArr", imgString);
				intent.putStringArrayListExtra("imgDesc",imgDesc);
				intent.putStringArrayListExtra("imgCaption", imgCaption);
				intent.putStringArrayListExtra("vlinksArr", vlinkArr);
				intent.putStringArrayListExtra("nameArr", allNames);
				context.startActivity(intent);
			}
		});
		return convertView;
	}


	static class ViewHolder{

		public ImageView img_flip;
		//public TextView title_txt;

	}

}
