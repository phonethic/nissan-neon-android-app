package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.Calendar;

import com.phonethics.database.DataBaseUtil;
import com.phonethics.neon.R;
import com.phonethics.neon.Remeinders;
import com.phonethics.receiver.ReminderNissan;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.provider.CalendarContract.Reminders;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NissanListAdapter extends ArrayAdapter<String>{

	Context 				context;
	int 					layoutResourceId, i;
	int						day,month,year,hour,min,intPreValue ,tempPosition;
	boolean					isAlarmSet= false, calledFirst=false;
	DataBaseUtil 	db;
	Remeinders				reminder;
	RelativeLayout			layout;

	ArrayList<String> 		note;
	ArrayList<String> 		date;
	ArrayList<String> 		time;
	ArrayList<String> 		id;
	ArrayList<String> 		OnOff;
	ArrayList<String> 		preValue;
	ArrayList<String> 		preUnit;

	String[]				noOfMonths;
	String					tempDate, tempDay, tempMonth,tempYear,tempHour,tempMin,tempTime,tempMin1,noteTyped;
	String					strPreValue, strPreUnit;

	int				HELLO_ID;
	AlarmManager 			alarm;
	Calendar 				cal,tempCal,calendar;
	AlarmManager 			am; 
	/*ViewHolder 				holder;*/

	PendingIntent sender;


	public NissanListAdapter(Context context, int layoutResourceId,
			ArrayList<String> note,
			ArrayList<String> date, 
			ArrayList<String> time, 
			ArrayList<String> id, 
			ArrayList<String> OnOff,
			ArrayList<String> preValue,
			ArrayList<String> preUnit) 
	{
		super(context, layoutResourceId, note);
		// TODO Auto-generated constructor stub

		this.context 			= context;
		this.layoutResourceId 	= layoutResourceId;
		this.note 				= note;
		this.date 				= date;
		this.time 				= time;
		this.id   				= id;
		this.OnOff				= OnOff;
		this.preValue			= preValue;
		this.preUnit			= preUnit;
		reminder				= new Remeinders();

		db 						= new DataBaseUtil(context);
		alarm 					= (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

		noOfMonths				= new String[]{"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		calendar 				= Calendar.getInstance();
		/*am 						= (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);*/


	}

	private static class ViewHolder
	{
		TextView 		textdata;
		TextView 		textDate;
		TextView 		textTime;
		TextView		textPreValue;
		TextView		textPreUnit;
		CheckBox 		checkboxOnOff;
		CheckBox 		checkbox;
		ListView		list;
		RelativeLayout	relativeLayout;
		LinearLayout	linear_2;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		tempPosition = position;
		// TODO Auto-generated method stub
		View row = convertView;
		final ViewHolder holder;

		calledFirst = false;
		Log.d("=======> Position <========", "POSIITON ===>" + position);
		if (convertView == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ViewHolder();
			holder.checkboxOnOff 	= (CheckBox) row.findViewById(R.id.toggle);
			holder.checkbox 		= (CheckBox) row.findViewById(R.id.chechk);

			holder.textdata 		= (TextView) row.findViewById(R.id.text);
			holder.textDate			= (TextView) row.findViewById(R.id.text_date);
			holder.textDate			= (TextView) row.findViewById(R.id.text_date);
			holder.textTime			= (TextView) row.findViewById(R.id.text_time);
			holder.textPreValue		= (TextView) row.findViewById(R.id.text_notify_value);
			holder.textPreUnit		= (TextView) row.findViewById(R.id.text_notify_unit);
			holder.linear_2			= (LinearLayout)row.findViewById(R.id.linear_2);
			holder.linear_2.setVisibility(View.GONE);
			holder.relativeLayout	= (RelativeLayout) row.findViewById(R.id.layout_row);


			//// This will Set the visibility of pending reminders to be always true on every new adapter call
			if(Remeinders.pendingVisible == true){
				/*holder.relativeLayout.setBackgroundResource(R.drawable.reminder_bg1);*/
				holder.relativeLayout.setBackgroundColor(Color.parseColor("#c71444"));
				/*	holder.checkboxOnOff.setChecked(true);*/
				holder.checkbox.setVisibility(View.VISIBLE);
			}else if(Remeinders.pendingVisible == false){
				/*holder.relativeLayout.setBackgroundResource(R.drawable.completed_reminder_bg);*/
				holder.relativeLayout.setBackgroundColor(Color.parseColor("#c71444"));
				holder.checkboxOnOff.setChecked(false);

				holder.checkbox.setVisibility(View.GONE);
			}

			/////////////////// This will check if reminder is on or off and will set the image accordingly 
			///////////////////	WILL SET THE ALARM AT DESIRE TIME
			if( OnOff.get(position).toString().equals("1") ){///   database field is set to 1 to turn on the alarm
				Log.d("====> Alarm On <===="," Alarm On at POSITION  =====>> " + position);
				holder.checkboxOnOff.setChecked(true);// button on

				noteTyped		= note.get(position); 

				tempDate		= date.get(position);
				tempDay 		= tempDate.substring(0,2);
				tempMonth		= tempDate.substring(3,6);
				tempYear		= tempDate.substring(7,11);

				tempTime 		= time.get(position).toString().trim();
				tempHour		= tempTime.substring(0, tempTime.indexOf(":"));
				tempMin1		= "";

				try{
					tempMin1			= tempTime.substring(3, 5);
				}catch(StringIndexOutOfBoundsException exp){
					tempMin1			= tempTime.substring(3, 4);
				}

				tempMin			= tempMin1;

				strPreValue		= preValue.get(position);
				strPreUnit		= preUnit.get(position);

				hour			= Integer.parseInt(tempHour);
				min				= Integer.parseInt(tempMin);
				day				= Integer.parseInt(tempDay);
				year			= Integer.parseInt(tempYear);
				month			= 0;

				for(int monthVal = 0; monthVal <noOfMonths.length; monthVal++){
					if(noOfMonths[monthVal].equals(tempMonth)){
						month = monthVal;
						break;
					}
				}

				intPreValue		= Integer.parseInt(strPreValue);
				if(strPreUnit.equalsIgnoreCase("No Pre Notification") || strPreValue.equalsIgnoreCase("00") ){
					holder.textPreUnit.setText("No Pre Notify");
					holder.textPreValue.setVisibility(View.GONE);
					setAlarm(day, month, year, hour, min, noteTyped,position);
				}else if(strPreUnit.equalsIgnoreCase("Minutes")){
					min = min - intPreValue;
					holder.textPreValue.setVisibility(View.VISIBLE);
					Calendar c1 = Calendar.getInstance();
					Calendar c2 = Calendar.getInstance();
					c1.set(year, month, day, hour, min, 0);
					if(c1.compareTo(c2)==-1){
						holder.checkboxOnOff.setChecked(false);
					}
					setAlarm(day, month, year, hour, min, noteTyped,position);
				}else if(strPreUnit.equalsIgnoreCase("Hours")){
					hour = hour - intPreValue;
					holder.textPreValue.setVisibility(View.VISIBLE);
					Calendar c1 = Calendar.getInstance();
					Calendar c2 = Calendar.getInstance();
					c1.set(year, month, day, hour, min, 0);
					if(c1.compareTo(c2)==-1){
						holder.checkboxOnOff.setChecked(false);
					}
					setAlarm(day, month, year, hour, min, noteTyped,position);
				}else if(strPreUnit.equalsIgnoreCase("Days")){
					day = day - intPreValue;
					holder.textPreValue.setVisibility(View.VISIBLE);
					Calendar c1 = Calendar.getInstance();
					Calendar c2 = Calendar.getInstance();
					c1.set(year, month, day, hour, min, 0);
					if(c1.compareTo(c2)==-1){
						holder.checkboxOnOff.setChecked(false);
					}
					setAlarm(day, month, year, hour, min, noteTyped,position);
				}else if(strPreUnit.equalsIgnoreCase("Weeks")){
					day = day - (7*intPreValue);
					holder.textPreValue.setVisibility(View.VISIBLE);
					Calendar c1 = Calendar.getInstance();
					Calendar c2 = Calendar.getInstance();
					c1.set(year, month, day, hour, min, 0);
					if(c1.compareTo(c2)==-1){
						holder.checkboxOnOff.setChecked(false);
					}
					setAlarm(day, month, year, hour, min, noteTyped,position);
				}
				Log.d("--- databse field set to 1 ---", "--------------- ALARM SET ------------" );

			}
			else{// database field is set to 0 to turn off the alarm
				holder.checkboxOnOff.setChecked(false);
				try{
					db.open();
					db.setAlarm(id.get(position).toString(), 0);
					db.close();
				}catch(SQLException exp){
					Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
				}
			}

			/*	noteTyped		= note.get(position); 

			tempDate		= date.get(position);
			tempDay 		= tempDate.substring(0,2);
			tempMonth		= tempDate.substring(3,6);
			tempYear		= tempDate.substring(7,11);

			tempTime 		= time.get(position).toString().trim();
			tempHour		= tempTime.substring(0, tempTime.indexOf(":"));
			tempMin1		= "";

			try{
				tempMin1			= tempTime.substring(3, 5);
			}catch(StringIndexOutOfBoundsException exp){
				tempMin1			= tempTime.substring(3, 4);
			}

			tempMin			= tempMin1;

			strPreValue		= preValue.get(position);
			strPreUnit		= preUnit.get(position);

			hour			= Integer.parseInt(tempHour);
			min				= Integer.parseInt(tempMin);
			day				= Integer.parseInt(tempDay);
			year			= Integer.parseInt(tempYear);
			month			= 0;

			for(int monthVal = 0; monthVal <noOfMonths.length; monthVal++){
				if(noOfMonths[monthVal].equals(tempMonth)){
					month = monthVal;
					break;
				}
			}

			intPreValue		= Integer.parseInt(strPreValue);
			if(strPreUnit.equalsIgnoreCase("No Pre Notification") || strPreValue.equalsIgnoreCase("00") ){
				holder.textPreUnit.setText("No Pre Notify");
				holder.textPreValue.setVisibility(View.GONE);
				setAlarm(day, month, year, hour, min, noteTyped,position);
			}else if(strPreUnit.equalsIgnoreCase("Minutes")){
				min = min - intPreValue;
				holder.textPreValue.setVisibility(View.VISIBLE);
				setAlarm(day, month, year, hour, min, noteTyped,position);
			}else if(strPreUnit.equalsIgnoreCase("Hours")){
				hour = hour - intPreValue;
				holder.textPreValue.setVisibility(View.VISIBLE);
				setAlarm(day, month, year, hour, min, noteTyped,position);
			}else if(strPreUnit.equalsIgnoreCase("Days")){
				day = day - intPreValue;
				holder.textPreValue.setVisibility(View.VISIBLE);
				setAlarm(day, month, year, hour, min, noteTyped,position);
			}else if(strPreUnit.equalsIgnoreCase("Weeks")){
				day = day - (7*intPreValue); 
				holder.textPreValue.setVisibility(View.VISIBLE);
				setAlarm(day, month, year, hour, min, noteTyped,position);
			}

			if(isAlarmSet && Remeinders.pendingVisible){
				holder.checkboxOnOff.setChecked(true);
			}else{
				holder.checkboxOnOff.setChecked(false);
			}
			 */

/*
			holder.relativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(holder.linear_2.isShown()){
						holder.linear_2.setVisibility(View.GONE);
					}else{
						holder.linear_2.setVisibility(View.VISIBLE);
					}
				}
			});*/
			holder.checkboxOnOff.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(Remeinders.pendingVisible == true){
						if( holder.checkboxOnOff.isChecked()){////////////if light is on than will set the alarm on desired time
							/*if(Remeinders.pendingVisible == true){*/
							Log.d("----- If ON CLICK-----", "--------------- REMINDER ON ------------" );
							calledFirst =true;
							try{
								db.open();
								db.setAlarm(id.get(position).toString(), 1);
								db.close();
							}catch(SQLException exp){
								Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
							}
							noteTyped		= note.get(position); 

							tempDate		= date.get(position);
							tempDay 		= tempDate.substring(0,2);
							tempMonth		= tempDate.substring(3,6);
							tempYear		= tempDate.substring(7,11);

							tempTime 		= time.get(position).toString().trim();
							tempHour		= tempTime.substring(0, tempTime.indexOf(":"));
							tempMin1		= "";
							try{
								tempMin1			= tempTime.substring(3, 5);
							}catch(StringIndexOutOfBoundsException exp){
								tempMin1			= tempTime.substring(3, 4);
							}

							tempMin			= tempMin1;

							strPreValue		= preValue.get(position);
							strPreUnit		= preUnit.get(position);

							hour			= Integer.parseInt(tempHour);
							min				= Integer.parseInt(tempMin);
							day				= Integer.parseInt(tempDay);
							year			= Integer.parseInt(tempYear);
							month			= 0;

							for(int monthVal = 0; monthVal <noOfMonths.length; monthVal++){
								if(noOfMonths[monthVal].equals(tempMonth)){
									month = monthVal;
									break;
								}
							}

							intPreValue		= Integer.parseInt(strPreValue);

							if(strPreUnit.equalsIgnoreCase("No Pre Notification")){
								holder.textPreUnit.setText("No Pre Notify");
								holder.textPreValue.setVisibility(View.GONE);
								setAlarm(day, month, year, hour, min, noteTyped,position);
							}else if(strPreUnit.equalsIgnoreCase("Minutes")){
								min = min - intPreValue;
								holder.textPreValue.setVisibility(View.VISIBLE);
								setAlarm(day, month, year, hour, min, noteTyped,position);
							}else if(strPreUnit.equalsIgnoreCase("Hours")){
								hour = hour - intPreValue;
								holder.textPreValue.setVisibility(View.VISIBLE);
								setAlarm(day, month, year, hour, min, noteTyped,position);
							}else if(strPreUnit.equalsIgnoreCase("Days")){
								day = day - intPreValue;
								holder.textPreValue.setVisibility(View.VISIBLE);
								setAlarm(day, month, year, hour, min, noteTyped,position);
							}else if(strPreUnit.equalsIgnoreCase("Weeks")){
								day = day - (7*intPreValue);
								holder.textPreValue.setVisibility(View.VISIBLE);
								setAlarm(day, month, year, hour, min, noteTyped,position);
							}

							Calendar cal 		= 	Calendar.getInstance();
							Calendar tempCal	=	Calendar.getInstance();

							tempCal.set(year, month, day, hour, min,0);

							if(tempCal.compareTo(cal) == -1){
								holder.checkboxOnOff.setChecked(false);
								Log.d("----- after on click IF condition -----", "---- TIME ALREADY PASSED -- " );
							}

						}
						else{
							/*	Toast.makeText(context, " ALARM OFF" + position,Toast.LENGTH_SHORT).show();		*/			
							Log.d("----- ELSE ON CLICK-----", "--------------- REMINDER OFF at position ---- " +position );
							try{
								db.open();
								db.setAlarm(id.get(position).toString(), 0);
								db.close();
							}catch(SQLException exp){
								Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
							}

						}

					}else if(Remeinders.pendingVisible==false){
						holder.checkboxOnOff.setChecked(false);
						try{
							db.open();
							db.setAlarm(id.get(position).toString(), 0);
							db.close();
						}catch(SQLException exp){
							Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
						}
					}
				}
			});

			holder.checkbox.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(holder.checkbox.isChecked()){
						/*view.showPrevious();*/
						i = position - 1 ;
						if(i <= 0) {
							i = 0;
						}

						//Toast.makeText(context, "POSITION Of List and I --" + position +" -- " + i, Toast.LENGTH_SHORT).show();

						AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
						alertDialog.setIcon(R.drawable.ic_launcher);
						alertDialog.setTitle("Neon");
						/*alertDialog.setMessage("Mark this Reminder as Completed?");*/
						if(Remeinders.pendingVisible == true){
							alertDialog.setMessage("Mark this reminder as completed?");
						}else{
							alertDialog.setMessage("Mark this reminder as pending?");
						}
						alertDialog.setCancelable(true);
						alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							/*String remId = id.get(position);*/
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								holder.checkbox.setChecked(false);
								try{

									db.open();
									/*db.moveToComplete(remId);*/
									if(Remeinders.pendingVisible == true){
										db.moveToComplete(id.get(position).toString());
										db.setAlarm(id.get(position).toString(), 0);
										Toast.makeText(context, "Reminder has been moved to completed", Toast.LENGTH_SHORT).show();
									}else{
										db.moveToPending(id.get(position).toString());
										Toast.makeText(context, "Reminder has been moved to pending", Toast.LENGTH_SHORT).show();
									}	
									db.close();
								}catch(SQLException sql){
									Log.d("move to complete list addapter", sql.toString());
								}
								notifyDataSetChanged();
							}
						});
						alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								holder.checkbox.setChecked(false);
							}
						});
						AlertDialog alert = alertDialog.create();
						alert.show();
					}
				}
			});

			row.setTag(holder);

		}else{
			holder = (ViewHolder)row.getTag();
		}
		String message = note.get(position);
		String sDate = date.get(position);
		String sTime = time.get(position);
		String sValue = preValue.get(position);
		String sUnit = preUnit.get(position);

		holder.textdata.setText(message);
		holder.textDate.setText(sDate);
		holder.textTime.setText(sTime);

		//holder.textdata.setTextColor(Color.BLACK);


		if(sValue.equalsIgnoreCase("0"))
		{
			holder.textPreValue.setVisibility(View.GONE);

		}
		else
		{
			holder.textPreValue.setVisibility(View.VISIBLE);
			holder.textPreValue.setText("("  + sValue);
		}
		if(sUnit.equalsIgnoreCase("No Pre Notification"))
		{
			holder.textPreUnit.setText("(No Pre Notify)");
		}
		else
		{
			holder.textPreUnit.setText(sUnit + ")");
		}

		holder.textDate.setTextColor(Color.WHITE);
		holder.textTime.setTextColor(Color.WHITE);
		holder.textPreValue.setTextColor(Color.WHITE);
		holder.textPreUnit.setTextColor(Color.WHITE);

		if(Remeinders.pendingVisible == true){
			//holder.textdata.setTextColor(Color.parseColor("#F87431"));
		}else{
			//holder.textdata.setTextColor(Color.BLACK);
		}

		return row;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();
		Remeinders.completedImg.performClick();
		Remeinders.pendingImg.performClick();
		try{
			db.open();
			if(Remeinders.pendingVisible == true){
				Remeinders.completedImg.performClick();
				Remeinders.pendingImg.performClick();
				reminder_details = db.getAllPendingRemeinders();
			}else{
				Remeinders.pendingImg.performClick();
				Remeinders.completedImg.performClick();
				reminder_details = db.getAllCompletedRemeinders();
			}
			db.close();
		}catch(SQLException exp){
			Log.d("SQLException adapter", exp.toString());
		}

		if(reminder_details.isEmpty()){
			if(Remeinders.pendingVisible == true){
				/*Toast.makeText(context, "There Are No Pending Reminders", Toast.LENGTH_SHORT).show();*/
				Remeinders.info.setVisibility(View.VISIBLE);
				Remeinders.pending_completText.setVisibility(View.GONE);

				id.clear();
				note.clear();
				date.clear();
				time.clear();
				OnOff.clear();
				preValue.clear();
				preUnit.clear();
			}else{
				/*Toast.makeText(context, "There Are No Completed Reminders", Toast.LENGTH_SHORT).show();*/
				Remeinders.info.setVisibility(View.VISIBLE);
				Remeinders.remeindersList.setVisibility(View.GONE);
				Remeinders.pending_completText.setVisibility(View.GONE);

				id.clear();
				note.clear();
				date.clear();
				time.clear();
				preValue.clear();
				preUnit.clear();
				OnOff.clear();
			}
		}else{
			if (Remeinders.pendingVisible == true) {
				Remeinders.pending_completText.setText(Remeinders.pendingText);
				Remeinders.pending_completText.setVisibility(View.GONE);
				Remeinders.info.setVisibility(View.GONE);
			}else{
				Remeinders.pending_completText.setText(Remeinders.completedText);
				Remeinders.pending_completText.setVisibility(View.GONE);
				Remeinders.info.setVisibility(View.GONE);
			}
			ArrayList<Object> tempArr 		= new ArrayList<Object>();
			ArrayList<String> tempId 		= new ArrayList<String>();
			ArrayList<String> tempNote 		= new ArrayList<String>();
			ArrayList<String> tempDate 		= new ArrayList<String>();
			ArrayList<String> tempTime 		= new ArrayList<String>();
			ArrayList<String> tempOnOff		= new ArrayList<String>();
			ArrayList<String> tempPreVal	= new ArrayList<String>();
			ArrayList<String> tempPreUnit	= new ArrayList<String>();

			tempArr.clear();
			id.clear();
			note.clear();
			date.clear();
			time.clear();
			OnOff.clear();
			preValue.clear();
			preUnit.clear();

			for(int pos = 0; pos < reminder_details.size(); pos++){		//// Will pick out the message, date and time of each reminder
				tempArr = reminder_details.get(pos);								//	 and Store it in individual arraylist
				tempId.add(		tempArr.get(0).toString());
				tempOnOff.add(	tempArr.get(1).toString());
				tempNote.add(	tempArr.get(2).toString());
				tempDate.add(	tempArr.get(3).toString());
				tempTime.add(	tempArr.get(4).toString());
				tempPreVal.add(	tempArr.get(5).toString());
				tempPreUnit.add(tempArr.get(6).toString());
			}

			id.addAll(tempId);
			OnOff.addAll(tempOnOff);
			note.addAll(tempNote);
			date.addAll(tempDate);
			time.addAll(tempTime);
			preValue.addAll(tempPreVal);
			preUnit.addAll(tempPreUnit);

			/*Toast.makeText(context, "ID Array Size " + id.size() +"-- ONOFF array--" + OnOff.size() , Toast.LENGTH_SHORT).show();*/
		}
		super.notifyDataSetChanged();
	}

	public void setAlarm(int dayValue, int monthValue, int yearValue, int hourValue, int minValue, String message, int pos){


		calendar.set(yearValue, monthValue, dayValue, hourValue, minValue, 0);
		Calendar tempCal = Calendar.getInstance();
		Intent intent = new Intent(context,ReminderNissan.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		intent.putExtra("title",message);
		intent.putExtra("note",message);
		intent.putExtra("id", pos);

		Log.d("****Indide set alarm method*****", "PSOITION " + pos);

		if((tempCal.compareTo(calendar)==  -1) || (tempCal.compareTo(calendar)==  0)){
			try{
				db.open();
				db.setAlarm(id.get(pos).toString(), 1);
				db.close();
			}catch(SQLException exp){
				Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
			}
			isAlarmSet = true;
			HELLO_ID = HELLO_ID + 1;
			PendingIntent sender = PendingIntent.getBroadcast(context, HELLO_ID,intent,PendingIntent.FLAG_UPDATE_CURRENT);
			/*PendingIntent newInt = PendingIntent.getActivity(context, HELLO_ID, intent, 0);*/
			am 						= (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);
			am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
			
			Toast.makeText(context, "*** ALARM SET AT ***  " + pos, Toast.LENGTH_SHORT).show();

		}else{
			try{
				db.open();
				db.setAlarm(id.get(pos).toString(), 0);
				db.close();
			}catch(SQLException exp){
				Log.d("SET ALARM OFF SQL EXCEPTION", exp.toString());
			}
			isAlarmSet = false;
			if(calledFirst){
				Toast.makeText(context, "Time Has Alredy Passed. Set proper Time. ", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public boolean alramSet(){
		return isAlarmSet;
	}

}
