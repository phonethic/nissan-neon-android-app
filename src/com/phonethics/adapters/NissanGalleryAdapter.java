package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.phonethics.imageloader.ImageLoader;
import com.phonethics.neon.R;

public class NissanGalleryAdapter extends BaseAdapter {

	Activity 						context;
	ArrayList<String> 				orderArr;
	ImageLoader						imgLoader;
	ArrayList<ArrayList<String>> 	allThumbnails, allCaptions;
	public static LayoutInflater 	inflator;
	int                             SCREEN_WIDTH,IMAGE_HIGHT;
	Handler 						handler_0,handler_1;
	Runnable 						runnable_0,runnable_1;
	int								START_TIME = 1000;
	Animation 						anim,anim1;
	ViewHolder 						holder;
	ArrayList<ImageView> 			imgview_arr;
	ArrayList<String> 				data;
	int pos;
	int count = 0;

	public NissanGalleryAdapter(Activity context,ArrayList<String> orderArr, ArrayList<ArrayList<String>> allThumbnails,
			ArrayList<ArrayList<String>> allCaptions){
		this.context = context;
		this.orderArr = orderArr;
		this.allThumbnails = allThumbnails;
		this.allCaptions = allCaptions;
		imgLoader = new ImageLoader(this.context);


		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5)-1);

		imgview_arr = new ArrayList<ImageView>();
		data = new ArrayList<String>();
		anim = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
		anim1 = AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return orderArr.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return orderArr.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null){
			inflator 		= context.getLayoutInflater();
			convertView 	= inflator.inflate(R.layout.imageview_flip, null);
			ViewHolder holder 			= new ViewHolder();
			holder.img_flip = (ImageView) convertView.findViewById(R.id.imgview_flip);
			holder.title_caption = (TextView) convertView.findViewById(R.id.text_caption);
			imgview_arr.add(holder.img_flip);
			holder.img_flip.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
			holder.img_flip.setTag(position);
			convertView.setTag(holder);
			
			/*switch (Integer.parseInt(holder.img_flip.getTag().toString())) {
			case 0:
				//anim = null;
				
				anim =  AnimationUtils.loadAnimation(context, R.anim.push_up_in);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;

			case 1:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.fade_in);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 2:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.fade_out);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 3:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 4:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.slide_out_left);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 5:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 6:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.shrink_fade_out_center);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 7:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 8:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.push_up_in);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			case 9:
				//anim = null;
				anim =  AnimationUtils.loadAnimation(context, R.anim.righttoleftslide);
				anim.setDuration(1000);
				//anim.setRepeatCount(-1);
				anim.setRepeatMode(Animation.RESTART);
				anim.setInterpolator(new LinearInterpolator());
				holder.img_flip.setAnimation(anim);
				break;
			default:
				break;
			}*/
		}

		final ViewHolder hold = (ViewHolder) convertView.getTag();
		String url = allThumbnails.get(position).get(0);
		imgLoader.DisplayImage(url, hold.img_flip);
		hold.title_caption.setText(allCaptions.get(position).get(0));
		//animateTheRow(position, hold.img_flip);
		
		switch (Integer.parseInt(hold.img_flip.getTag().toString())) {
		case 0:
			//anim = null;
			
			anim =  AnimationUtils.loadAnimation(context, R.anim.push_up_in);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;

		case 1:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.fade_in);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 2:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.fade_out);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 3:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 4:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.slide_out_left);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 5:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 6:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.shrink_fade_out_center);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 7:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 8:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.push_up_in);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		case 9:
			//anim = null;
			anim =  AnimationUtils.loadAnimation(context, R.anim.righttoleftslide);
			anim.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim.setRepeatMode(Animation.RESTART);
			anim.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim);
			break;
		default:
			break;
		}
		if(position == 0){/*
			final Handler handler = new Handler();
			Runnable runnable = new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					anim =  AnimationUtils.loadAnimation(context, R.anim.push_up_in);
					anim.setDuration(1000);
					//anim.setRepeatCount(-1);
					anim.setRepeatMode(Animation.RESTART);
					anim.setInterpolator(new LinearInterpolator());
					hold.img_flip.setAnimation(anim);
					anim.setAnimationListener(new AnimationListener() {
						
						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub
							count++;
							Toast.makeText(context, "Count "+count, 0).show();
						}
						
						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							count++;
							Toast.makeText(context, "Count "+count, 0).show();
						}
					});
					handler.postDelayed(this, 3000);
				}
				
				
			};
			handler.postDelayed(runnable, 1000);
			
		*/}
		
		else{
			
		}
		/*if(position == 1){
			anim1 =  AnimationUtils.loadAnimation(context, R.anim.fade_in);
			anim1.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim1.setRepeatMode(Animation.RESTART);
			anim1.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim1);
		}
		if(position == 2){
			anim1 =  AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim1.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim1.setRepeatMode(Animation.RESTART);
			anim1.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim1);
		}
		if(position == 3){
			anim1 =  AnimationUtils.loadAnimation(context, R.anim.righttoleftslide);
			anim1.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim1.setRepeatMode(Animation.RESTART);
			anim1.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim1);
		}
		if(position == 4){
			anim1 =  AnimationUtils.loadAnimation(context, R.anim.slide_out_left);
			anim1.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim1.setRepeatMode(Animation.RESTART);
			anim1.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim1);
		}if(position == 5){
			anim1 =  AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
			anim1.setDuration(1000);
			//anim.setRepeatCount(-1);
			anim1.setRepeatMode(Animation.RESTART);
			anim1.setInterpolator(new LinearInterpolator());
			hold.img_flip.setAnimation(anim1);
		}
*/		

		return convertView;
	}


	static class ViewHolder{

		public ImageView img_flip;
		public TextView title_caption;
	}


	public void stopAnim(){
		
	}

	public void startAnim(int position){
		
	}

	void printData(){
		for(int i=0;i<data.size();i++){
			//Log.d("=====", "========  " + i +"  ============");
			//Log.d("====", "data == "+ data.get(i));
		}
	}

	void restartAnim(int position){/*
		pos = position;
		//handler_pos_1 	= new Handler();
		runnable 		= new Runnable() {

			int x = 0 ;

			ArrayList<String> tempList = allThumbnails.get(pos);

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if(pos==0){
					imgLoader.DisplayImage(tempList.get(x), imgview_arr.get(0));
					anim = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
					imgview_arr.get(0).setAnimation(anim);
					imgview_arr.get(0).startAnimation(anim);
					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							//Toast.makeText(context, "anim end", 0).show();
							printData();
							x++;
							if(x==tempList.size()||x>2){
								x=0;
							}
							if(x!=0){
								imgLoader.DisplayImage(tempList.get(x-1), imgview_arr.get(0));
							}else if(x==0){
								imgLoader.DisplayImage(tempList.get(tempList.size()-1), imgview_arr.get(0));
							}
						}

					});
					//handler_pos_1.postDelayed(runnable, 2000);
				}else if(pos == 3){
					imgLoader.DisplayImage(tempList.get(x), imgview_arr.get(3));
					anim = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
					imgview_arr.get(3).setAnimation(anim);
					imgview_arr.get(3).startAnimation(anim);
					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							//Toast.makeText(context, "anim end", 0).show();
							printData();
							x++;
							if(x==tempList.size()||x>2){
								x=0;
							}
							if(x!=0){
								imgLoader.DisplayImage(tempList.get(x-1), imgview_arr.get(3));
							}else if(x==0){
								imgLoader.DisplayImage(tempList.get(tempList.size()-1), imgview_arr.get(3));
							}
						}

					});
				}
				handler_pos_1.postDelayed(runnable, 2000);
			}

		};
		handler_pos_1.postDelayed(runnable, START_TIME);
	*/}

	
	
	
	
	public void animateTheRow(int posn, final ImageView imgview){
		pos = posn;
		
		if(pos==0){
			handler_0 = null;
			runnable_0 =null;
			handler_0 = new Handler();
			runnable_0 = new Runnable() {
				
				int x = 0 ;
				ArrayList<String> tempList = allThumbnails.get(0);

				@Override
				public void run() {
					// TODO Auto-generated method stub

					imgLoader.DisplayImage(tempList.get(x), imgview);
					//anim = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
					imgview.setAnimation(anim);
					imgview.startAnimation(anim);
					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							//Toast.makeText(context, "anim end", 0).show();
							//printData();
							x++;
							if(x==tempList.size()||x>2){
								x=0;
							}
							if(x!=0){
								imgLoader.DisplayImage(tempList.get(x-1), imgview);
							}else if(x==0){
								imgLoader.DisplayImage(tempList.get(tempList.size()-1), imgview);
							}
						}

					});
					handler_0.postDelayed(this, 3000);
				
				}
			};
			handler_0.postDelayed(runnable_0, START_TIME);
		}else if(pos == 1){

			handler_1 = null;
			runnable_1 =null;
			handler_1 = new Handler();
			runnable_1 = new Runnable() {
				
				int y = 0 ;
				ArrayList<String> tempList = allThumbnails.get(1);

				@Override
				public void run() {
					// TODO Auto-generated method stub

					imgLoader.DisplayImage(tempList.get(y), imgview);
					//anim = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
					imgview.setAnimation(anim1);
					imgview.startAnimation(anim1);
					anim1.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							//Toast.makeText(context, "anim end", 0).show();
							//printData();
							y++;
							if(y==tempList.size()||y>2){
								y=0;
							}
							if(y!=0){
								imgLoader.DisplayImage(tempList.get(y-1), imgview);
							}else if(y==0){
								imgLoader.DisplayImage(tempList.get(tempList.size()-1), imgview);
							}
						}

					});
					handler_1.postDelayed(this, 3000);
				
				}
			};
			handler_1.postDelayed(runnable_1, START_TIME+500);
		
		}
	}
}
