package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.customclass.DealerLocationAndAddress;

import com.phonethics.neon.Dealer_location;
import com.phonethics.neon.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DealersListAdapters extends BaseExpandableListAdapter {

	private List<DealerLocationAndAddress> mParent;
	private LayoutInflater inflater;
	Activity context;

	public DealersListAdapters(Activity context, List<DealerLocationAndAddress> parent){
		mParent = parent;
		inflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child, parent,false);
		}
		TextView text_name = (TextView) convertView.findViewById(R.id.text_dealerName);
		TextView text_add = (TextView) convertView.findViewById(R.id.text_dealerAdd);
		 final RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.chid_layout);
		text_name.setText(mParent.get(groupPosition).dlrName.get(childPosition));
		text_add.setText(mParent.get(groupPosition).dlrAddress.get(childPosition));
		text_name.setTextColor(Color.BLACK);
		text_add.setTextColor(Color.BLACK);
		layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Toast.makeText(context, "City " +mParent.get(groupPosition).getCityName() + " Dealer Name " + mParent.get(groupPosition).dlrName.get(childPosition), Toast.LENGTH_SHORT).show();*/
				/*layout.setBackgroundColor(Color.parseColor("#f7941d"));*/
				
				
				
				String dealerName = mParent.get(groupPosition).dlrName.get(childPosition);
				String dealerAdd  = mParent.get(groupPosition).dlrAddress.get(childPosition);
				String imgUrl1		= mParent.get(groupPosition).dlrPhoto1.get(childPosition);
				String imgUrl2		= mParent.get(groupPosition).dlrPhoto2.get(childPosition);
				String latitude		= mParent.get(groupPosition).dlrlatitude.get(childPosition);
				String longitude	= mParent.get(groupPosition).dlrlongitude.get(childPosition);

				ArrayList<String> extraDetails = new ArrayList<String>();
				ArrayList<String> imgUrlsArr = new ArrayList<String>();

				String phone1 = mParent.get(groupPosition).dlrPhone1.get(childPosition);
				String phone2 = mParent.get(groupPosition).dlrPhone2.get(childPosition);
				String mobile1 = mParent.get(groupPosition).dlrMobile1.get(childPosition);
				String mobile2 = mParent.get(groupPosition).dlrMobile2.get(childPosition);
				String fax = mParent.get(groupPosition).dlrFax.get(childPosition);
				String email = mParent.get(groupPosition).dlrEmail.get(childPosition);
				String website = mParent.get(groupPosition).dlrWebsite.get(childPosition);
				String header = mParent.get(groupPosition).getCityName();

				Log.i("data ---- ", "Phone1 " + phone1);
				Log.i("data ---- ", "phone2 " + phone2);
				Log.i("data ---- ", "mobile1 " + mobile1);
				Log.i("data ---- ", "mobile2 " + mobile2);
				Log.i("data ---- ", "fax " + fax);
				Log.i("data ---- ", "email " + email);
				Log.i("data ---- ", "website " + website);
				Log.i("data ---- ", "website " + imgUrl1);
				
				
				
				/*if(!phone1.equalsIgnoreCase("") && !phone1.equalsIgnoreCase("0") ){
					extraDetails.add(phone1);
				}if(!phone2.equalsIgnoreCase("") && !phone2.equalsIgnoreCase("0")){
					extraDetails.add(phone2);
				}if(!mobile1.equalsIgnoreCase("") && !mobile1.equalsIgnoreCase("0")){
					extraDetails.add(mobile1);
				}if(!mobile2.equalsIgnoreCase("") && !mobile2.equalsIgnoreCase("0")){
					extraDetails.add(mobile2);
				}if(!fax.equalsIgnoreCase("") && !fax.equalsIgnoreCase("0")){
					extraDetails.add(fax);
				}if(!email.equalsIgnoreCase("") && !email.equalsIgnoreCase("0")){
					extraDetails.add(email);
				}if(!website.equalsIgnoreCase("") && !!website.equalsIgnoreCase("0") ){
					extraDetails.add(website);
				}
				*/
				

				extraDetails.add(phone1);
				extraDetails.add(phone2);
				extraDetails.add(mobile1);
				extraDetails.add(mobile2);
				extraDetails.add("~"+fax);
				extraDetails.add(email);
				extraDetails.add(website);
				
				imgUrlsArr.clear();
				if(!imgUrl1.equalsIgnoreCase("")){
				imgUrlsArr.add(imgUrl1);

				}
				if(!imgUrl2.equalsIgnoreCase("")){

				imgUrlsArr.add(imgUrl2);
				}

				Intent intent = new Intent(context, Dealer_location.class);
				intent.putExtra("dlrName", dealerName);
				intent.putExtra("dlrAdd", dealerAdd);
				intent.putExtra("imgUrl", imgUrl1);
				intent.putExtra("latitude", latitude);
				intent.putExtra("longitude", longitude);
				intent.putExtra("header", header);
				intent.putStringArrayListExtra("extraDetails", extraDetails);
				intent.putStringArrayListExtra("imgUrlsArr", imgUrlsArr);
				context.startActivity(intent);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				
			}
		});
		/*text_add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(context, "City " +mParent.get(groupPosition).getCityName() + " Dealer Name " + mParent.get(groupPosition).dlrName.get(childPosition), Toast.LENGTH_SHORT).show();
				String dealerName = mParent.get(groupPosition).dlrName.get(childPosition);
				String dealerAdd  = mParent.get(groupPosition).dlrAddress.get(childPosition);
				String imgUrl		= mParent.get(groupPosition).dlrPhoto1.get(childPosition);
				String latitude		= mParent.get(groupPosition).dlrlatitude.get(childPosition);
				String longitude	= mParent.get(groupPosition).dlrlongitude.get(childPosition);
				Intent intent = new Intent(context, Dealer_location.class);
				intent.putExtra("dlrName", dealerName);
				intent.putExtra("dlrAdd", dealerAdd);
				intent.putExtra("imgUrl", imgUrl);
				intent.putExtra("latitude", latitude);
				intent.putExtra("longitude", longitude);
				context.startActivity(intent);
			}
		});*/

		return convertView;
	}

	@Override
	public int getChildrenCount(int i) {
		// TODO Auto-generated method stub
		return mParent.get(i).dlrName.size();
	}

	@Override
	public Object getGroup(int i) {
		// TODO Auto-generated method stub
		return mParent.get(i).getCityName();
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return mParent.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.parent_layout, parent,false);
		}
		TextView textView = (TextView) convertView.findViewById(R.id.text_cityName);
		textView.setText(mParent.get(groupPosition).getCityName());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
